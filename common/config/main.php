<?php

use common\components\Environment;
use common\components\IpGeoBase;
use common\components\LogDbTarget;
use common\modules\banner\MBanner;
use common\modules\message\MMessage;
use common\modules\news\MNews;
use common\modules\pages\MPages;
use common\modules\seo\MSeo;

return [
	'aliases'    => [
		'@bower' => '@vendor/bower-asset',
		'@npm'   => '@vendor/npm-asset',
	],
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'modules' => [
		'news'    => [
			'class' => MNews::class,
		],
		'seo'     => [
			'class' => MSeo::class,
		],
		'message' => [
			'class' => MMessage::class,
		],
		'pages'   => [
			'class' => MPages::class,
		],
		'banner'  => [
			'class' => MBanner::class,
		]
	],
	'components' => [
		'ipgeobase'   => [
			'class'      => IpGeoBase::class,
			'useLocalDB' => true,
		],
		'yandexGeo'   => [
			'class' => \common\components\YandexGeo::class,
			'key' => '082a7ffb-f131-40f9-b1ca-73dbf658c945'
		],
		'opengraph'   => [
			'class' => 'fgh151\opengraph\OpenGraph',
		],
		'imageresize' => [
			'class'       => 'noam148\imageresize\ImageResize',
			//path relative web folder. In case of multiple environments (frontend, backend) add more paths
			'cachePath'   => ['assets/images/cache', '../../frontend/web/assets/images/cache'],
			//use filename (seo friendly) for resized images else use a hash
			'useFilename' => false,
			//show full url (for example in case of a API)
			'absoluteUrl' => false,
		],
		'imageCache'  => [
			'class'      => 'iutbay\yii2imagecache\ImageCache',
			'sourcePath' => '@app/web/images/uploads',
			'sourceUrl'  => '@web/images/uploads',
			'thumbsPath' => '@app/web/images/thumb',
			//'thumbsUrl'  => '@web/site/thumbs?path=',
			'sizes'      => [
				'thumb'          => [150, 150],
				'medium'         => [300, 300],
				'large'          => [600, 600],
				'280'            => [280, 280],
				'250'            => [250, 250],
				'240'            => [240, 240],
				'200'            => [200, 200],
				'35'             => [35, 35],
				'330'            => [330, 330],
				'mobile-preview' => [310, 310],
			],
		],
		'env'          => [
			'class'           => Environment::class,
			'defaultCityId'   => '',
			'defaultLanguage' => 'ru',
		],
		'assetManager' => [
			'linkAssets'      => true,
			'appendTimestamp' => true,
		],
		'cache'        => [
			'class'        => \yii\caching\MemCache::class,
			'useMemcached' => true,
		],
		'user'         => [
			'identityClass'   => \common\models\User::class,
			'enableAutoLogin' => true,
			'authTimeout'     => 3600 * 8,
		],
		'session'      => [
			'class'        => 'yii\web\Session',
			'cookieParams' => ['httponly' => true, 'lifetime' => 3600 * 8],
			'timeout'      => 3600 * 8,
			'useCookies'   => true,
		],
		'log'          => [
			'traceLevel' => 3,
			'targets'    => [
				'db' => [
					'class'  => LogDbTarget::class,
					'levels' => ['error'],
				],
			],
		],
	],
];
