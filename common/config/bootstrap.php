<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@webBackend', '/internal/');
Yii::setAlias('@companyPhoto', dirname(dirname(__DIR__)) . '/frontend/web/uploads/company/photo');
Yii::setAlias('@companyLogo', dirname(dirname(__DIR__)) . '/frontend/web/uploads/company/logo');
Yii::setAlias('@newsImage', dirname(dirname(__DIR__)) . '/frontend/web/uploads/news');


