<?php

namespace common\components;

use common\helpers\Dump;
use yii\base\Component;
use Yii;

/**
 * Class YandexGeo
 *
 * @package common\components
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class YandexGeo extends Component {
	public $key;
	private $_apiUrl = 'https://geocode-maps.yandex.ru/1.x';

	const PARAM_FORMAT = 'format';
	const PARAM_GEOCODE = 'geocode';
	const PARAM_API_KEY = 'apikey';

	const FORMAT_JSON = 'json';

	public function getLocation($address) {

		$queryString = http_build_query([
			static::PARAM_FORMAT  => static::FORMAT_JSON,
			static::PARAM_API_KEY => $this->key,
			static::PARAM_GEOCODE => $address
		]);

		$results = json_decode(file_get_contents($this->_apiUrl . '?' . $queryString));

		if(isset($results->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos)) {
			return explode(' ', $results->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
		}

		return null;
	}
}