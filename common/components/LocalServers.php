<?php

namespace common\components;

class LocalServers {

	const SERVERS = [
		'192.168.1.170' => 'Data516',
		'192.168.1.56'  => 'Data2',
		'192.168.1.146' => 'CalloCall',
		'192.168.1.52'  => 'Sherlock Web',
		'192.168.1.101' => 'Телефонная станция Panasonic',
	];

	const CLASS_STATUS = [
		self::STATUS_OFFLINE => 'alert alert-danger',
		self::STATUS_ONLINE => 'alert alert-success',
	];

	const STATUS_ONLINE = 1;
	const STATUS_OFFLINE = 0;

	const STATUS_NAMES = [
		self::STATUS_ONLINE => 'Доступен',
		self::STATUS_OFFLINE => 'Недоступен',
	];

	const STATUS_COLOR = [
		self::STATUS_ONLINE => '',
		self::STATUS_OFFLINE => '',
	];
}