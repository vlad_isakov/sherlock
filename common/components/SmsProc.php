<?php
/**
 * @author Исаков Владислав <visakov@biletur.ru>
 */

namespace common\components;

use common\components\SMSBeeline;
use yii\base\Component;

class SmsProc extends Component {

	public $url = 'https://beeline.amega-inform.ru/sms_send';
	const ATTR_URL = 'url';

	public $action = '';
	const ATTR_ACTION = 'action';

	public $login = '1657691.1';
	const ATTR_LOGIN = 'login';

	public $password = '45W7tx5rn633da';
	const ATTR_PASSWORD = 'password';

	public $text;
	const ATTR_TEXT = 'text';

	public $phone;
	const ATTR_PHONE = 'phone';

	const ACTION_SEND = 'post_sms';
	const ACTION_STATUS = 'status';

	public $id;
	const ATTR_ID = 'id';

	/**
	 * Отправка запроса
	 *
	 * @return bool|string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function send() {

		$sms = new \common\components\SMSBeeline($this->login, $this->password, 'beeline.amega-inform.ru');
		$result = $sms->post_message($this->text, $this->phone, 'SL_516');

		return $result;
	}

	/**
	 * Проверка статуса смс
	 *
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function checkStatus() {
		$sms = new \common\components\SMSBeeline($this->login, $this->password, 'beeline.amega-inform.ru');
		$result = $sms->status_sms_id($this->id);

		return $result;
	}
}