<?php
namespace common\components;

use yii\base\Component;

class Call extends Component {
	public $id;
	const ATTR_ID = 'id';

	public $aNumber;
	const ATTR_A_NUMBER = 'aNumber';

	public $bNumber;
	const ATTR_B_NUMBER = 'bNumber';

	public $cityGuid;
	const ATTR_CITY_GUID = 'cityGuid';

	public $cityName;
	const ATTR_CITY_NAME = 'cityName';

	public $cityNum;
	const ATTR_CITY_NUM = 'cityNum';

	public $logicalNumber;
	const ATTR_LOGICAL_NUMBER = 'logicalNumber';

	public $clientName ;
	const ATTR_CLIENT_NAME = 'clientName';
}