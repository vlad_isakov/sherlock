<?php

use fgh151\opengraph\OpenGraph;
use yii\BaseYii;

/**
 *
 * Этот файл содержит phpdoc для системных классов Yii.
 * Внимание! Он не подключается и не используется для создания объектов.
 *
 * Этот файл нужен только для IDE как "обманка", чтобы не править файлы в папке vendor.
 * Так как базовая версия Yii не значет ничего о нашиъ компонентах и наших классах, то IDE не подсвечивает их.
 * Чтобы это работало, в этом файле мы переопределили Yii и Application, которые будем дополнять своими свойствами.
 *
 * Возможно, потребуется пометить оригинальный Yii.php как PlainText, чтобы автокомплит полноценно работал.
 */
class Yii extends BaseYii {
	/** @var yii\console\Application|yii\web\Application|Application The application instance */
	public static $app;
}

/**
 * @author Vlad Isakov
 *
 */
class User extends \yii\web\User {

}


/**
 * @author isakov.v
 *
 * @property yii\swiftmailer\Mailer                 $mail
 * @property-read \iutbay\yii2imagecache\ImageCache $imageCache
 * @property-read fgh151\opengraph\OpenGraph        $opengraph
 * @property-read \common\components\IpGeoBase      $ipgeobase
 * @property-read \common\components\Environment    $env
 * @property-read noam148\imageresize\ImageResize   $imageresize
 * @property-read \common\components\YandexGeo      $yandexGeo
 */
class Application {
}