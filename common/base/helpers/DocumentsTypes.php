<?php
namespace common\base\helpers;

/**
 * Хэлпер-сборник идентификаторов типова документов по 1с/веб-базе.
 *
 * @author Казанцев Александр <kazancev.al@dns-shop.ru>
 */
class DocumentsTypes {
	/** Виртуальная корзина (для товаров каталога). */
	const VIRTUAL_BASKET = 10130;

	/** Расходная накладная (для товаров каталога). */
	const INVOICE = 130;

	/** Счёт. */
	const BILL = 127;

	/** Виртуальная корзина уценки. */
	const MARKDOWN_BASKET = 10133;

	/** Документ - продажа уценки. */
	const MARKDOWN_SALE = 133;

	/** Документ оплаты. */
	const PAYMENT_DOCUMENT = 248;

	/** Предоплата ремонта (Сервисный центр ДНС) */
	const REPAIR_PREPAY     = 300;
}