<?php
namespace common\base\helpers;

use yii\base\InvalidParamException;

/**
 * Помощник по работе с объектами.
 *
 *
 */
class ObjectHelper {

	/**
	 * Привести объект к указанному типу.
	 *
	 * Набор аргументов должен совпадать у исходного объекта и объекта, в который он приводится.
	 *
	 * @param mixed  $object        Объект для приведения в другой тип.
	 * @param string $className     Наименование класса (с пространством имен) в тип которого нужно преобразовать указанный объект.
	 *
	 * @return mixed Преобразованный объект в указанный тип или false при ошибке преобразования.
	 *
	 * @throws InvalidParamException
	 *
	 *
	 */
	public static function cast($object, $className) {
		// -- валидируем параметры
		if (!is_object($object)) {
			throw new InvalidParamException('Параметр $object должен является объектом.');
		}
		elseif (!is_string($className)) {
			throw new InvalidParamException('Параметр $class не является строкой.');
		}
		elseif (!class_exists($className)) {
			throw new InvalidParamException('Не найден класс: ' . $className);
		}
		// -- -- --

		// -- делаем приведение типа
		$serializedObject = preg_replace(
			'/^O:\d+:"[^"]++"/',
			'O:' . strlen($className) . ':"' . $className . '"',
			serialize($object)
		);

		$result = @unserialize($serializedObject);
		// -- -- --

		return $result;
	}

	/**
	 * Привести массив объектов к указанному типу.
	 *
	 * Набор аргументов должен совпадать у исходного объекта и объекта, в который он приводится.
	 *
	 * @param array  $objects       Массив объектов для приведения в другой тип.
	 * @param string $className     Наименование класса (с пространством имен), в тип которого нужно преобразовать каждый объект из указанного массива.
	 *
	 * @return array|bool Массив объектов, преобразованных в указанный тип или false при ошибке преобразования.
	 *
	 * @throws InvalidParamException
	 *
	 *
	 */
	public static function castMany(array $objects, $className) {
		// -- валидируем параметры
		if (!is_array($objects) || reset($objects) === false) {
			throw new InvalidParamException('Параметр $object должен является непустым массивом объектов.');

		}
		// -- -- --

		// -- делаем приведение типа
		$castedObjects = [];
		foreach ($objects as $object) {
			$result = static::cast($object, $className);
			if ($result === false) {
				return false;
			}

			$castedObjects[] = $result;
		}
		// -- -- --

		return $castedObjects;
	}

	/**
	 * Скопировать значения атрибутов из одного объекта в другой.
	 *
	 * @param object|array $fromObject    Исходный объект или ассоциативный массив в формате [атрибут => значение].
	 * @param object       $toObject      Конечный объект.
	 *
	 * @return object
	 *
	 *
	 */
	public static function copy($fromObject, $toObject) {
		foreach ($fromObject as $propertyName => $propertyValue) {
			if (property_exists($toObject, $propertyName)) {
				$toObject->$propertyName = $propertyValue;
			}
		}

		return $toObject;
	}

	/**
	 * Скопировать значения атрибутов каждого объекта массива в массив объектов указанного типа.
	 *
	 * @param array          $objects      Массив исходных объектов, данные которых будут использованы для создания объектов указанного типа.
	 * @param string         $className    Наименование класса (с пространством имен) для создания конечного массива объектов.
	 * @param string|null    $indexBy      Наименование атрибута, по значениям которого проиндексировать конечный массив (при null - не индексировать).
	 *
	 * @return array
	 *
	 *
	 */
	public static function copyMany(array $objects, $className, $indexBy = null) {
		$result = [];

		foreach ($objects as $srcObject) {
			$destObject = new $className();
			$destObject = ObjectHelper::copy($srcObject, $destObject);

			if (null === $indexBy) {
				$result[] = $destObject;
			}
			else {
				$result[$destObject->$indexBy] = $destObject;
			}
		}

		return $result;
	}
}