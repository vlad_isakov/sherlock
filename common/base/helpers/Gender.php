<?php

namespace common\base\helpers;

/**
 * Хелпер по работе с полом.
 *
 * Хелпер нужен, т.к. у нас нет справочника полов и не будет (не нужен);
 * Необходимо знать конкретные идентификаторы пола для форм и валидации.
 *
 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
 */
class Gender {
	/** Неизвестный пол */
	const UNKNOWN	 = 0;

	/** Мужской пол */
	const MALE		 = 1;

	/** Женский пол */
	const FEMALE	 = 2;

	/** @var string[] Названия полов в формате [идентификатор пола => название] */
	protected static $genders = [
		self::UNKNOWN	 => '-',
		self::MALE		 => 'Мужской',
		self::FEMALE	 => 'Женский'
	];

	/**
	 * Получить список полов (для gui)
	 *
	 * @param bool $withUnknown указать true, чтобы включить в массив элемент с неизвестным полом (прочерком)
	 *
	 * @return array массив в формате [идентификатор пола => название пола]
	 *
	 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
	 */
	public static function getGendersList($withUnknown = false) {
		$result = static::$genders;

		if (!$withUnknown) {
			unset($result[static::UNKNOWN]);
		}

		return $result;
	}

	/**
	 * Получить массив с возможными идентификаторами пола (для валидации)
	 *
	 * @return int[]
	 *
	 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
	 */
	public static function getRange() {
		return array_keys(static::$genders);
	}
}
