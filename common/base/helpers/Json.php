<?php
namespace common\base\helpers;

use stdClass;

/**
 * Вспомогательный класс для работы с JSON-объектами.
 *
 *
 */
class Json {
	/**
	 * Конвертация данных из строки в объект PHP.
	 * Дело в том, что json_decode возвращает или объект, или массив.
	 * Получается, нельзя получить объект с массивом одновременно.
	 *
	 * Внимание!
	 * Данные передаются по ссылке, чтобы не занимать дополнительную память.
	 * Выявлено, что память сильно расходуется - это происходит при больших объёмах данных.
	 *
	 * @param array|string $data Данные, которые необходимо сконвертировать
	 *
	 *
	 */
	public static function decode(&$data) {
		if (false === is_array($data)) {
			$data2 = @json_decode($data, true);

			if (false === is_array($data2)) {
				return;
			}

			$data = $data2;
			$data2 = null;// Если не обнулить, то будет два больших массива, из-за этого будут проблемы с памятью
		}

		$data = static::convertObjectArray($data);
	}

	/**
	 * Конвертация массива в объект.
	 *
	 * @param mixed $data
	 *
	 * @return array|stdClass
	 *
	 *
	 */
	protected static function convertObjectArray($data) {
		if (false === is_array($data)) {
			return $data;
		}

		$isObject = true;
		$attributesCount = 0;
		foreach ($data as $key => $value) {
			$attributesCount++;

			if (1 === preg_match('/^[0-9\/\-+=~\.,\?]/', $key)) {
				$isObject = false;
				break;
			}
			elseif (1 === preg_match('/[\/\-+=~\.,\?]/', $key)) {
				$isObject = false;
				break;
			}
		}

		if (0 === $attributesCount) {
			return [];
		}

		if (true === $isObject) {
			$result = new stdClass();
			foreach ($data as $key => $value) {
				$result->$key = static::convertObjectArray($value);
			}
		}
		else {
			$result = [];
			foreach ($data as $key => $value) {
				$result[$key] = static::convertObjectArray($value);
			}
		}

		return $result;
	}
}