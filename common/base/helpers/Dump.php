<?php
namespace common\base\helpers;

use Exception;
use yii\helpers\VarDumper;

/**
 *
 */
class Dump extends VarDumper {
	/**
	 * @inheritdoc
	 *
	 *
	 */
	public static function d($var, $depth = 10, $highlight = true) {
		echo self::dumpAsString($var, $depth, $highlight);
	}

	/**
	 * @inheritdoc
	 *
	 * @author isakov.v
	 * Дамп с остановкой
	 */
	public static function dDie($var, $depth = 10, $highlight = true) {
		echo self::dumpAsString($var, $depth, $highlight);
		die;
	}

	/**
	 * Сбор и возврат стека вызовов функций.
	 *
	 *
	 *
	 * @param Exception $e
	 * @return string
	 */
	public static function backtrace($e = null) {
		$result = [];

		if ($e !== null) {
			$result[] = $e->getMessage() . PHP_EOL;
		}

		$rows = debug_backtrace();
		array_shift($rows);

		foreach ($rows as $row) {
			if (isset($row['class']) && $row['class'] != '') {
				$result[] = $row['class'] . '::';
			}

			$result[] = $row['function'] . '(';

			$first = true;
			foreach ($row['args'] as $arg) {
				if ($first) {
					$first = false;
				} else {
					$result[] = ', ';
				}

				$s = print_r($arg, true);
				if (strlen($s) > 1000) {
					$s = '[long var] of ' . gettype($arg) . ' type';
				}

				$result[] = $s;
			}

			$result[] = ') at ' . (isset($row['file']) ? $row['file'] : '-') . ':' . (isset($row['line']) ? $row['line'] : '-') . PHP_EOL;
		}

		return implode('', $result);
	}

	/**
	 * Выводит информацию о случившемся исключении в стиле Java.
	 * Этот метод лучше, чем $e->getTraceAsString, потому как захватывает больше данных.
	 *
	 *
	 *
	 * @param \Exception $e Случившееся исключение
	 * @param array $seen Использовать не надо! Нужно для служебного рекурсивного вызова.
	 * @return string Plain-текст с информацией об исключении с переносами строк
	 */
	public static function getTrace($e, $seen = null) {
		$starter = $seen ? 'Caused by: ' : '';

		$result = [];
		if (!$seen) $seen = [];

		$trace  = $e->getTrace();
		$prev   = $e->getPrevious();

		$result[] = sprintf('%s%s: %s', $starter, get_class($e), $e->getMessage());

		$file = $e->getFile();
		$line = $e->getLine();
		while (true) {
			$current = $file . ':' . $line;

			if (is_array($seen) && in_array($current, $seen)) {
				$result[] = sprintf(' ... %d more', count($trace) + 1);
				break;
			}

			$result[] = sprintf(' at %s%s%s(%s%s%s)',
				count($trace) && array_key_exists('class', $trace[0]) ? str_replace('\\', '.', $trace[0]['class']) : '',
				count($trace) && array_key_exists('class', $trace[0]) && array_key_exists('function', $trace[0]) ? '.' : '',
				count($trace) && array_key_exists('function', $trace[0]) ? str_replace('\\', '.', $trace[0]['function']) : '(main)',
				$line === null ? $file : basename($file),
				$line === null ? '' : ':',
				$line === null ? '' : $line
			);

			if (is_array($seen)) {
				$seen[] = $file . ':' . $line;
			}

			if (!count($trace)) {
				break;
			}

			$file = array_key_exists('file', $trace[0]) ? $trace[0]['file'] : 'Unknown Source';
			$line = array_key_exists('file', $trace[0]) && array_key_exists('line', $trace[0]) && $trace[0]['line'] ? $trace[0]['line'] : null;
			array_shift($trace);
		}

		$result = implode("\n", $result);

		if ($prev) {
			$result .= "\n" . self::getTrace($prev, $seen);
		}

		return $result;
	}

	/**
	 * Возвращает дамп в виде стуктурированного текста.
	 * Скажем так: это аналог стандартной функции print_r с более удобным форматированием.
	 *
	 *
	 *
	 * @param mixed $subject
	 * @param bool $return
	 * @param int $depth
	 * @param array $refChain
	 * @return string
	 */
	public static function print_r($subject, $return = false, $depth = 1, $refChain = []) {
		if ($depth > 20) return '* DEPTH GREATER THAN 20 *';

		$result = [];

		if (is_object($subject)) {
			foreach ($refChain as $refVal) {
				if ($refVal === $subject) {
					return '* RECURSION *';
				}
			}

			array_push($refChain, $subject);

			$result[] = get_class($subject) . ' (';

			$subject = (array)$subject;

			// -- Определяем наиболее длинный ключ
			$maxKeyLength = 0;
			foreach (array_keys($subject) as $key) {
				if (mb_strlen($key) <= $maxKeyLength) continue;
				$maxKeyLength = mb_strlen($key);
			}
			// -- -- -- --

			foreach ($subject as $key => $val) {
				$rowLabel = '';

				if ($key[0] == "\0") {
					$keyParts = explode("\0", $key);
					$rowLabel .= $keyParts[2] . (($keyParts[1] == '*') ? ':protected' : ':private');
				} else {
					$rowLabel .= $key;
				}

				$rowValue = self::print_r($val, $return, $depth + 1, $refChain);

				$result[] = str_repeat(' ', $depth * 4) . '\'' . $rowLabel . '\'' . str_repeat(' ', $maxKeyLength - mb_strlen($rowLabel)) . ' => ' . $rowValue;
			}

			$result[] = str_repeat(' ', ($depth - 1) * 4) . ')';
			array_pop($refChain);
		} else if (is_array($subject)) {
			if (count(array_keys($subject)) == 0) {
				$result[] = 'array()';
			} else {
				$result[] = 'array(';

				// -- Определяем наиболее длинный ключ
				$maxKeyLength = 0;
				foreach (array_keys($subject) as $key) {
					if (mb_strlen($key) <= $maxKeyLength) continue;
					$maxKeyLength = mb_strlen($key);
				}
				// -- -- -- --

				foreach ($subject as $key => $val) {
					$result[] = str_repeat(' ', $depth * 4) . '\'' . $key . '\'' . str_repeat(' ', $maxKeyLength - mb_strlen($key)) . ' => ' . self::print_r($val, $return, $depth + 1, $refChain);
				}

				$result[] = str_repeat(' ', ($depth - 1) * 4) . ')';
			}

		} else if (is_bool($subject)) {
			$result[] = ($subject === true ? 'true' : 'false');
		} else if ($subject === null) {
			$result[] = 'null';
		} else if (is_float($subject) || is_int($subject)) {
			$result[] = $subject;
		} else {
			$result[] = '\'' . $subject . '\'';
		}

		$result = implode(PHP_EOL, $result);

		if ($depth == 1 && $return !== true) echo $result;
		return $result;
	}
}