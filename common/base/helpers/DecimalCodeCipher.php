<?php
namespace common\base\helpers;

use yii\base\InvalidParamException;

/**
 * Помощник для шифрования простых кодов, заданных в виде десятичного целого числа.
 *
 * Пример такого кода: код подтверждения, высланный по SMS - 0543.
 * Пример зашифрованного кода: B894IA4.
 * Назначение помощника - шифровать код для сохранения в БД, но при отправке его по SMS (Email) клиенту - расшифровывать.
 * Зашифрованный код обрамляется символами, чтобы можно было однозначно определить, что этот код зашифрован - такое обозначение называется токеном.
 * Пример токена: {{B894IA4}}
 *
 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
 */
class DecimalCodeCipher {

	/** Обозначение начала токена */
	const TOKEN_BRACE_BEGIN    = '{{';
	/** Обозначение конца токена */
	const TOKEN_BRACE_END      = '}}';

	/**
	 * Зашифровать целочисленное число.
	 *
	 * @param string $decimal Строка, содержащая десятичное целое число (допускается ведущий ноль).
	 *
	 * @return string
	 *
	 * @throws InvalidParamException
	 *
	 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
	 */
	public static function encrypt($decimal) {
		// -- Проверяем, что число - десятичное целочисленное (трюк с единицей нужен из-за того, что допускаются числа, начинающиеся с нуля)
		if (('1' . $decimal) !== (string)(int)('1' . $decimal)) {
			throw new InvalidParamException('Допускаются только десятичные целочисленные числа.');
		}
		// -- -- --

		// Максимально-допустимое число цифр в исходном числе
		$maxDecimalLength = 8;

		// Узнаем, сколько у исходного числа цифр
		$length = strlen($decimal);

		if ($length > $maxDecimalLength) {
			throw new InvalidParamException('Допускается число не более ' . $maxDecimalLength . ' цифр.');
		}

		// Дополняем спереди нулями и ставим единицу, чтобы получить большое число
		$code = '1' . str_pad($decimal, $maxDecimalLength + 1, '0', STR_PAD_LEFT);

		// Сохраняем в последнем символе количество цифр исходного числа
		$code .= $length;

		// Преобразуем в 31-ричную систему счисления из десятичной
		$code = base_convert($code, 10, 31);

		// Преобразуем в верхний регистр, чтобы код получился более наглядным
		$code = strtoupper($code);

		return $code;
	}

	/**
	 * Расшифровать целочисленное число.
	 *
	 * @param string $code Код, который был получен преобразованием с помощью encryptDecimal().
	 *
	 * @return string
	 *
	 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
	 */
	public static function decrypt($code) {
		// Преобразуем в десятичную систему счисления из 31-ричной
		$code = base_convert($code, 31, 10);

		// Узнаем, сколько у искомого числа цифр (хранится в последнем символе строки)
		$length = substr($code, -1, 1);

		// Избавляемся от последнего символа строки
		$code = substr($code, 0, strlen($code) - 1);

		// Извлекаем искомую подстроку
		$code = substr($code, -$length, $length);

		return $code;
	}

	/**
	 * Сделать токен из строки, представляющий зашифрованный код.
	 *
	 * Пример. Зашифрованная строка B894IA4 на выходе станет такой: {{B894IA4}}
	 *
	 * @param string $string Строка, представляющая зашифрованный код.
	 *
	 * @return string
	 *
	 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
	 */
	public static function createToken($string) {
		return static::TOKEN_BRACE_BEGIN . $string . static::TOKEN_BRACE_END;
	}

	/**
	 * Проверить, содержит ли строка токен.
	 *
	 * Пример токена: {{B894IA4}}
	 *
	 * @param string $string
	 *
	 * @return bool
	 *
	 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
	 */
	public static function containsToken($string) {
		return (false !== strpos($string, static::TOKEN_BRACE_BEGIN) && false !== strpos($string, static::TOKEN_BRACE_END));
	}

	/**
	 * Расшифровать токен, содержащейся в строке (расшифровывает только первый найденный токен в строке).
	 *
	 * @param string $string Исходная строка, в которой содержится токен.
	 *
	 * @return bool|string
	 *
	 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
	 */
	public static function decryptTokenizedString($string) {
		if (!static::containsToken($string)) {
			return false;
		}

		// -- Находим токен в переданной строке
		$tokenStartPosition = strpos($string, static::TOKEN_BRACE_BEGIN);
		$tokenEndPosition   = strpos($string, static::TOKEN_BRACE_END);
		// -- -- --

		// Извлекаем из строки токен
		$token = substr($string, $tokenStartPosition, $tokenEndPosition - $tokenStartPosition + strlen(static::TOKEN_BRACE_END));

		// -- Извлекаем из токена код и расшифровываем его
		$code = ltrim($token, static::TOKEN_BRACE_BEGIN);
		$code = rtrim($code, static::TOKEN_BRACE_END);
		$code = static::decrypt($code);
		// -- -- --

		// Заменяем токен из исходной строки на расшифрованный код
		$code = str_replace($token, $code, $string);

		return $code;
	}
}