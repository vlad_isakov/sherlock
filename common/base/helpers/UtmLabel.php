<?php
namespace common\base\helpers;

use Yii;

/**
 * Класс-помощник по работе с UTM-метками.
 *
 * @link http://www.shopolog.ru/metodichka/analytics/ischerpyvayushchee-rukovodstvo-po-ispolzovaniyu-utm-metok/ Исчерпывающее руководство по использованию UTM-меток.
 *
 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
 */
class UtmLabel {
	/** Ключ для хранения меток в сессии */
	const UTM_LABEL_STORAGE_SESSION_KEY = 'utmLabelStorage';

	/** GET-параметр запроса, содержащего UTM-метку - utm_source. */
	const ACTION_PARAM_UTM_SOURCE = 'utm_source';

	/** GET-параметр запроса, содержащего UTM-метку - utm_medium. */
	const ACTION_PARAM_UTM_MEDIUM = 'utm_medium';

	/** GET-параметр запроса, содержащего UTM-метку - utm_campaign. */
	const ACTION_PARAM_UTM_CAMPAIGN = 'utm_campaign';

	/** GET-параметр запроса, содержащего UTM-метку - utm_content. */
	const ACTION_PARAM_UTM_CONTENT = 'utm_content';

	/** GET-параметр запроса, содержащего UTM-метку - utm_term. */
	const ACTION_PARAM_UTM_TERM = 'utm_term';

	/** GET-параметр запроса, содержащего UTM-метку - yclid. */
	const ACTION_PARAM_UTM_YCLID = 'yclid';

	/** GET-параметр запроса, содержащего UTM-метку - gclid. */
	const ACTION_PARAM_UTM_GCLID = 'gclid';

	/**
	 * Получить список UTM-меток.
	 *
	 * @return string[]
	 *
	 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
	 */
	public static function getLabels() {
		return [
			static::ACTION_PARAM_UTM_SOURCE,
			static::ACTION_PARAM_UTM_MEDIUM,
			static::ACTION_PARAM_UTM_CAMPAIGN,
			static::ACTION_PARAM_UTM_CONTENT,
			static::ACTION_PARAM_UTM_TERM,
			static::ACTION_PARAM_UTM_YCLID,
			static::ACTION_PARAM_UTM_GCLID,
		];
	}

	/**
	 * Получить UTM-метки с их значениями из GET-запроса.
	 *
	 * @return string[] Массив в формате [название метки => значение метки].
	 *
	 * @author Максим Трофимов <trofimov.mv@dns-shop.ru>
	 */
	public static function getParams() {
		$queryParams = Yii::$app->request->get();

		$result = [];
		foreach (static::getLabels() as $label) {
			if (array_key_exists($label, $queryParams)) {
				$result[$label] = $queryParams[$label];
			}
		}

		return $result;
	}

	/**
	 * Сохранение UTM-меток в сессию, если таковые имеются.
	 *
	 * @author Дегтярев Илья <degtyarev.iv@dns-shop.ru>
	 */
	public static function saveLabelsToSession() {
		$labels = static::getParams();

		if ([] !== $labels) {
			Yii::$app->session->set(static::UTM_LABEL_STORAGE_SESSION_KEY, $labels);
		}
	}

	/**
	 * Получение ранее сохраненных в сессии UTM-меток.
	 *
	 * @return string[]
	 *
	 * @author Дегтярев Илья <degtyarev.iv@dns-shop.ru>
	 */
	public static function getLabelsFromSession() {
		return Yii::$app->session->get(static::UTM_LABEL_STORAGE_SESSION_KEY, []);
	}
}