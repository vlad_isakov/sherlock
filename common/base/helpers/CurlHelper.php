<?php
namespace common\base\helpers;

/**
 * Вспомогательный класс для работы с curl.
 *
 * @author Дегтярев Илья <degtyarev.iv@dns-shop.ru>
 */
class CurlHelper {

	/**
	 * Выполнение curl_exec() с конвертацией кодировки ответа в UTF-8.
	 *
	 * @param resource $curl                   Дескриптор cURL (Результат curl_init())
	 * @param bool     $ignoreConversionErrors Игнорировать ли неконвертируемые символы
	 *
	 * @return mixed|string
	 *
	 * @author Дегтярев Илья <degtyarev.iv@dns-shop.ru>
	 */
	public static function exec($curl, $ignoreConversionErrors = true) {
		$curlResult = curl_exec($curl);

		if (!is_string($curlResult)) {
			return $curlResult;
		}

		$content_type = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
		$charset = null;

		// Пытаемся получить кодировку через HTTP Content-Type: header
		preg_match('@([\w/+]+)(;\s*charset=(?P<charset>\S+))?@i', $content_type, $matches);
		if (array_key_exists('charset', $matches)) {
			$charset = $matches['charset'];
		}
		// -- -- -- --

		// Пытаемся получить кодировку через <meta> элемент на странице
		if (null === $charset) {
			preg_match('@<meta\s+http-equiv="Content-Type"\s+content="([\w/]+)(;\s*charset=(?P<charset>[^\s"]+))?@i', $curlResult, $matches);
			if (array_key_exists('charset', $matches)) {
				$charset = $matches['charset'];
			}
		}
		// -- -- -- --

		// Пытаемся получить кодировку через <xml> тег на странице
		if (null === $charset) {
			preg_match('@<\?xml.+encoding="(?P<charset>[^\s"]+)@si', $curlResult, $matches);
			if (array_key_exists('charset', $matches)) {
				$charset = $matches['charset'];
			}
		}
		// -- -- -- --

		// Пытаемся получить кодировку средтвами mb_detect_encoding
		if (null === $charset) {
			$encoding = mb_detect_encoding($curlResult);
			if (false !== $encoding) {
				$charset = $encoding;
			}
		}
		// -- -- -- --

		// Конвертируем в UTF-8, если исходная кодировка не UTF-8
		if (null !== $charset && strtoupper($charset) !== 'UTF-8') {
			$resultCharset = $ignoreConversionErrors ? 'UTF-8//IGNORE' : 'UTF-8';
			$curlResult    = iconv($charset, $resultCharset, $curlResult);
		}
		// -- -- -- --

		return $curlResult;
	}
}