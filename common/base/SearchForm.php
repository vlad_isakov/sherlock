<?php
namespace common\base;

use common\base\validators\IntegerValidator;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\Pagination;
use yii\validators\EachValidator;
use yii\validators\Validator;

/**
 * Базовый класс для форм поиска моделей.
 *
 *
 *
 */
abstract class SearchForm extends Model {
	/** @var array Массив с формами по-умолчанию, чтобы каждый раз не создавать. */
	private static $_defaultForms = [];

	/** @var ActiveRecord Класс модели, с которой связана форма. */
	protected $_model;

	/** @var Pagination Компонент для постраничной навигации. */
	public $pagination;

	/** @var ActiveRecord[] Массив с найденными моделями. */
	public $models;

	/** @var int Количество элементов на страницу (постраничная навигация). */
	public $pageSize;
	const ATTR_PAGE_SIZE = 'pageSize';

	/** @var array Доступные варианты разбивки постраничной навигации. */
	public $pageSizeDropDown = [
		  50 =>   50,
		 100 =>  100,
		 250 =>  250,
		 500 =>  500,
		1000 => 1000,
		2500 => 2500,
	];

	/** @var int Выбранный вариант сортировки. */
	public $sortType;
	const ATTR_SORT_TYPE = 'sortType';

	/** @var array Доступные варианты сортировки. */
	public $sortTypeDropDown = [];

	/** @var int В каком порядке сортировка. */
	public $sortInvert;
	const ATTR_SORT_INVERT = 'sortInvert';

	/** Константа порядка сортировки: в нормальном порядке (А-Я). */
	const SORT_INVERT_FALSE = 0;
	/** Константа порядка сортировки: в обратном порядке (Я-А). */
	const SORT_INVERT_TRUE = 1;

	/** @var array Список доступных вариантов для порядка сортировки. */
	public $sortInvertDropDown;

	/** Константа выбора: "НЕ УЧИТЫВАТЬ". */
	const CHOOSE_EMPTY = '';
	/** Константа выбора: "НЕТ". */
	const CHOOSE_NO = -1;
	/** Константа выбора: "ДА". */
	const CHOOSE_YES = 1;

	/** @var array Список вариантов выпадающего списка. */
	public $chooseYesNo;

	/** Константа короткого имени (чтобы как можно сильнее сократить строку адреса). */
	const SHORT_FORM_NAME = 's';

	/**
	 * Конструктор формы.
	 *
	 * @param string $modelClassName Название класса модели, с которой связана форма
	 *
	 *
	 */
	public function __construct($modelClassName) {
		$this->_model = new $modelClassName;

		// -- Заполняем данные
		$this->sortInvertDropDown = [
			static::SORT_INVERT_FALSE   => 'От А до Я',
			static::SORT_INVERT_TRUE    => 'От Я до А',
		];

		$this->chooseYesNo = [
			static::CHOOSE_EMPTY    => '- - -',
			static::CHOOSE_NO       => 'Нет',
			static::CHOOSE_YES      => 'Да',
		];
		// -- -- -- --

		// -- Определяем параметры по-умолчанию
		$this->sortInvert   = static::SORT_INVERT_FALSE;
		$this->pageSize     = 50;
		// -- -- -- --

		parent::__construct();
	}

	/**
	 * Получение "пустой" формы.
	 * Необходимо, в частности, чтобы получить значения атрибутов по-умолчанию.
	 *
	 * @param string $modelClassName Название класса модели, с которой связана форма
	 *
	 * @return static
	 *
	 *
	 */
	private static function getDefaultForm($modelClassName) {
		// -- Чтобы каждый раз не создавать новую форму
		if (false === array_key_exists(static::class, self::$_defaultForms)) {
			self::$_defaultForms[static::class] = new static($modelClassName);
		}
		// -- -- -- --

		return self::$_defaultForms[static::class];
	}

	/**
	 * Присваивание форме атрибутов.
	 * Перекрываем родительский метод, так как нам нужна дополнительная обработка.
	 *
	 * @param array $values
	 *
	 * @param bool $safeOnly
	 *
	 *
	 */
	public function setAttributes($values, $safeOnly = true) {
		parent::setAttributes($values, $safeOnly);

		// -- Форматируем значения
		foreach ($this->safeAttributes() as $attribute) {
			$this->formatAttributeValue($attribute);
		}
		// -- -- -- --
	}

	/**
	 * Дополнительное форматирование указанного значения.
	 *
	 * @param string $attribute Название атрибута, значение, которого необходимо отформатировать
	 *
	 *
	 * @author Канатников Андрей <kanatnikov.as@dns-shop.ru>
	 */
	protected function formatAttributeValue($attribute) {
		$value = $this->$attribute;

		if (false === is_array($value)) {
			$value = trim($value);
		};

		if ('' === $value) {
			$this->$attribute = null;

			return;
		}

		// -- Проверяем есть ли валидаторы для которых нужно приводить тип данных
		$formatToInteger      = false;
		$formatToIntegerArray = false;

		$rules = $this->getActiveValidators($attribute);
		foreach ($rules as $rule) {
			if ($rule instanceof IntegerValidator) { // Если это правило валидации целых чисел и в поле цифры
				$formatToInteger = true;
			}
			elseif ($rule instanceof EachValidator) { // Если это правило валидации массива
				$nestedRule = $rule->rule; // Получаем внутренне правило валидатора массива
				if (is_array($nestedRule) && isset($nestedRule[0])) { // Если внутренне правило заполнено и указан его тип
					$nestedRule = Validator::createValidator($nestedRule[0], $this, $this->attributes, array_slice($nestedRule, 1)); // Создаем объект валидатора
					if ($nestedRule instanceof integerValidator) { // Если внутренний валидатор для целых чисел и в поле цифры
						$formatToIntegerArray = true;
					}
				}
			}
		}
		// -- -- -- --

		if ($formatToInteger && 1 === preg_match('/^-{0,1}([1-9]+\d*|0)$/', $value)) {
			$value = (int)$value;
		}
		elseif ($formatToIntegerArray) {
			$valueArray = (array)$value;
			foreach ($valueArray as $key => $valuePart) {
				if (1 === preg_match('/^-{0,1}([1-9]+\d*|0)$/', $valuePart)) {
					$valueArray[$key] = (int)$valuePart;
				}
			}
			$value = $valueArray;
		}

		$this->$attribute = $value;
	}

	/**
	 * Действие перед поиском.
	 * В этот метод необходимо добавить заполнение данных, например, для выпадающих списков.
	 *
	 *
	 */
	protected function beforeSearch() {
		//
	}

	/**
	 * Действия после поиска
	 *
	 * @author Канатников Андрей <kanatnikov.as@dns-shop.ru>
	 */
	protected function afterSearch() {
		
	}
	
	/**
	 * Генерация критерии для отбора данных.
	 *
	 * @return ActiveQuery|null Возвращает компонент для поиска или NULL, если поиск выполнять не надо
	 *
	 *
	 */
	protected function getDbCriteria() {
		return $this->_model->find();
	}

	/**
	 * Выполняем поиск моделей - все данные из поиска записываются в атрибуты.
	 *
	 *
	 */
	public function search() {
		$this->load(Yii::$app->request->get(), static::SHORT_FORM_NAME);

		$this->beforeSearch();

		// -- Определяем критерию для отбора данных
		if (false === $this->validate()) {
			$criteria = null;
		}
		else {
			$criteria = $this->getDbCriteria();

			if (null !== $criteria) {
				$criteria->from($this->_model->tableName() . ' AS t');

				// -- Определяем параметры сортировки
				if (static::SORT_INVERT_TRUE === $this->sortInvert) {
					if (0 !== count($criteria->orderBy)) {
						$ak = array_keys($criteria->orderBy);
						$ak = $ak[0];
						if (SORT_ASC === $criteria->orderBy[$ak]) {
							$criteria->orderBy[$ak] = SORT_DESC;
						}
						else {
							$criteria->orderBy[$ak] = SORT_ASC;
						}
					}
				}
				// -- -- -- --

				// -- Инициализируем постраничный вывод
				$this->pagination = new Pagination();
				$countCriteria = clone $criteria;
				$countCriteria->prepare(Yii::$app->db->getQueryBuilder()); // Если в релейшене стоит orderBy, то он применяется в prepare(), в count запрос попадает сортировка и он падает с ошибкой.
				$this->pagination->totalCount = $countCriteria->orderBy([])->count();
				$this->pagination->pageSize = $this->pageSize;// Количество записей на странице
				$criteria->limit($this->pageSize);
				$criteria->offset($this->pagination->offset);
				// -- -- -- --

				$this->models = $criteria->all();
			}
		}
		// -- -- -- --

		// -- Если критерия не указана, то ничего и не находим
		if (null === $criteria) {
			$this->pagination = new Pagination();
			$this->pagination->totalCount = 0;
			$this->models = [];
		}
		// -- -- -- --

		$this->afterSearch();
	}

	/**
	 * Пытаемся сократить URL адрес путём удаления оттуда значений по умолчанию для модели.
	 *
	 *
	 *
	 * @return false|array Если сократить нельзя, то false, иначе параметры нового URL адреса
	 */
	public function getShortUrl() {
		// -- Проверяем, есть ли данные в запросе
		$query = Yii::$app->request->get($this->formName());

		if (false === is_array($query)) {
			$query = Yii::$app->request->get(static::SHORT_FORM_NAME);
			if (false === is_array($query)) {
				return false;
			}
			$canReduceUrl = false;
		}
		else {
			$canReduceUrl = true;// Если в ссылке используется длинное название формы, то обязательно надо укоротить
		}
		// -- -- -- --

		$attributes = static::getDefaultForm(get_class($this->_model))->activeAttributes();
		foreach ($query as $attribute => $value) {
			if (false === in_array($attribute, $attributes)) {
				continue;
			}

			if (static::getDefaultForm(get_class($this->_model))->$attribute == $value) {
				unset($query[$attribute]);
				$canReduceUrl = true;
			}
		}

		if (false === $canReduceUrl) {
			return false;
		}

		unset($_GET[$this->formName()]);// Удаляем название формы, так как строкой ниже мы его заменим на более короткое
		$_GET[static::SHORT_FORM_NAME] = $query;

		return [''] + $_GET;
	}

	/**
	 * Получение атрибутов для URL-адреса.
	 * Необходимо, чтобы проверить наличие указанных атрибутов, а также отсеить те, что являются по-умолчанию.
	 *
	 *
	 *
	 * @param string $modelClassName Название класса модели, с которой связана форма
	 * @param string $route Адрес страницы, на которой располагается форма
	 * @param array $params Список параметров вида [name => value , name => value]
	 * @param bool $invalidException Необходимо ли выбросить исключение, если указанный атрибут не найден
	 * @return array
	 * @throws Exception
	 */
	public static function getUrl($modelClassName, $route, array $params, $invalidException = true) {
		$safeAttributes = static::getDefaultForm($modelClassName)->getAttributes();

		// -- Проходимся по всем параметрам и проверяем их
		foreach ($params as $paramName => $paramValue) {
			$paramName = preg_replace('/\[[^\[\]]*\]$/', '', $paramName);// Корректировка, если указано, что это массив
			if (array_key_exists($paramName, $safeAttributes)) {
				if (true === $invalidException) {
					throw new Exception('Form ' . static::class . ' doesn\'t have attribute: ' . $paramName . '.');
				}
				unset($params[$paramName]);
			}
			else {
				if (false === is_array($paramValue)) {
					if (0 === strcmp($paramValue, static::getDefaultForm($modelClassName)->$paramName)) {
						unset($params[$paramName]);
					}
				}
			}
		}
		// -- -- -- --

		return Yii::$app->urlManager->createUrl([$route, static::SHORT_FORM_NAME => $params]);
	}
}