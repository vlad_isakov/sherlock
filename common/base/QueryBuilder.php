<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace common\base;

use yii\db\Expression;
use yii\db\Query;

/**
 * QueryBuilder is the query builder for MySQL databases.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class QueryBuilder extends \yii\db\mysql\QueryBuilder {
	/**
	 * @inheritdoc
	 */
	public function insert($table, $columns, &$params) {
		$schema = $this->db->getSchema();

		if (($tableSchema = $schema->getTableSchema($table)) !== null) {
			$columnSchemas = $tableSchema->columns;
		}
		else {
			$columnSchemas = [];
		}
		$names = [];
		$placeholders = [];
		$values = ' DEFAULT VALUES';
		if ($columns instanceof \yii\db\Query) {
			list($names, $values) = $this->prepareInsertSelectSubQuery($columns, $schema);
		}
		else {
			foreach ($columns as $name => $value) {
				$names[] = $schema->quoteColumnName($name);
				if ($value instanceof Expression) {
					$placeholders[] = $value->expression;
					foreach ($value->params as $n => $v) {
						$params[$n] = $v;
					}
				}
				elseif ($value instanceof \yii\db\Query) {
					list($sql, $params) = $this->build($value, $params);
					$placeholders[] = "($sql)";
				}
				else {
					$phName = self::PARAM_PREFIX . count($params);
					$placeholders[] = $phName;
					$params[$phName] = !is_array($value) && isset($columnSchemas[$name]) ? $columnSchemas[$name]->dbTypecast($value) : $value;
				}
			}
			if (empty($names) && $tableSchema !== null) {
				$columns = !empty($tableSchema->primaryKey) ? $tableSchema->primaryKey : [reset($tableSchema->columns)->name];
				foreach ($columns as $name) {
					$names[] = $schema->quoteColumnName($name);
					$placeholders[] = 'DEFAULT';
				}
			}
		}

		return 'INSERT INTO ' . $schema->quoteTableName($table)
			. (!empty($names) ? ' (' . implode(', ', $names) . ')' : '')
			. (!empty($placeholders) ? ' VALUES (' . implode(', ', $placeholders) . ')' : $values);
	}

	/**
	 * Creates an UPDATE SQL statement.
	 * For example,
	 *
	 * ```php
	 * $params = [];
	 * $sql = $queryBuilder->update('user', ['status' => 1], 'age > 30', $params);
	 * ```
	 *
	 * The method will properly escape the table and column names.
	 *
	 * @param string       $table     the table to be updated.
	 * @param array        $columns   the column data (name => value) to be updated.
	 * @param array|string $condition the condition that will be put in the WHERE part. Please
	 *                                refer to [[Query::where()]] on how to specify condition.
	 * @param array        $params    the binding parameters that will be modified by this method
	 *                                so that they can be bound to the DB command later.
	 *
	 * @return string the UPDATE SQL
	 */
	public function update($table, $columns, $condition, &$params) {
		if (($tableSchema = $this->db->getTableSchema($table)) !== null) {
			$columnSchemas = $tableSchema->columns;
		}
		else {
			$columnSchemas = [];
		}

		$lines = [];
		foreach ($columns as $name => $value) {
			if ($value instanceof Expression) {
				$lines[] = $this->db->quoteColumnName($name) . '=' . $value->expression;
				foreach ($value->params as $n => $v) {
					$params[$n] = $v;
				}
			}
			else {
				$phName = self::PARAM_PREFIX . count($params);
				$lines[] = $this->db->quoteColumnName($name) . '=' . $phName;
				$params[$phName] = !is_array($value) && isset($columnSchemas[$name]) ? $columnSchemas[$name]->dbTypecast($value) : $value;
			}
		}

		$sql = 'UPDATE ' . $this->db->quoteTableName($table) . ' SET ' . implode(', ', $lines);
		$where = $this->buildWhere($condition, $params);

		return $where === '' ? $sql : $sql . ' ' . $where;
	}

	/**
	 * Creates a DELETE SQL statement.
	 * For example,
	 *
	 * ```php
	 * $sql = $queryBuilder->delete('user', 'status = 0');
	 * ```
	 *
	 * The method will properly escape the table and column names.
	 *
	 * @param string       $table     the table where the data will be deleted from.
	 * @param array|string $condition the condition that will be put in the WHERE part. Please
	 *                                refer to [[Query::where()]] on how to specify condition.
	 * @param array        $params    the binding parameters that will be modified by this method
	 *                                so that they can be bound to the DB command later.
	 *
	 * @return string the DELETE SQL
	 */
	public function delete($table, $condition, &$params) {
		$sql = 'DELETE FROM ' . $this->db->quoteTableName($table);
		$where = $this->buildWhere($condition, $params);

		return $where === '' ? $sql : $sql . ' ' . $where;
	}

	/**
	 * @param string|array $condition
	 * @param array        $params the binding parameters to be populated
	 *
	 * @return string the WHERE clause built from [[Query::$where]].
	 */
	public function buildWhere($condition, &$params) {
		$where = $this->buildCondition($condition, $params);

		return $where === '' ? '' : 'WHERE ' . $where;
	}

	/**
	 * Generates a SELECT SQL statement from a [[Query]] object.
	 *
	 * @param Query $query  the [[Query]] object from which the SQL statement will be generated.
	 * @param array $params the parameters to be bound to the generated SQL statement. These parameters will
	 *                      be included in the result with the additional parameters generated during the query building process.
	 *
	 * @return array the generated SQL statement (the first array element) and the corresponding
	 * parameters to be bound to the SQL statement (the second array element). The parameters returned
	 * include those provided in `$params`.
	 */
	public function build($query, $params = []) {

		$query = $query->prepare($this);

		$params = empty($params) ? $query->params : array_merge($params, $query->params);
		$clauses = [
			$this->buildSelect($query->select, $params, $query->distinct, $query->selectOption),
			$this->buildFrom($query->from, $params),
			$this->buildJoin($query->join, $params),
			$this->buildWhere($query->where, $params),
			$this->buildGroupBy($query->groupBy),
			$this->buildHaving($query->having, $params),
		];

		$sql = implode($this->separator, array_filter($clauses));
		$sql = $this->buildOrderByAndLimit($sql, $query->orderBy, $query->limit, $query->offset);

		if (!empty($query->orderBy)) {
			foreach ($query->orderBy as $expression) {
				if ($expression instanceof Expression) {
					$params = array_merge($params, $expression->params);
				}
			}
		}
		if (!empty($query->groupBy)) {
			foreach ($query->groupBy as $expression) {
				if ($expression instanceof Expression) {
					$params = array_merge($params, $expression->params);
				}
			}
		}

		$union = $this->buildUnion($query->union, $params);
		if ($union !== '') {
			$sql = "($sql){$this->separator}$union";
		}

		return [$sql, $params];
	}
}
