<?php
namespace common\base\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Виджет для добавления переключателя в виде кнопок
 *
 * @author Кривонос Иван <krivonos.iv@dns-shop.ru>
 */
class ButtonToggle extends InputWidget {

	/** Тип переключателя: radio-button */
	const TOGGLE_TYPE_RADIO     = 'radio';
	/** Тип переключателя: checkbox */
	const TOGGLE_TYPE_CHECKBOX  = 'checkbox';

	/** @var string Тип переключателя. */
	public $type = self::TOGGLE_TYPE_RADIO;

	/** @var string Классы, которые необходимо добавить к контейнеру виджета. */
	public $containerClass;

	/** @var string Класс каждой кнопки */
	public $buttonClass = 'btn btn-default';

	/** @var string[] Список доступных для выбора значений. */
	public $values = [];

	/** @var string[] Доступные типы переключателя. */
	protected static $_toggleTypes = [
		self::TOGGLE_TYPE_RADIO,
		self::TOGGLE_TYPE_CHECKBOX
	];

	/**
	 * @inheritdoc
	 *
	 * @author Кривонос Иван <krivonos.iv@dns-shop.ru>
	 */
	public function run() {
		$buttonList = '';

		foreach ($this->values as $key => $value) {
			$buttonList .= $this->renderButton($key, $value);
		}

		echo Html::tag('div', $buttonList, [
			'class'         => 'btn-group' . (null !== $this->containerClass ? ' ' . $this->containerClass : ''),
			'data-toggle'   => 'buttons',
		]);
	}

	/**
	 * Отрисовка отдельной кнопки.
	 *
	 * @param string $value Значение поля
	 * @param string $title Заголовок кнопки
	 *
	 * @return string
	 *
	 * @author Кривонос Иван <krivonos.iv@dns-shop.ru>
	 */
	protected function renderButton($value, $title) {
		$attribute  = $this->attribute;
		$input      = $this->type;
		$inputId    = Html::getInputId($this->model, $this->attribute) . '[' . $value . ']';

		$button = Html::$input(
			Html::getInputName($this->model, $this->attribute),
			($this->model->$attribute === (string)$value),
			[
				'id'            => $inputId,
				'value'         => $value,
				'autocomplete'  => 'off',
			]
		);

		return Html::label($button . $title, $inputId, ['class' => $this->buttonClass,]);
	}
}