<?php
namespace common\base\widgets;
use yii\helpers\Html;

/**
 * @author isakov.v <iskakov.VI@dns-shop.ru>
 *
 * Расиширение виджета пейджинга
 * @package common\base\widgets
 */
class LinkPager extends \yii\widgets\LinkPager{

	/** @var bool Показывать или нет форму перехода на указанную страницу */
	public $showGotoButton = false;
	const ATTR_SHOW_GOTO_BUTTON = 'showGotoButton';

	const ATTR_PAGINATION = 'pagination';
	const ATTR_MAX_BUTTON_COUNT = 'maxButtonCount';

	/**
	 * @author isakov.v <iskakov.VI@dns-shop.ru>
	 *
	 * @inheritdoc
	 */
	protected function renderPageButtons() {
		$html = parent::renderPageButtons();

		if (true === $this->showGotoButton && $this->pagination->totalCount > 1) {
			$form = Html::beginForm('', 'GET');
					$form .=  Html::tag('div', Html::input('text', 'page', $this->pagination->page + 1,
						['class' => 'form-control']), ['class' => 'col-md-1 pagination']);
					$form .= Html::tag('div', Html::submitButton('Перейти', ['class' => 'btn btn-primary']), ['class' => 'col-md-1 pagination']);
				$form .= Html::endForm();
			$html =  Html::tag('div', $html, ['class' => 'col-md-10']) . $form;
		}

		return $html;
	}
}