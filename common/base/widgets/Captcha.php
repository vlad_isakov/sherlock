<?php
namespace common\base\widgets;

use yii\captcha\CaptchaValidator;

/**
 * @inheritdoc
 *
 *
 */
class Captcha extends \yii\captcha\Captcha {
	/**
	 * @inheritdoc
	 *
	 *
	 */
	public function init() {
		$this->imageOptions = [
			'class' => 'captcha-img',
			'style' => 'margin:0 0 0 8px',
		];

		// -- Определяем, по какому адресу забирать изображение капчи
		if (null !== $this->model && null !== $this->attribute) {
			$rules = $this->model->getActiveValidators($this->attribute);
			foreach ($rules as $rule) {
				if ($rule instanceof CaptchaValidator) {
					$this->captchaAction = $rule->captchaAction;
				}
			}
		}
		// -- -- -- --

		// -- Определяем шаблон для отрисовки капчи
		$this->template = '
			<div class="row">
				<div class="col-lg-6">
					{input}
					<div class="captcha-reload" style="padding:10px 0 0 0;text-align:center">
						' . \yii\helpers\Html::tag('a', 'обновить код', ['href' => '#', 'style' => 'margin:0']) . '
					</div>
				</div>
				<div class="col-lg-6">
					{image}
				</div>
			</div>
		';
		// -- -- -- --

		parent::init();
	}
}