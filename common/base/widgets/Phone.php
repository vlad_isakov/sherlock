<?php
namespace common\base\widgets;

use yii\widgets\MaskedInput;

/**
 * Виджет поля ввода номера телефона.
 *
 *
 */
class Phone extends MaskedInput {
	/** @inheritdoc */
	public $mask = '+7 (999) 999 9999';
}