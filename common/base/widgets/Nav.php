<?php
namespace common\base\widgets;

use Yii;

/**
 * @inheritdoc
 *
 *
 */
class Nav extends \yii\bootstrap\Nav {
	/**
	 * @inheritdoc
	 *
	 *
	 */
	protected function isItemActive($item) {
		// -- Проверяем стандартным способом
		$result = parent::isItemActive($item);
		if (true === $result) {
			return true;
		}
		// -- -- -- --

		// -- Если элемент не выделен, проверяем уже своим способом (дело в том, что стандартный алгоритм учитывает только массивы)
		if (false === isset($item['url'])) {
			return false;
		}

		if (false === is_string($item['url'])) {
			return false;
		}

		if ($item['url'] === Yii::$app->request->getUrl()) {
			return true;
		}
		// -- -- -- --

		return false;
	}
}