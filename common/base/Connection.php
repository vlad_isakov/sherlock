<?php
/**
 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
 */

namespace common\base;

use Yii;
use yii\base\NotSupportedException;

class Connection extends \yii\db\Connection {

	private $_schema;

	/**
	 * Returns the schema information for the database opened by this connection.
	 * @return Schema the schema information for the database opened by this connection.
	 * @throws NotSupportedException if there is no support for the current driver type
	 */
	public function getSchema()
	{
		if ($this->_schema !== null) {
			return $this->_schema;
		} else {
			$driver = $this->getDriverName();
			if (isset($this->schemaMap[$driver])) {
				$config = !is_array($this->schemaMap[$driver]) ? ['class' => $this->schemaMap[$driver]] : $this->schemaMap[$driver];
				$config['db'] = $this;
				$config['class'] = Schema::class;

				return $this->_schema = Yii::createObject($config);
			} else {
				throw new NotSupportedException("Connection does not support reading schema information for '$driver' DBMS.");
			}
		}
	}

	/**
	 * Returns the query builder for the current DB connection.
	 * @return QueryBuilder the query builder for the current DB connection.
	 */
	public function getQueryBuilder()
	{
		return $this->getSchema()->getQueryBuilder();
	}
}