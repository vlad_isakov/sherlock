<?php
namespace common\base;

use Yii;
use yii\base\Component;

/**
 *
 * Класс сохранения и получения последних запросов пользователей к моделям
 *
 * @author isakov.v
 */
class UserRequestsHistory extends Component {

	/**
	 * @param string $calledClass
	 */
	protected $calledClass;

	/**
	 * @param string $calledClass
	 */
	public function __construct($calledClass) {
		$this->calledClass = $calledClass;

		parent::__construct();
	}

	/**
	 * @param array $data
	 */
	public function setRequestParams($data) {
		Yii::$app->cache->set($this->_getName(), $data);
	}

	/**
	 * @return array
	 */
	public function getRequestParams() {
		$requestName = $this->_getName();

		return Yii::$app->cache->get($requestName);
	}

	/**
	 * Устанавливает кол-во записей для текущей конфигурации фильтров
	 *
	 * @param int $count
	 * @param string $cacheKey
	 *
	 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
	 */
	public function setRecordCount($count, $cacheKey) {
		Yii::$app->cache->set($cacheKey, $count, 10 * 60);
	}

	/**
	 * Возвращает последнее установленное кол-во записей для текущей конфигурации фильтров
	 *
	 * @param $cacheKey
	 *
	 * @return bool|array
	 *
	 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
	 */
	public function getRecordCount($cacheKey) {
		return Yii::$app->cache->get($cacheKey);
	}

	/**
	 * Взять имя ключа для кэша
	 *
	 * @return string
	 *
	 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
	 */
	protected function _getName() {
		return md5('request_history.' . $this->calledClass . '.' . Yii::$app->user->id. '.v1.6');
	}
}