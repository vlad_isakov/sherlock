<?php
namespace common\base;

/**
 * @author  isakov.v
 *
 * Query переопределяющий createCommand для подставки коммента перед запросом.
 *
 */
class ActiveQuery extends \yii\db\ActiveQuery {

	private $noLoadBalance = false;

	/** @var bool Нужно ли добавить в select инструкцию NO UPDATE */
	private $isSelectForUpdate = false;


	/**
	 * @author isakov.v
	 * Не отправлять запрос на балансировщик, а прямо в мастер
	 *
	 * @return $this
	 */
	public function noBalance() {
		$this->noLoadBalance = true;

		return $this;
	}

	/**
	 * Установка чтения с блокировкой.
	 *
	 * @return static
	 */
	public function forUpdate() {
		$this->isSelectForUpdate = true;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function createCommand($db = null) {
		/* @var $modelClass ActiveRecord */
		$modelClass = $this->modelClass;
		if ($db === null) {
			$db = $modelClass::getDb();
		}

		if ($this->sql === null) {
			list ($sql, $params) = $db->getQueryBuilder()->build($this);
		}
		else {
			$sql    = $this->sql;
			$params = $this->params;
		}

		if (true === $this->noLoadBalance) {
			$sql = '/*NO LOAD BALANCE*/' . $sql;
		}

		if ($this->isSelectForUpdate === true) {
			$sql .= ' FOR UPDATE';
		}

		return $db->createCommand($sql, $params);
	}

	/**
	 * @author Исаков Владислав <newsperparser@gmail.com>
	 */
	public function userFilter() {
		$model = new $this->modelClass;

		$result = $this->andWhere([strtolower($model::tableName() . '.user_id') => \Yii::$app->user->id]);

		/** @var ActiveRecord $result */

		return $result;

	}

	/**
	 * Возврат первого экземпляра из набора.
	 *
	 * @return ActiveRecord
	 *
	 * @author Кривонос Иван <krivonos.iv@dns-shop.ru>
	 */
	public function first() {
		$result = $this->limit(1)->one();/** @var ActiveRecord $result */

		return $result;
	}

	/**
	 * @inheritdoc
	 *
	 * Отличие от родительского метода - явное приведение типа.
	 *
	 * @return int
	 *
	 * @author Дегтярев Илья <degtyarev.iv@dns-shop.ru>
	 */
	public function count($q = '*', $db = null) {
		return (int)parent::count($q, $db);
	}
}