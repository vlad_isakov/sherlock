<?php
/**
 * Created by PhpStorm.
 * User: dotty
 * Date: 16.10.15
 * Time: 17:11
 */

namespace common\base\bootstrap;

/**
 * Наши настройки шаблонов и полей
 */
class ActiveField extends \yii\bootstrap\ActiveField {

	public $checkboxTemplate = "<div class=\"checkbox\">\n{input}\n{beginLabel}\n{labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>";
	public $horizontalCheckboxTemplate = "{beginWrapper}\n<div class=\"checkbox\">\n{input}\n{beginLabel}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}";


	/**
	 * @inheritdoc
	 */
	public function checkboxList($items, $options = []) {
		$options['item'] = function ($index, $label, $name, $checked, $value) {
			return '<div class="checkbox"><input type="checkbox" ' . ($checked ? 'checked="checked"' : '') . ' id="' . $name . $index . '" value="' . $value . '" name="' . $name . '"><label for="' . $name . $index . '">' . $label . '</label></div>';
		};

		$options['class'] = 'checkbox-list';

		parent::checkboxList($items, $options);

		return $this;
	}

	/**
	 * @inheritdoc
	 *
	 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
	 */
	public function radioListFront($items, $options = []) {
		$options['item'] = function ($index, $label, $name, $checked, $value) {
			return '<div class="radio"><input type="radio" ' . ($checked ? 'checked="checked"' : '') . ' id="' . $name . $index . '" value="' . $value . '" name="' . $name . '"><label for="' . $name . $index . '">' . $label . '</label></div>';
		};

		$options['class'] = 'radio-list';

		parent::radioList($items, $options);

		return $this;
	}
}