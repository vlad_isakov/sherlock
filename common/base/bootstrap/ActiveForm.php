<?php
/**
 * Created by PhpStorm.
 * User: dotty
 * Date: 16.10.15
 * Time: 17:15
 */

namespace common\base\bootstrap;


/**
 * Class ActiveForm
 *
 * Подменяем ActiveField
 */
class ActiveForm extends \yii\bootstrap\ActiveForm {

	public $fieldClass = 'common\base\bootstrap\ActiveField';

}