<?php
namespace common\base;

/**
 * @inheritdoc
 *
 * @method ActiveQuery hasMany($class, array $link) see [[BaseActiveRecord::hasMany()]] for more info
 * @method ActiveQuery hasOne($class, array $link) see [[BaseActiveRecord::hasOne()]] for more info
 *
 *
 */
class ActiveRecord extends \yii\db\ActiveRecord {
	/**
	 * @return ActiveQuery
	 *
	 * @author isakov.v
	 */
	public static function find() {
		return new ActiveQuery(get_called_class());
	}

	/**
	 * @inheritdoc
	 *
	 *
	 */
	public function getFirstError($attribute = null) {
		$errors = $this->getErrors($attribute);
		if (0 === count($errors)) {
			return null;
		}

		$error = reset($errors);

		if (null !== $attribute) {// Если указан атрибут, то массив является одноуровневым - в нём только список ошибок
			return $error;
		}
		else {// Если атрибут не указан, то массив ошибок является многоуровневым, где ключом является название атрибута
			$error = reset($error);
		}

		return $error;
	}

	/**
	 * Получение названия тега для кэширования.
	 *
	 * @param string[]|string|null $ids Идентификаторы или NULL, если необходим только общий для всех тег
	 *
	 * @return string[]
	 *
	 *
	 */
	public static function getCacheTags($ids = null) {
		$tags = [static::class];

		if (null !== $ids) {
			if (false === is_array($ids)) {
				$ids = [$ids];
			}

			foreach ($ids as $id) {
				$tags[] = static::class . '(' . $id . ')';
			}
		}

		return $tags;
	}

	/**
	 * Получение тега для сброса кэша.
	 *
	 * @param string|null $id Идентификатор или NULL, если нужен общий тег
	 *
	 * @return string
	 *
	 *
	 */
	public static function getFlushCacheTag($id = null) {
		$tag = static::class;

		if (null !== $id) {
			$tag .= '(' . $id . ')';
		}

		return $tag;
	}
}