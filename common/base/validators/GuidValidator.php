<?php
namespace common\base\validators;

use yii\validators\RegularExpressionValidator;

/**
 * Валидатор для проверки значения на формат GUID.
 *
 *
 */
class GuidValidator extends RegularExpressionValidator {
	/** @inheritdoc */
	public $pattern = '/^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$/';
}