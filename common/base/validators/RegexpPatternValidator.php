<?php
namespace common\base\validators;

use yii\validators\Validator;

/**
 * Валидатор для проверки правильности ввода регулярного выражения.
 *
 *
 */
class RegexpPatternValidator extends Validator {
	/** @inheritdoc */
	public $message = 'Регулярное выражение указано неверно.';

	/**
	 * @inheritdoc
	 *
	 *
	 */
	public function validateAttribute($model, $attribute) {
		if (false === @preg_match($model->$attribute, 'Any text here')) {
			$this->addError($model, $attribute, $this->message);
		}
	}
}