<?php
namespace common\base\validators;

/**
 * @inheritdoc
 *
 * @author Турушев Николай <Turushev.NS@dns-shop.ru>
 */
class EachValidator extends \yii\validators\EachValidator {
	const ATTR_RULE = 'rule';
}