<?php
namespace common\base\validators;

use yii\helpers\HtmlPurifier;
use yii\validators\Validator;

/**
 * Очистка значения атрибута от HTML тегов для предотвращения XSS.
 *
 *
 */
class PurifyValidator extends Validator {
	/**
	 * @inheritdoc
	 *
	 *
	 */
	public function validateAttribute($model, $attribute) {
		$model->$attribute = HtmlPurifier::process($model->$attribute, ['HTML.Allowed' => '']);
	}
}