<?php
namespace common\base\validators;

/**
 * Валидатор для удаления пробелов в начале и в конце.
 * По сути, обёртка вокруг функции trim.
 *
 *
 */
class TrimValidator extends FilterValidator {
	/** @inheritdoc */
	public $filter = 'trim';
}