<?php
namespace common\base\validators;

use yii\validators\NumberValidator;

/**
 * Валидатор для проверки ввода целого числа.
 *
 * @author Смирнов Илья <Smirnov.IN@dns-shop.ru>
 */
class IntegerValidator extends NumberValidator {
	/** @inheritdoc */
	public $integerOnly = true;

	const ATTR_MIN = 'min';
	const ATTR_MAX = 'max';
}