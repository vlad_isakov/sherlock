<?php
namespace common\base\validators;

use common\base\Model;
use yii\base\Exception;
use yii\validators\Validator;

/**
 * Валидатор для проверки атрибутов, содержащих в себе массивы.
 * Фактически, валидатор пробрасывает указанный валидатор для каждого значения массива.
 *
 *
 */
class ArrayValidator extends Validator {
	/** @var array Название валидатора (нулевой элемент) и список параметров, которые будут переданы валидатору для проверки ключей массива. */
	public $keys;
	const ATTR_KEYS = 'keys';

	/** @var array Название валидатора (нулевой элемент) и список параметров, которые будут переданы валидатору для проверки значений массива. */
	public $values;
	const ATTR_VALUES = 'values';

	/** @var bool Может ли атрибут иметь пустое значение. */
	public $allowEmpty = true;

	/**
	 * Валидация атрибута.
	 *
	 *
	 *
	 * @param Model $model Объект для валидации
	 * @param string $attribute Название атрибута для проверки
	 */
	public function validateAttribute($model, $attribute) {
		// -- Проверяем, может ли быть пустое значение
		if ($this->isEmpty($model->$attribute)) {
			if (true === $this->allowEmpty) {
				$model->$attribute = [];
				return;
			}
			$this->addError($model, $attribute, \Yii::t('', 'Атрибут {attribute} не может быть пустым.', ['{attribute}' => $attribute]));
			return;
		}
		// -- -- -- --

		// -- Проверяем, является ли атрибут массивом
		if (false === is_array($model->$attribute)) {
			$this->addError($model, $attribute, \Yii::t('', 'Атрибут {attribute} не является массивом.', ['{attribute}' => $attribute]));
			return;
		}
		// -- -- --

		// -- Если указано, что необходимо проверить значения массива
		if (isset($this->values)) {
			$this->_validate($model, $attribute, $this->values);
		}
		// -- -- -- --

		// -- Если указано, что необходимо проверить ключи массива
		if (isset($this->keys)) {
			$this->_validate($model, $attribute, $this->keys);
		}
		// -- -- -- --
	}

	/**
	 * Валидация значений массива.
	 *
	 *
	 *
	 * @param Model $object Объект, который необходимо проверить
	 * @param string $attribute Название атрибута
	 * @param array $params Список параметров валидации
	 * @throws Exception
	 */
	private function _validate($object, $attribute, array $params) {
		// -- Проверяем указанный валидатор
		if (false === isset($params[0])) {
			throw new Exception('Не указан валидатор.');
		}
		$validatorClassName = $params[0];
		unset($params[0]);// Удаляем параметр с именем валидатора из списка, так как он уже не нужен
		// -- -- -- --

		// -- Инициализация валидатора и назначение параметров валидации
		$validator = self::createValidator($validatorClassName, $object, [$attribute], $params);
		// -- -- -- --

		// -- Проходимся по каждому элементу массива
		foreach ($object->$attribute as $value) {
			if ($this->allowEmpty && $this->isEmpty($value)) {
				continue;
			}
			$result = $validator->validate($value, $error);// Валидируем значение
			if (false === $result) {
				$this->addError($object, $attribute, $error);
				break;
			}
		}
		// -- -- -- --
	}
}