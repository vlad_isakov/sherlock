<?php
namespace common\base\validators;

/**
 * @inheritdoc
 *
 * @author Турушев Николай <Turushev.NS@dns-shop.ru>
 */
class FilterValidator extends \yii\validators\FilterValidator {
	const ATTR_FILTER           = 'filter';
	const ATTR_SKIP_ON_ARRAY    = 'skipOnArray';
	const ATTR_SKIP_ON_EMPTY    = 'skipOnEmpty';
}