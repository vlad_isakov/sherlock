<?php
namespace common\base\validators;

use Exception;
use yii\validators\Validator;

/**
 * Приводим GUID к формату 1С.
 *
 * @author Медвеженков Владимир <medvezhenkov.v@dns-shop.ru>
 */
class ConvertToGuidFormatValidator extends Validator {
	/**
	 * @inheritdoc
	 * @author Медвеженков Владимир <medvezhenkov.v@dns-shop.ru>
	 * @throws Exception
	 */
	public function validateAttribute($model, $attribute) {
		if (is_string($model->$attribute)) {
			$model->$attribute = strtoupper($model->$attribute);

			// Если guid правильной длины
			if (36 === strlen($model->$attribute)) {
				return true;
			}
			// Если это md5 хэш
			elseif (32 === strlen($model->$attribute)) {
				$model->$attribute =
						mb_strcut($model->$attribute, 0, 8) . '-'
						. mb_strcut($model->$attribute, 8, 4) . '-'
						. mb_strcut($model->$attribute, 12, 4) . '-'
						. mb_strcut($model->$attribute, 16);
				return true;
			}
		}

		throw new Exception('Не поддерживаемый тип $guid');
	}
}