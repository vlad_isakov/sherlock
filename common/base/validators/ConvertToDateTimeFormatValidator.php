<?php
namespace common\base\validators;

use Exception;
use yii\db\Expression;
use yii\validators\Validator;

/**
 * Приводим дату к формату 1С.
 *
 * @author Медвеженков Владимир <medvezhenkov.v@dns-shop.ru>
 */
class ConvertToDateTimeFormatValidator extends Validator {
	/**
	 * @inheritdoc
	 * @author Медвеженков Владимир <medvezhenkov.v@dns-shop.ru>
	 * @throws Exception
	 */
	public function validateAttribute($model, $attribute) {
		if (is_null($model->$attribute) || empty($model->$attribute)) {
			$model->$attribute = date('Y-m-d\TH:i:s');
			return true;
		}

		if ($model->$attribute instanceof Expression) {
			if ('localtimestamp' === (string)$model->$attribute || 'localtime' === (string)$model->$attribute) {
				return true;
			}
			else {
				$model->$attribute = date('Y-m-d\TH:i:s');
				return true;
			}
		}

		if (is_string($model->$attribute)) {
			$model->$attribute = date('Y-m-d\TH:i:s', strtotime($model->$attribute));
			return true;
		}

		if (is_int($model->$attribute)) {
			$model->$attribute = date('Y-m-d\TH:i:s', $model->$attribute);
			return true;
		}

		throw new Exception('Не поддерживаемый тип $dateTime');
	}
}