<?php
namespace common\base\validators;

/**
 * Валидатор проверки строки
 *
 * @author  Исаков Владислав <isakov.vi@dns-shop.ru>
 */
class StringValidator extends \yii\validators\StringValidator {
	const ATTR_MIN = 'min';
	const ATTR_MAX = 'max';
}