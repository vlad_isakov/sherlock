<?php

namespace common\base\validators;

use yii\validators\RegularExpressionValidator;

/**
 * Валидатор номера документа веб-базы или 1С (для РН, документов оплаты и пр.).
 * Формат: Т-00003677.
 * Для других докуменов, не соответствующих вышеприведённому формату нужно добавлять настройки валидатора.
 * На текущий момент класс сделан для валидации только документов ИМ, поступающих из веб-базы.
 *
 * @author Казанцев Александр <kazancev.al@dns-shop.ru>
 */
class DocumentNumberValidator extends RegularExpressionValidator {

	/** @inheritdoc */
	public $pattern = '/^[a-zА-Яа-яЁё]-[0-9]{8}$/ui';

}