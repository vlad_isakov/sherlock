<?php
/**
 * @author Исаков Владислав <newsperparser@gmail.com>
 */

namespace common\base;


use common\models\TelegramLog;


class TelegramHelper {
	public $message;
	public $content;
	public $callBack;

	public $button;

	const ADMIN_USERS = [
		345310144,
		391973293
	];

	const ACTION_PRODUCT_INFO = 'Информация о товаре';
	const ACTION_ADD_PRODUCT = 'Добавить товар';
	const ACTION_ACTIONS = 'Акции';
	const ACTION_START = '/start';

	const STATE_NORMAL = 'normal';
	const STATE_PRODUCT_INFO = 'product_info';
	const STATE_ADD_PRODUCT = 'add_product';
	const STATE_ADD_PRODUCT_PROCESS = 'add_product_process';

	public function getMessage() {
		$this->content = file_get_contents("php://input");
		TelegramLog::add($this->content);
		$update = json_decode($this->content, true);
		$this->message = $update["message"];
	}

	public function getChat() {
		return $this->message["chat"];
	}

	public function getText() {
		if (array_key_exists('text', $this->message)) {
			return $this->message["text"];
		}
		else return '';
	}

	public function getChatId() {
		return $this->message["chat"]["id"];
	}

	public function getUserName() {
		return $this->message["chat"]["first_name"];
	}

	public function isDocument() {
		if (array_key_exists('document', $this->message)) {
			return true;
		}

		return false;
	}

	public function isContact() {
		if (array_key_exists('contact', $this->message)) {
			return true;
		}

		return false;
	}


	public function getButtons($userId) {
		$buttons[] = [
			[
				"text" => static::ACTION_PRODUCT_INFO
			],
			[
				"text" => static::ACTION_ACTIONS
			]
		];

		if (false !== array_search($userId, TelegramHelper::ADMIN_USERS)) {
			$buttons[] = [
				[
					"text" => static::ACTION_ADD_PRODUCT
				],
			];
		}

		$keyboard = [
			"keyboard"          => $buttons,
			"one_time_keyboard" => false, // можно заменить на FALSE,клавиатура скроется после нажатия кнопки автоматически при True
			"resize_keyboard"   => true // можно заменить на FALSE, клавиатура будет использовать компактный размер автоматически при True
		];

		return $keyboard;
	}
}