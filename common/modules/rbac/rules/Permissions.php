<?php
/**
 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
 */

namespace common\modules\rbac\rules;

class Permissions {
	//Разрешения
	const P_ADMIN = 'PermissionAdmin';
	const P_SEARCH_COMPANY = 'PermissionSearchCompany';
	const P_SEND_SMS = 'PermissionSendSms';

	//Роли
	const ROLE_ADMIN = 'Administrator';
	const ROLE_CONTENT_MANAGER = 'ContentManager';
	const ROLE_OPERATOR = 'Operator';
}