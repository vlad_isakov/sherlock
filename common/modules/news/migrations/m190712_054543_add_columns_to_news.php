<?php

namespace modules\news\migrations;

use common\components\Migration;
use yii\db\Schema;

class m190712_054543_add_columns_to_news extends Migration {
	private $_tableName = 'site_news';

	public function safeUp() {
		$this->addColumn($this->_tableName, 'category_id', Schema::TYPE_INTEGER . ' DEFAULT 0');
		$this->addColumn($this->_tableName, 'title', Schema::TYPE_STRING . ' NOT NULL');
		$this->addColumn($this->_tableName, 'is_published', Schema::TYPE_BOOLEAN . ' DEFAULT 0');
		$this->addColumn($this->_tableName, 'active_from', Schema::TYPE_DATETIME . ' DEFAULT NULL');
		$this->addColumn($this->_tableName, 'active_to', Schema::TYPE_DATETIME . ' DEFAULT NULL');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropColumn($this->_tableName, 'category_id');
		$this->dropColumn($this->_tableName, 'title');
		$this->dropColumn($this->_tableName, 'is_published');
		$this->dropColumn($this->_tableName, 'active_from');
		$this->dropColumn($this->_tableName, 'active_to');
	}
}
