<?php

namespace common\modules\news\models;

use common\base\ActiveRecord;
use common\models\Company;
use common\models\ObjectFile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\validators\RequiredValidator;
use yii\validators\SafeValidator;

/**
 * Модель Новостей
 *
 * @author isakov.v
 *
 * Поля таблицы:
 * @property int             $id
 * @property int             $old_id
 * @property string          $category_id
 * @property string          $date
 * @property string          $title
 * @property string          $text
 * @property bool            $is_published
 * @property string          $image
 * @property string          $company_guid
 * @property string          $active_from
 * @property string          $active_to
 * @property string          $insert_stamp
 * @property string          $update_stamp
 *
 * @property-read Company    $company
 * @property-read ObjectFile $objectImage
 *
 */
class SiteNews extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_OLD_ID = 'old_id';
	const ATTR_CATEGORY_ID = 'category_id';
	const ATTR_DATE = 'date';
	const ATTR_TITLE = 'title';
	const ATTR_TEXT = 'text';
	const ATTR_IS_PUBLISHED = 'is_published';
	const ATTR_IMAGE = 'image';
	const ATTR_COMPANY_GUID = 'company_guid';
	const ATTR_ACTIVE_FROM = 'active_from';
	const ATTR_ACTIVE_TO = 'active_to';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';

	const CATEGORY_NEWS = 0;
	const CATEGORY_ACTION = 1;

	public $imageFile;
	const ATTR_IMAGE_FILE = 'imageFile';

	public static function tableName() {
		return 'site_news';
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('getdate()'),
			],
		];
	}

	public function attributeLabels() {
		return [
			static::ATTR_TITLE        => 'Название',
			static::ATTR_TEXT         => 'Содержание',
			static::ATTR_DATE         => 'Дата',
			static::ATTR_IS_PUBLISHED => 'Опубликована',
			static::ATTR_IMAGE_FILE   => 'Изображение',

		];
	}

	public function rules() {
		return [
			[static::ATTR_DATE, RequiredValidator::class],
			[static::ATTR_TITLE, RequiredValidator::class],
			[static::ATTR_TEXT, RequiredValidator::class],
			[static::ATTR_COMPANY_GUID, RequiredValidator::class],
			[static::ATTR_IS_PUBLISHED, SafeValidator::class],
			[static::ATTR_IMAGE_FILE, SafeValidator::class],
		];
	}

	/**
	 * @return bool|string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public static function getLogoPath() {
		return Yii::getAlias('@newsImage');
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getWebImagePath() {
		if (null === $this->objectImage) {
			return '/images/image-8-170x170.jpg';
		}

		return '/uploads/news' . DIRECTORY_SEPARATOR . substr($this->objectImage->filename, 0, 2) . DIRECTORY_SEPARATOR . $this->objectImage->filename;
	}

	/**
	 * @return \common\base\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getCompany() {
		return $this->hasOne(Company::class, [Company::ATTR_GUID => static::ATTR_COMPANY_GUID]);
	}

	const REL_COMPANY = 'company';

	/**
	 * @return \common\base\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getObjectImage() {
		return $this->hasOne(ObjectFile::class, [ObjectFile::ATTR_OBJECT_ID => static::ATTR_ID])
			->andWhere([ObjectFile::ATTR_OBJECT => static::class]);
	}

	const REL_IMAGE = 'objectImage';

}