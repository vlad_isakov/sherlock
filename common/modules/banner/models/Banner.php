<?php

namespace common\modules\banner\models;

use common\models\ObjectFile;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\validators\RequiredValidator;
use yii\validators\SafeValidator;
use yii\validators\StringValidator;
use yii\validators\UrlValidator;

/**
 * Поля таблицы:
 * @property integer         $id
 * @property string          $title
 * @property string          $url
 * @property string          $beg_date
 * @property string          $end_date
 * @property int             $zone
 * @property string          $utm
 * @property int             $click_count
 * @property int             $show_count
 * @property string          $insert_stamp
 * @property string          $update_stamp
 * @property int             $priority
 *
 * @property-read ObjectFile $image
 */
class Banner extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_TITLE = 'title';
	const ATTR_URL = 'url';
	const ATTR_BEG_DATE = 'beg_date';
	const ATTR_END_DATE = 'end_date';
	const ATTR_ZONE = 'zone';
	const ATTR_UTM = 'utm';
	const ATTR_CLICK_COUNT = 'click_count';
	const ATTR_SHOW_COUNT = 'show_count';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_PRIORITY = 'priority';

	/** @var string */
	public $file;
	const ATTR_FILE = 'file';

	const ZONE_HEADER = 0;
	const ZONE_MIDDLE = 1;

	const ZONE_NAMES = [
		self::ZONE_HEADER => 'Верхний',
		self::ZONE_MIDDLE => 'Средний',
	];

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('getdate()'),
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public static function tableName() {
		return 'banner';
	}

	/**
	 * @return string|string[]
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public static function primaryKey() {
		return [static::ATTR_ID];
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID           => 'id',
			static::ATTR_TITLE        => 'Название',
			static::ATTR_URL          => 'URL',
			static::ATTR_UTM          => 'UTM-метки',
			static::ATTR_BEG_DATE     => 'Начало показа',
			static::ATTR_END_DATE     => 'Конец показа',
			static::ATTR_CLICK_COUNT  => 'Переходы',
			static::ATTR_INSERT_STAMP => 'Дата создания',
			static::ATTR_UPDATE_STAMP => 'Дата изменения',
			static::ATTR_ZONE         => 'Место размещения',
			static::ATTR_PRIORITY     => 'Приоритет',
		];
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function rules() {
		return [
			[static::ATTR_TITLE, RequiredValidator::class],
			[static::ATTR_URL, RequiredValidator::class],
			[static::ATTR_URL, UrlValidator::class],
			[static::ATTR_BEG_DATE, RequiredValidator::class],
			[static::ATTR_END_DATE, RequiredValidator::class],
			[static::ATTR_ZONE, RequiredValidator::class],
			[static::ATTR_UTM, StringValidator::class],
			[static::ATTR_PRIORITY, SafeValidator::class],
		];
	}

	/**
	 * @return array|\yii\db\ActiveRecord|null
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getImage() {
		return $this->hasOne(ObjectFile::class, [ObjectFile::ATTR_OBJECT_ID => static::ATTR_ID])
			->andWhere([ObjectFile::ATTR_OBJECT => static::class])->one();
	}

	const REL_IMAGE = 'image';
}
