<?php
namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Модель Районов. Таблица Districts
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string $Name
 * @property string $rowguid
 *
 */
class District extends ActiveRecord {

	const ATTR_NAME = 'Name';
	const ATTR_GUID = 'rowguid';

	public static function tableName() {
		return 'Districts';
	}

	public static function getAll() {

		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, 1]);
		$districts = Yii::$app->cache->get($cacheKey);
		if (false === $districts) {
			$districts =  ArrayHelper::map(static::find()->orderBy([static::ATTR_NAME => SORT_ASC])->all(), static::ATTR_GUID, static::ATTR_NAME);

			Yii::$app->cache->set($cacheKey, $districts, null);
		}

		return $districts;
	}
}