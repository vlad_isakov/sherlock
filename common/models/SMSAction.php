<?php
namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * СМС-акция
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @property string $rowguid
 * @property string $Firm_rowguid
 * @property string $Class_rowguid
 * @property string $Content
 * @property string $SDate
 * @property string $FDate
 * @property string $row_type
 * @property string $town_rowguid
 *
 */
class SMSAction extends ActiveRecord {

	const ATTR_GUID = 'rowguid';
	const ATTR_COMPANY_GUID = 'Firm_rowguid';
	const ATTR_CLASS_GUID = 'Class_rowguid';
	const ATTR_CONTENT = 'Content';
	const ATTR_START_DATE = 'SDate';
	const ATTR_END_DATE = 'FDate';
	const ATTR_ROW_TYPE = 'row_type';
	const ATTR_TOWN_GUID = 'town_rowguid';

	public static function tableName() {
		return 'SMSNews';
	}

	/**
	 * Получение действующего текста СМС-акции по услуге
	 *
	 * @param string $serviceGuid
	 *
	 * @return bool|string
	 *
	 * @author Исаков Владислав
	 */
	public static function getAction($serviceGuid) {

		if (null === $serviceGuid || empty($serviceGuid)) {
			return false;
		}
        //Ищем сначала смс-акцию к конкретной услуге
		/** @var static $action */
		$action = static::find()
			->andWhere([static::ATTR_CLASS_GUID => $serviceGuid])
			->andWhere(['<=', static::ATTR_START_DATE, new Expression('getdate()')])
			->andWhere(['>=', static::ATTR_END_DATE, new Expression('dateadd(DD, -1, cast(getdate() as date))')])
			->one();

		//Если не нашли то ищем смс-акцию к группе в которую входит услуга
		if (null === $action) {
            /** @var \common\models\ServiceName $serviceClass */
            $serviceClass = ServiceName::find()->where([ServiceName::ATTR_GUID => $serviceGuid])->one();
            if(null === $serviceClass) {
                return false;
            }

            /** @var static $action */
            $action = static::find()
                ->andWhere([static::ATTR_CLASS_GUID => $serviceClass->Class_Num])
                ->andWhere(['<=', static::ATTR_START_DATE, new Expression('getdate()')])
                ->andWhere(['>=', static::ATTR_END_DATE, new Expression('dateadd(DD, -1, cast(getdate() as date))')])
                ->one();

            if (null === $action) {
                return false;
            }
		}

		return $action->Content;
	}
}