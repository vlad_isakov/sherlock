<?php
/**
 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
 */

namespace common\models;


use yii\db\ActiveRecord;

/**
 * @property string $rowguid
 * @property string $firm_rowguid
 * @property string $NamesList
 * @property string $PNB_rowguid
 *
 *
 * @author  Исаков Владислав <isakov.vi@dns-shop.ru>
 */
class CompanyDistrictAlias extends ActiveRecord {
	const ATTR_GUID = 'rowguid';
	const ATTR_COMPANY_GUID = 'firm_rowguid';
	const ATTR_NAME_LIST = 'NamesList';
	const ATTR_PNB_rowguid = 'PNB_rowguid';

	public static function tableName() {
		return 'FirmsPNames';
	}
}