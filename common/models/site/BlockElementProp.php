<?php

namespace common\models\site;

use yii\db\ActiveRecord;

/**
 * Поля таблицы:
 * @property string                      $IBLOCK_ELEMENT_ID
 * @property string                      $PROPERTY_17
 * @property string                      $PROPERTY_21
 * @property string                      $PROPERTY_22
 * @property string                      $PROPERTY_40
 *
 */

class BlockElementProp extends ActiveRecord {
	const ATTR_BLOCK_ELEMENT_ID = 'IBLOCK_ELEMENT_ID';
	const ATTR_ADDRESS = 'PROPERTY_17';
	const ATTR_EMAIL = 'PROPERTY_21';
	const ATTR_SITE = 'PROPERTY_22';
	const ATTR_WORK_HOURS = 'PROPERTY_40';

	public static function getDb() {
		return \Yii::$app->get('dbSite');
	}

	public static function tableName() {
		return 'b_iblock_element_prop_s6';
	}
}
