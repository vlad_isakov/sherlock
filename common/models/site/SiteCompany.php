<?php

namespace common\models\site;

use yii\db\ActiveRecord;

/**
 * Поля таблицы:
 * @property int    $id
 * @property string $old_id
 * @property string $guid
 *
 */
class SiteCompany extends ActiveRecord {
	const ATTR_ID = 'id';
	const ATTR_OLD_ID = 'old_id';
	const ATTR_GUID = 'guid';

	public static function tableName() {
		return 'site_company';
	}


}
