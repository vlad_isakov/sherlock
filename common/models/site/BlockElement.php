<?php

namespace common\models\site;

use yii\db\ActiveRecord;

/**
 * Поля таблицы:
 * @property int                      $ID
 * @property string                      $XML_ID
 * @property string                      $NAME
 * @property string                      $ACTIVE
 * @property string                      $ACTIVE_FROM
 * @property string                      $IBLOCK_ID
 * @property string                      $IBLOCK_SECTION_ID
 *
 */

class BlockElement extends ActiveRecord {
	const ATTR_ID = 'ID';
	const ATTR_XML_ID = 'XML_ID';
	const ATTR_ACTIVE = 'ACTIVE';
	const ATTR_ACTIVE_FROM = 'ACTIVE_FROM';
	const ATTR_NAME = 'NAME';
	const ATTR_BLOCK_ID = 'IBLOCK_ID';
	const ATTR_BLOCK_SECTION_ID = 'IBLOCK_SECTION_ID';


	public static function getDb() {
		return \Yii::$app->get('dbSite');
	}

	public static function tableName() {
		return 'b_iblock_element';
	}
}
