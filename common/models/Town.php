<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Модель Городов. Таблица Towns
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string $Name
 * @property string $Phone_Code
 * @property string $rowguid
 * @property string $Region_Num
 * @property int    $id_geobase
 */
class Town extends ActiveRecord {

	const ATTR_NAME = 'Name';
	const ATTR_PHONE_CODE = 'Phone_Code';
	const ATTR_GUID = 'rowguid';
	const ATTR_REGION_NUM = 'Region_Num';
	const ATTR_ID_GEOBASE = 'id_geobase';

	const DEFAULT_CITY_GUID = 'E6D830DB-FA47-4D7C-8D6E-5BD09E037E98';
	const NAKHODKA_CITY_GUID = 'BB2287A8-EB92-4CEE-A606-D09353452996';

	const NAME_GUID = [
		'vladivostok' => self::DEFAULT_CITY_GUID,
		'nakhodka'    => self::NAKHODKA_CITY_GUID,
	];

	const PHONE_GUID = [
		'4232516516' => self::DEFAULT_CITY_GUID,
		'4236692516' => self::NAKHODKA_CITY_GUID,
	];

	const GUID_RUS_NAME = [
		self::DEFAULT_CITY_GUID  => 'Владивосток',
		self::NAKHODKA_CITY_GUID => 'Находка',
	];

	public static function tableName() {
		return 'Towns';
	}

	/**
	 * @return array|mixed|\yii\db\ActiveRecord[]
	 *
	 * @author Исаков Владислав
	 */
	public static function getAll() {
		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, 1]);
		$cities = Yii::$app->cache->get($cacheKey);
		if (false === $cities) {
			$cities = static::find()->orderBy([static::ATTR_NAME => SORT_ASC])->all();
			$cities = ArrayHelper::map($cities, static::ATTR_GUID, static::ATTR_NAME);
			Yii::$app->cache->set($cacheKey, $cities, 3600 * 60 * 24);
		}

		return $cities;
	}
}