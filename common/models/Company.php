<?php

namespace common\models;

use common\models\site\SiteCompany;
use common\modules\news\models\SiteNews;
use Yii;
use yii\db\ActiveRecord;
use yii\validators\RequiredValidator;
use yii\validators\StringValidator;

/**
 * Модель Компаний. Таблица Firms
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string                                   $Official_Name
 * @property string                                   $Office
 * @property string                                   $Nick_Name
 * @property string                                   $Representative
 * @property string                                   $Step
 * @property string                                   $Description
 * @property string                                   $Counter
 * @property string                                   $Home
 * @property string                                   $TransAcc
 * @property string                                   $CorrAcc
 * @property string                                   $BIK
 * @property string                                   $INN
 * @property string                                   $OKONH
 * @property string                                   $OKPO
 * @property string                                   $WorkHours
 * @property string                                   $HowToGetScheme
 * @property string                                   $Postindex
 * @property string                                   $Pager
 * @property string                                   $URL
 * @property string                                   $Email
 * @property string                                   $Reg_Date
 * @property string                                   $End_Date
 * @property string                                   $Pri_Date
 * @property string                                   $Send_Address
 * @property int                                      $Show
 * @property int                                      $Internet_Show
 * @property int                                      $Color_Index
 * @property int                                      $Rotation
 * @property string                                   $rowguid
 * @property string                                   $Man_Country_num
 * @property string                                   $Region_num
 * @property string                                   $Area_num
 * @property string                                   $Town_num
 * @property string                                   $District_num
 * @property string                                   $Street_num
 * @property string                                   $Bank_num
 * @property string                                   $Ownership_Num
 * @property string                                   $currentbdt
 * @property string                                   $currentdt
 * @property string                                   $currentbreakbdt
 * @property string                                   $currentbreakedt
 * @property string                                   $timestring
 * @property string                                   $PNB_rowguid
 * @property string                                   $Number
 * @property string                                   $Address_rowguid
 * @property string                                   $CAgent_Num
 * @property string                                   $CrAgent_Num
 * @property string                                   $update_stamp
 *
 * @property-read \common\models\Town                 $town
 * @property-read \common\models\Region               $region
 * @property-read \common\models\District             $district
 * @property-read \common\models\Street               $street
 * @property-read \common\models\CompanyWorkHours     $workHours
 * @property-read \common\models\CompanyPhone[]       $phones
 * @property-read \common\models\ContractsLog[]       $contracts
 * @property-read \common\models\PriorityPrice[]      $activePriorityPrices
 * @property-read \common\models\ServiceName          $serviceNameOne
 * @property-read \common\models\ServiceName          $serviceNameTwo
 * @property-read \common\models\CompanyDistrictAlias $districtAlias
 * @property-read \common\models\Addresses[]          $addresses
 * @property-read SiteCompany                         $siteCompany
 * @property-read \common\models\ObjectFile[]         $objectFiles
 * @property-read \common\models\ObjectFile           $objectLogo
 * @property-read SiteNews[]                          $news
 *
 */
class Company extends ActiveRecord {

	const ATTR_GUID = 'rowguid';
	const ATTR_TOWN_NUM = 'Town_num';
	const ATTR_REGION_NUM = 'Region_num';
	const ATTR_AREA_NUM = 'Area_num';
	const ATTR_DISTRICT_NUM = 'District_num';
	const ATTR_STREET_NUM = 'Street_num';
	const ATTR_OFFICIAL_NAME = 'Official_Name';
	const ATTR_NICK_NAME = 'Nick_Name';
	const ATTR_COLOR_INDEX = 'Color_Index';
	const ATTR_HOME = 'Home';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_C_AGENT_GUID = 'CAgent_Num';
	const ATTR_CR_AGENT_GUID = 'CrAgent_Num';
	const ATTR_HOW_TO = 'HowToGetScheme';
	const ATTR_INTERNET_SHOW = 'Internet_Show';
	const ATTR_EMAIL = 'Email';

	const FAKE_ID = '000000000000000000000000';

	const OBJECT_COMPANY_IMAGES = 'object_company_images';

	public static function tableName() {
		return 'Firms';
	}

	/** @var string */
	public $logo;

	const COLORS = [
		1 => 'green-tr', //Зеленый
		2 => '', //?
		3 => 'red-tr', //Красный
		4 => '', //?
		5 => '#FFFFE0', //Нейтральный
		6 => 'black-tr', //Черный
	];
	const COLUMN_COLORS = [
		1 => 'cell-green', //Зеленый
		2 => '', //?
		3 => 'cell-red', //Красный
		4 => '', //?
		5 => '#FFFFE0', //Нейтральный
		6 => 'cell-black', //Черный
	];

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function attributeLabels() {
		return [
			static::ATTR_OFFICIAL_NAME => 'Официальное название',
		];
	}

	/**
	 * @return bool|string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public static function getImagePath() {
		return Yii::getAlias('@companyPhoto');
	}

	/**
	 * @return bool|string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public static function getLogoPath() {
		return Yii::getAlias('@companyLogo');
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function rules() {
		return [
			[static::ATTR_OFFICIAL_NAME, RequiredValidator::class],
			[static::ATTR_OFFICIAL_NAME, StringValidator::class],
		];
	}

	/**
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getTown() {
		return $this->hasOne(Town::class, [Town::ATTR_GUID => static::ATTR_TOWN_NUM]);
	}

	const REL_TOWN = 'town';

	/**
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getRegion() {
		return $this->hasOne(Region::class, [Region::ATTR_GUID => static::ATTR_REGION_NUM]);
	}

	const REL_REGION = 'region';

	/**
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getDistrict() {
		return $this->hasOne(District::class, [District::ATTR_GUID => static::ATTR_DISTRICT_NUM]);
	}

	const REL_DISTRICT = 'district';

	/**
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getStreet() {
		return $this->hasOne(Street::class, [Street::ATTR_GUID => static::ATTR_STREET_NUM]);
	}

	const REL_STREET = 'street';

	/**
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getWorkHours() {
		return $this->hasOne(CompanyWorkHours::class, [CompanyWorkHours::ATTR_FIRM_GUID => static::ATTR_GUID]);
	}

	const REL_WORK_HOURS = 'workHours';

	/**
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getPhones() {
		return $this->hasMany(CompanyPhone::class, [CompanyPhone::ATTR_FIRM_GUID => static::ATTR_GUID])
			->orderBy([CompanyPhone::tableName() . '.' . CompanyPhone::ATTR_NUMBER => SORT_ASC]);
	}

	const REL_PHONES = 'phones';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getContracts() {
		return $this->hasMany(ContractsLog::class, [ContractsLog::ATTR_COMPANY_GUID => static::ATTR_GUID])
			->orderBy([static::tableName() . '.' . ContractsLog::ATTR_END_DATE => SORT_DESC]);
	}

	const REL_CONTRACTS = 'contracts';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getActivePriorityPrices() {
		return $this->hasMany(PriorityPrice::class, [PriorityPrice::ATTR_FIRM_GUID => static::ATTR_GUID]);
		//->andWhere(['>=', new Expression(PriorityPrice::tableName() . '.' . PriorityPrice::ATTR_PRIORITY_END_DATE), new Expression('convert(varchar(10), GETDATE(), 111)')]);
	}

	const REL_ACTIVE_PRIORITY_PRICES = 'activePriorityPrices';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getServiceNameOne() {
		return $this->hasOne(ServiceName::class, [ServiceName::ATTR_GUID => PriorityPrice::ATTR_REFERENCE_ONE])
			->via(static::REL_ACTIVE_PRIORITY_PRICES);
	}

	const REL_SERVICE_NAME_ONE = 'serviceNameOne';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getServiceNameTwo() {
		return $this->hasOne(ServiceName::class, [ServiceName::ATTR_GUID => PriorityPrice::ATTR_REFERENCE_TWO])
			->via(static::REL_ACTIVE_PRIORITY_PRICES);
	}

	const REL_SERVICE_NAME_TWO = 'serviceNameTwo';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getDistrictAlias() {
		return $this->hasOne(CompanyDistrictAlias::class, [CompanyDistrictAlias::ATTR_COMPANY_GUID => static::ATTR_GUID]);
	}

	const REL_DISTRICT_ALIAS = 'districtAlias';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getAddresses() {
		return $this->hasMany(Addresses::class, [Addresses::ATTR_COMPANY_GUID => static::ATTR_GUID])
			->orderBy([Addresses::ATTR_POSITION => SORT_ASC]);
	}

	const REL_ADDRESSES = 'addresses';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getSiteCompany() {
		return $this->hasOne(SiteCompany::class, [SiteCompany::ATTR_GUID => static::ATTR_GUID]);
	}

	const REL_SITE_COMPANY = 'siteCompany';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getObjectFiles() {
		return $this->hasMany(ObjectFile::class, [ObjectFile::ATTR_OBJECT_GUID => static::ATTR_GUID])
			->andWhere([ObjectFile::ATTR_OBJECT => static::OBJECT_COMPANY_IMAGES]);
	}

	const REL_OBJECT_FILES = 'objectFiles';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getObjectLogo() {
		return $this->hasOne(ObjectFile::class, [ObjectFile::ATTR_OBJECT_GUID => static::ATTR_GUID])
			->andWhere([ObjectFile::ATTR_OBJECT => static::class]);
	}

	const REL_OBJECT_LOGO = 'objectLogo';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getNews() {
		return $this->hasMany(SiteNews::class, [SiteNews::ATTR_COMPANY_GUID => static::ATTR_GUID])->orderBy([SiteNews::ATTR_DATE => SORT_DESC]);
	}

	const REL_NEWS = 'news';

	/**
	 * @return ObjectFile|\yii\db\ActiveRecord|null
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getLogo() {
		return ObjectFile::find()
			->andWhere([ObjectFile::ATTR_OBJECT => static::class])
			->andWhere([ObjectFile::ATTR_OBJECT_GUID => $this->rowguid])
			->one();
	}
}