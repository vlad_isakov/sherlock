<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Модель звонков присутствующихв системе сейчас. Таблица CALLS
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @property int    $ID
 * @property int    $OP_ID
 * @property int    $GR_ID
 * @property string $TYPE
 * @property int    $LINE
 * @property string $INITS_TIME
 * @property int    $PRIORITY
 * @property bool   $ON_HOLD
 * @property string $ANUMBER
 * @property string $BNUMBER
 * @property int    $call_id
 * @property string $begin_time
 *
 *
 */
class Calls extends ActiveRecord {
	const ATTR_ID = 'ID';
	const ATTR_OP_ID = 'OP_ID';
	const ATTR_GR_ID = 'GR_ID';
	const ATTR_TYPE = 'TYPE';
	const ATTR_LINE = 'LINE';
	const ATTR_INITS_TIME = 'INITS_TIME';
	const ATTR_PRIORITY = 'PRIORITY';
	const ATTR_ON_HOLD = 'ON_HOLD';
	const ATTR_A_NUMBER = 'ANUMBER';
	const ATTR_B_NUMBER = 'BNUMBER';
	const ATTR_CALL_ID = 'call_id';
	const ATTR_BEGIN_TIME = 'begin_time';

	const TYPE_OP = 'OP';
	const TYPE_IN = 'IN';

	public static function getDb() {
		return \Yii::$app->get('dbCallcenter');
	}

	public static function tableName() {
		return 'CALLS';
	}
}