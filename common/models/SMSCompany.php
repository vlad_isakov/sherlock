<?php
namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * СМС-компания
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @property string $caption
 * @property string $rowguid
 * @property string $smstext
 * @property bool $deleted
 *
 */
class SMSCompany extends ActiveRecord {

	const ATTR_GUID = 'rowguid';
	const ATTR_CAPTION = 'caption';
	const ATTR_SMS_TEXT = 'smstext';
	const ATTR_IS_DELETED = 'deleted';

	public static function tableName() {
		return 'SMSList';
	}
}