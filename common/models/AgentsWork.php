<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Поля таблицы:
 * @property string $rowguid
 * @property string $Agent_Num
 * @property string $Firm_Num
 * @property string $Date_Start
 * @property string $Date_Finish
 *
 */
class AgentsWork extends ActiveRecord {

	const ATTR_GUID = 'rowguid';
	const ATTR_AGENT_GUID = 'Agent_Num';
	const ATTR_COMPANY_GUID = 'Firm_Num';
	const ATTR_DATE_START = 'Date_Start';
	const ATTR_DATE_FINISH = 'Date_Finish';

	public static function tableName() {
		return 'Agents_Work';
	}
}
