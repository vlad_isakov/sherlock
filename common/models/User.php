<?php

namespace common\models;

use common\base\validators\IntegerValidator;
use Yii;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;

/**
 * @property int                         $Number
 * @property int                         $Enabled
 * @property string                      $User_Name
 * @property string                      $Password
 * @property string                      $rowguid
 * @property int                         $logical_number
 * @property string                      $company_guid
 *
 * @property-read \common\models\Company $company
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface {
	public $id;
//	public $username;
	public $password;
	public $authKey;
	public $accessToken;

	const ATTR_ID = 'Number';
	const ATTR_GUID = 'rowguid';
	const ATTR_LOGICAL_NUMBER = 'logical_number';
	const ATTR_USER_NAME = 'User_Name';
	const ATTR_PASSWORD = 'Password';
	const ATTR_COMPANY_GUID = 'company_guid';

	public function getPrimaryKey($asArray = false) {
		return [static::ATTR_GUID];
	}

	public function attributeLabels() {
		return [
			static::ATTR_LOGICAL_NUMBER => 'ID в базе CallCenter'
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function getLogicalNumber() {
		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, Yii::$app->user->id, 1]);

		$logicalNumber = Yii::$app->cache->get($cacheKey);

		if (false === $logicalNumber) {
			/** @var User $login */
			$login = User::find()->where([User::ATTR_ID => Yii::$app->user->id])->one();
			$logicalNumber = $login->logical_number;

			if (!empty($logicalNumber)) {
				Yii::$app->cache->set($cacheKey, $logicalNumber, null, new TagDependency(['tags' => User::class]));
			}
		}

		return $logicalNumber;
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function rules() {
		return [
			[static::ATTR_LOGICAL_NUMBER, IntegerValidator::class],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public static function tableName() {
		return 'Logins';
	}

	/**
	 * {@inheritdoc}
	 */
	public static function findIdentity($id) {
		return static::findOne([static::ATTR_GUID => $id]);
	}

	/**
	 * {@inheritdoc}
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		//die;
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 *
	 * @return static|null
	 */
	public static function findByUsername($username) {
		/** @var \common\models\User $user */
		$user = static::find()->where(['User_Name' => $username, 'Enable' => 1])->one();

		if (null !== $user) {
			return $user;
		}

		return null;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getId() {
		return $this->rowguid;
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
	 */
	public function getUsername() {
		return $this->User_Name;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAuthKey() {
		return $this->rowguid;
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateAuthKey($authKey) {
		return $this->rowguid === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 *
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return $this->Password === $password;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getCompany() {
		return $this->hasOne(Company::class, [Company::ATTR_GUID => static::ATTR_COMPANY_GUID]);
	}

	const REL_COMPANY = 'company';
}
