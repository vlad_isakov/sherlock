<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Поля таблицы:
 * @property string                      $rowguid
 * @property string                      $Name
 * @property string                      $Description
 * @property string                      $Phone
 * @property string                      $Office
 * @property string                      $Home
 * @property string                      $Phone_NUm
 * @property string                      $Phone_Digits
 * @property int                         $Position
 * @property string                      $Town_Num
 * @property string                      $District_Num
 * @property string                      $Street_Num
 * @property string                      $Firm_Num
 * @property string                      $Name_Num
 * @property string                      $Region_Num
 * @property string                      $WorkHours
 *
 *
 * @property-read \common\models\Town    $city
 * @property-read \common\models\Street  $addrStreet
 * @property-read \common\models\Company $company
 *
 */
class Addresses extends ActiveRecord {

	const ATTR_GUID = 'rowguid';
	const ATTR_NAME = 'Name';
	const ATTR_STREET_GUID = 'Street_Num';
	const ATTR_CITY_GUID = 'Town_Num';
	const ATTR_COMPANY_GUID = 'Firm_Num';
	const ATTR_POSITION = 'Position';
	const ATTR_HOME = 'Home';

	public static function tableName() {
		return 'Addresses';
	}

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getCompany() {
		return $this->hasOne(Company::class, [Company::ATTR_GUID => static::ATTR_COMPANY_GUID]);
	}

	const REL_COMPANY = 'company';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getCity() {
		return $this->hasOne(Town::class, [Town::ATTR_GUID => static::ATTR_CITY_GUID]);
	}

	const REL_CITY = 'city';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getAddrStreet() {
		return $this->hasOne(Street::class, [Street::ATTR_GUID => static::ATTR_STREET_GUID]);
	}

	const REL_ADDR_STREET = 'addrStreet';
}
