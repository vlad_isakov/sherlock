<?php
/**
 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
 */

namespace common\models;


use yii\db\ActiveRecord;

/**
 * @property string $rowguid
 * @property string $row_rowguid
 * @property string $NamesList
 * @property string $price_rowguid
 *
 *
 * @author  Исаков Владислав <isakov.vi@dns-shop.ru>
 */
class PriceDistrictAlias extends ActiveRecord {
	const ATTR_GUID = 'rowguid';
	const ATTR_NAME_LIST = 'NamesList';
	const ATTR_PRICE_GUID = 'price_rowguid';
	const ATTR_ROW_GUID = 'row_rowguid';

	public static function tableName() {
		return 'PricePNames';
	}
}