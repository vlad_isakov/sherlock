<?php

namespace common\models;

use common\base\helpers\DateHelper;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Поля таблицы:
 * @property integer $id
 * @property integer $operator_id
 * @property integer $text
 * @property integer $user_id
 * @property string  $date
 * @property string  $cost
 * @property string  $phone
 * @property integer $status
 * @property string  $user_guid
 */
class Sms extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_OPERATOR_ID = 'operator_id';
	const ATTR_TEXT = 'text';
	const ATTR_USER_ID = 'user_id';
	const ATTR_DATE = 'date';
	const ATTR_COST = 'cost';
	const ATTR_STATUS = 'status';
	const ATTR_PHONE = 'phone';
	const ATTR_USER_GUID = 'user_guid';

	/**
	 * queued    сообщение в очереди отправки
	 * wait    передано оператору на отправку
	 * accepted    сообщение принято оператором, но статус доставки неизвестен
	 * delivered    сообщение доставлено
	 * not_delivered    сообщение не доставлено
	 * failed    ошибка при работе по сообщению
	 */

	const STATUS_WAIT_FOR_SEND = 0;
	const STATUS_SENT_TO_OPERATOR = 1;
	const STATUS_DELIVERED = 2;
	const STATUS_NOT_DELIVERED = 3;

	const STATUS_NAMES = [
		self::STATUS_WAIT_FOR_SEND    => 'Ожидает отправки',
		self::STATUS_SENT_TO_OPERATOR => 'Передано для отправки',
		self::STATUS_DELIVERED        => 'Доставлено',
		self::STATUS_NOT_DELIVERED    => 'Не доставлено',
	];

	const STATUS_COLOR = [
		self::STATUS_WAIT_FOR_SEND    => '',
		self::STATUS_SENT_TO_OPERATOR => '',
		self::STATUS_DELIVERED        => 'alert alert-success',
		self::STATUS_NOT_DELIVERED    => 'alert alert-danger',
	];

	const BEELINE_STATUSES = [
		'accepted'      => self::STATUS_SENT_TO_OPERATOR,
		'queued'        => self::STATUS_SENT_TO_OPERATOR,
		'wait'          => self::STATUS_SENT_TO_OPERATOR,
		'not_delivered' => self::STATUS_NOT_DELIVERED,
		'failed'        => self::STATUS_NOT_DELIVERED,
		'delivered'     => self::STATUS_DELIVERED,
	];

	public static function tableName() {
		return 'sms';
	}

	public function attributeLabels() {
		return [
			static::ATTR_ID          => 'id',
			static::ATTR_OPERATOR_ID => 'operator_id',
			static::ATTR_TEXT        => 'text',
			static::ATTR_USER_ID     => 'user_id',
			static::ATTR_DATE        => 'date',
			static::ATTR_COST        => 'cost',
			static::ATTR_STATUS      => 'status',
			static::ATTR_PHONE       => 'Телефон',
		];
	}

	/**
	 * @param string $userGuid
	 * @param int    $status
	 *
	 * @return int|string
	 *
	 * @author Исаков Владислав
	 */
	public static function getCountBuStatus($userGuid, $status) {
		return static::find()->andWhere([static::ATTR_USER_GUID => $userGuid])
			->andWhere([static::ATTR_STATUS => $status])
			->andWhere(['>', static::ATTR_DATE, new Expression('CAST(GETDATE() AS DATE)')])
			->count();
	}
}
