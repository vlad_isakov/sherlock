<?php
namespace common\models;


use yii\db\ActiveRecord;

/**
 * Модель Регионов. Таблица Regions
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string $Name
 * @property string $Phone_Code
 * @property string $rowguid
 *
 */
class Region extends ActiveRecord {

	const ATTR_NAME = 'Name';
	const ATTR_PHONE_CODE = 'Phone_Code';
	const ATTR_GUID = 'rowguid';

	public static function tableName() {
		return 'Regions';
	}
}