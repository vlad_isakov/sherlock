<?php

namespace common\models;

use common\base\helpers\StringHelper;
use common\helpers\Dump;
use yii\db\ActiveRecord;
use Yii;

/**
 * Поля таблицы:
 * @property string $rowguid
 * @property string $Folder_Name
 * @property string $Owner_Num
 *

 */
class Classifier extends ActiveRecord {
	const ATTR_GUID = 'rowguid';
	const ATTR_NAME = 'Folder_Name';
	const ATTR_PARENT_GUID = 'Owner_Num';

	/** @var \common\models\Classifier[] */
	public $childs;


	public static function tableName() {
		return 'Classifier';
	}

	/**
	 * Получение дерева категорий
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public static function getTree() {
		$cacheKey = Yii::$app->cache->buildKey(__METHOD__);
		/** @var \common\models\Classifier[] $mainFolders */
		$mainFolders = Yii::$app->cache->get($cacheKey);

		if (false === $mainFolders) {
			$mainFolders = static::find()
				->where([static::ATTR_PARENT_GUID => StringHelper::GUID_NULL])
				->orderBy([static::ATTR_NAME => SORT_ASC])
				->all();

			foreach ($mainFolders as &$mainFolder) {
				/** @var \common\models\Classifier[] $levelOneFolders */
				$levelOneFolders = static::find()
					->where([static::ATTR_PARENT_GUID => $mainFolder->rowguid])
					->orderBy([static::ATTR_NAME => SORT_ASC])
					->all();

				foreach ($levelOneFolders as &$levelOneFolder) {
					$levelTwoFolders = static::find()
						->where([static::ATTR_PARENT_GUID => $levelOneFolder->rowguid])
						->orderBy([static::ATTR_NAME => SORT_ASC])
						->all();

					$levelOneFolder->childs = $levelTwoFolders;

				}

				$mainFolder->childs = $levelOneFolders;
			}
			Yii::$app->cache->set($cacheKey, $mainFolders, 3600 * 24);
		}

		return $mainFolders;
	}
}
