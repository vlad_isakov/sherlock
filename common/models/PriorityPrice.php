<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Модель Товаров и Услуг
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string                                 $rowguid
 * @property int                                    $prioritet
 * @property string                                 $step
 * @property string                                 $Text1
 * @property string                                 $String1
 * @property string                                 $String2
 * @property string                                 $Float1
 * @property string                                 $Float2
 * @property int                                    $townprioritet
 * @property string                                 $PrioritetEndDate
 * @property string                                 $TownPrioritetEndDate
 *
 * @property-read \common\models\ServiceName        $referenceOne
 * @property-read \common\models\ServiceName        $referenceTwo
 * @property-read \common\models\Company            $company
 * @property-read \common\models\PriceDistrictAlias $districtAlias
 */
class PriorityPrice extends ActiveRecord {

	const ATTR_PRIORITY = 'prioritet';
	const ATTR_GUID = 'rowguid';
	const ATTR_STEP = 'step';
	const ATTR_TEXT = 'Text1';
	const ATTR_FLOAT_ONE = 'Float1';
	const ATTR_FLOAT_TWO = 'Float2';
	const ATTR_STRING_ONE = 'String1';
	const ATTR_STRING_TWO = 'String2';
	const ATTR_TOWN_PRIORITY = 'townprioritet';
	const ATTR_PRIORITY_END_DATE = 'PrioritetEndDate';
	const ATTR_TOWN_PRIORITY_END_DATE = 'PrioritetEndDate';
	const ATTR_FIRM_GUID = 'firm_num';
	const ATTR_REFERENCE_ONE = 'Reference1';
	const ATTR_REFERENCE_TWO = 'Reference2';

	public static function tableName() {
		return 'Price3';
	}

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getReferenceOne() {
		return $this->hasOne(ServiceName::class, [ServiceName::ATTR_GUID => static::ATTR_REFERENCE_ONE]);
	}

	const REL_REFERENCE_ONE = 'referenceOne';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getReferenceTwo() {
		return $this->hasOne(ServiceName::class, [ServiceName::ATTR_GUID => static::ATTR_REFERENCE_TWO]);
	}

	const REL_REFERENCE_TWO = 'referenceTwo';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getCompany() {
		return $this->hasOne(Company::class, [Company::ATTR_GUID => static::ATTR_FIRM_GUID]);
	}

	const REL_COMPANY = 'company';


	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getDistrictAlias() {
		return $this->hasOne(PriceDistrictAlias::class, [PriceDistrictAlias::ATTR_ROW_GUID => static::ATTR_GUID]);
	}

	const REL_DISTRICT_ALIAS = 'districtAlias';
}