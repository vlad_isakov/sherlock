<?php
namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * СМС-акция
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @property string $Class_Num
 *
 */
class ClassifierView extends ActiveRecord {

	const ATTR_CLASS_GUID = 'Class_Num';
	const ATTR_FOLDER_NAME = 'Folder_Name';
	const ATTR_NAME_GUID = 'Name_Rowguid';
	const ATTR_NAME = 'Name';
	const ATTR_COMMENTS = 'Comments';

	public static function tableName() {
		return 'ClassifierContent_VIEW';
	}

}