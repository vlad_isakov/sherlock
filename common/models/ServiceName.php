<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Модель Товаров и Услуг
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string $Name
 * @property string $rowguid
 * @property string $Group_Num
 * @property string $Class_Num
 *
 */
class ServiceName extends ActiveRecord {
	const ATTR_NAME = 'Name';
	const ATTR_GUID = 'rowguid';
	const ATTR_GROUP_NUM = 'Group_Num';
	const ATTR_CLASS_NUM = 'Class_Num';

	public static function tableName() {
		return 'Names';
	}
}