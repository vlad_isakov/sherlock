<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Модель Телефонов компании. Таблица Зрщтуы
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string                      $Number
 * @property string                      $Phone
 * @property string                      $Description
 * @property int                         $Flag_Fax
 * @property string                      $Digits
 * @property string                      $rowguid
 * @property string                      $Firm_Num
 *
 *
 * @property-read \common\models\Company $company
 */
class CompanyPhone extends ActiveRecord {

	const ATTR_NUMBER = 'Number';
	const ATTR_PHONE = 'Phone';
	const ATTR_DESCRIPTION = 'Description';
	const ATTR_FLAG_FAX = 'Flag_Fax';
	const ATTR_DIGITS = 'Digits';
	const ATTR_GUID = 'rowguid';
	const ATTR_FIRM_GUID = 'Firm_Num';

	public static function tableName() {
		return 'Phones';
	}

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function getCompany() {
		return $this->hasOne(Company::class, [Company::ATTR_GUID => static::ATTR_FIRM_GUID]);
	}

	const REL_COMPANY = 'company';

}