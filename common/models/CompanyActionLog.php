<?php

namespace common\models;

use yii\db\ActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Поля таблицы:
 * @property integer                     $id
 * @property string                      $user_guid
 * @property string                      $company_guid
 * @property int                         $action_type
 * @property string                      $message
 * @property string                      $reference_guid
 * @property string                      $insert_stamp
 * @property string                      $update_stamp
 *
 * @property-read \common\models\Company $company
 * @property-read \common\models\User    $user
 */
class CompanyActionLog extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_USER_GUID = 'user_guid';
	const ATTR_COMPANY_GUID = 'company_guid';
	const ATTR_ACTION_TYPE = 'action_type';
	const ATTR_MESSAGE = 'message';
	const ATTR_REFERENCE_GUID = 'reference_guid';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';

	const ACTION_OPEN_COMPANY = 0;
	const ACTION_SEND_SMS_BY_DEMAND = 1;
	const ACTION_SEND_SMS_ACTION = 2;
	const ACTION_SEND_SMS_COMPANY = 3;
	const ACTION_OPEN_ADV_NEWS = 4;

	const ACTION_NAMES = [
		self::ACTION_OPEN_COMPANY       => 'Предоставление информации о компании',
		self::ACTION_SEND_SMS_BY_DEMAND => 'Отправка контактных данных по просьбе клиента',
		self::ACTION_SEND_SMS_ACTION    => 'Отправка клиенту рекламной SMS(SMS-акция)',
		self::ACTION_SEND_SMS_COMPANY   => 'Отправка клиенту рекламной SMS(SMS-компания)',
		self::ACTION_OPEN_ADV_NEWS      => 'Предоставление информации из рекламной новости',
	];

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => 'insert_stamp',
				'updatedAtAttribute' => 'update_stamp',
				'value'              => new Expression('getdate()'),
			],
		];
	}

	public static function tableName() {
		return 'company_action_log';
	}

	public function attributeLabels() {
		return [
			static::ATTR_ID             => 'id',
			static::ATTR_USER_GUID      => 'Пользователь',
			static::ATTR_COMPANY_GUID   => 'Компания',
			static::ATTR_ACTION_TYPE    => 'Событие',
			static::ATTR_MESSAGE        => 'Сообщение',
			static::ATTR_REFERENCE_GUID => 'Связанная сущность',
			static::ATTR_INSERT_STAMP   => 'Дата',
			static::ATTR_UPDATE_STAMP   => 'action_type',
		];
	}

	/**
	 * Добавление записи события
	 *
	 * @param string      $companyGuid
	 * @param int         $actionId
	 * @param string      $message
	 * @param string|null $referenceGuid
	 *
	 * @author Исаков Владислав
	 */
	public static function add($companyGuid, $actionId, $message, $referenceGuid = null) {
		$log = new static();
		$log->user_guid = Yii::$app->user->id;
		$log->company_guid = $companyGuid;
		$log->action_type = $actionId;
		$log->message = $message;
		$log->reference_guid = $referenceGuid;
		$log->insert(false);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 *
	 */
	public function getCompany() {
		return $this->hasOne(Company::class, [Company::ATTR_GUID => static::ATTR_COMPANY_GUID]);
	}

	const REL_COMPANY = 'company';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 *
	 */
	public function getUser() {
		return $this->hasOne(User::class, [User::ATTR_GUID => static::ATTR_USER_GUID]);
	}

	const REL_USER = 'user';
}
