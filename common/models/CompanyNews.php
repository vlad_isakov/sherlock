<?php
namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Новости
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @property string $rowguid
 * @property string Content
 */
class CompanyNews extends ActiveRecord {

	const ATTR_GUID = 'rowguid';

	const ATTR_CONTENT = 'Content';
	const ATTR_START_DATE = 'Date_From';
	const ATTR_END_DATE = 'Date_To';
	const ATTR_TOWN_GUID = 'town_num';

	public static function tableName() {
		return 'News';
	}

	/**
	 * Получение действующего текста СМС-акции по услуге
	 *
	 * @param string $guid
	 *
	 * @return bool|string
	 *
	 * @author Исаков Владислав
	 */
	public static function getText($guid) {
		if (null === $guid || empty($guid)) {
			return false;
		}

        //Ищем сначала смс-акцию к конкретной услуге
		/** @var static $news */
		$news = static::find()
			->andWhere([static::ATTR_GUID => $guid])
			->one();

		//return $news->Content;
		return str_replace(PHP_EOL, '<br>', $news->Content);
	}
}