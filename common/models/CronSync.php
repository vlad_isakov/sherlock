<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Данные синхронизаций
 *
 * @property int    $id
 * @property int    $task
 * @property int    $last_info
 * @property int    $status
 * @property string $last_date
 *
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class CronSync extends ActiveRecord {
	const ATTR_ID = 'id';
	const ATTR_TASK = 'task';
	const ATTR_LAST_INFO = 'last_info';
	const ATTR_STATUS = 'status';
	const ATTR_LAST_DATE = 'last_date';

	const TASK_SYNC_COMPANY_WITH_SITE = 0;
	const TASK_SMS = 1;
	const TASK_ACTUALIZE_CONTRACTS = 2;
	const TASK_CHECK_SMS = 3;
	const TASK_CONVERT_AGENT_NUM = 4;

	const TASK_NAMES = [
		self::TASK_SYNC_COMPANY_WITH_SITE => 'Синхронизация компаний с сайтом',
		self::TASK_ACTUALIZE_CONTRACTS    => 'Актуализация прайсов',
		self::TASK_SMS                    => 'Отправка SMS',
		self::TASK_CHECK_SMS              => 'Запрос статусов отправленных SMS',
		self::TASK_CONVERT_AGENT_NUM      => 'Привязка компаний к торговым представителям'
	];

	const TASK_PERIOD = [
		self::TASK_SYNC_COMPANY_WITH_SITE => 3600 * 24,
		self::TASK_ACTUALIZE_CONTRACTS    => 3600 * 24,
		self::TASK_SMS                    => 60,
		self::TASK_CHECK_SMS              => 60,
		self::TASK_CONVERT_AGENT_NUM      => 5
	];

	const STATUS_WAITING = 0;
	const STATUS_PROCESS = 1;

	const STATUS_NAMES = [
		self::STATUS_WAITING => 'Ожидает выполнения',
		self::STATUS_PROCESS => 'Выполняется',
	];

	const CLASS_STATUS = [
		self::STATUS_WAITING => 'alert alert-info',
		self::STATUS_PROCESS => 'alert alert-success',
	];

	public static function tableName() {
		return 'cron_sync';
	}

	public function attributeLabels() {
		return [
			static::ATTR_TASK      => 'Задача',
			static::ATTR_LAST_INFO => 'Последнии сообщения',
			static::ATTR_STATUS    => 'Статус',
			static::ATTR_LAST_DATE => 'Последний запуск',
		];
	}

	/**
	 * Запуск задачи
	 *
	 * @param int $taskId
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public static function startTask($taskId) {
		/** @var static $task */
		$task = static::find()->where([static::ATTR_TASK => $taskId])->one();
		if (null === $task) {
			$task = new CronSync();
			$task->task = $taskId;
		}
		$task->last_date = new Expression('GETDATE()');
		$task->status = static::STATUS_PROCESS;
		$task->save();
	}

	/**
	 * Завершение задачи
	 *
	 * @param int    $taskId
	 * @param string $message
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public static function finishTask($taskId, $message = '') {
		/** @var static $task */
		$task = static::find()->where([static::ATTR_TASK => $taskId])->one();
		if (null === $task) {
			return;
		}

		$task->status = static::STATUS_WAITING;
		$task->last_info = $message;
		$task->last_date = new Expression('GETDATE()');
		$task->save();
	}
}