<?php
namespace common\models;

use yii\db\ActiveRecord;

/**
 * Модель логов звонков. Таблица CALLOPER_LOG
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string $PHONE
 *
 *
 */
class CallOperatorLog extends ActiveRecord {

	const ATTR_CALL_ID = 'CALL_ID';
	const ATTR_PHONE = 'PHONE';
	const ATTR_TIME_STAMP = 'TIME_STAMP';

	public static function getDb() {
		return \Yii::$app->get('dbCallcenter');
	}

	public static function tableName() {
		return 'CALLOPER_LOG';
	}
}