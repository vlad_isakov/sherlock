<?php
namespace common\models;

use yii\db\ActiveRecord;

/**
 * Модель Городов. Таблица Towns
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string $Begin_Date
 * @property string $End_Date
 * @property string $Contract_Label
 * @property string $Status
 * @property string $rowguid
 * @property string $Firm_Num
 * @property string $Agent_Num
 *
 */
class ContractsLog extends ActiveRecord {

	const ATTR_BEG_DATE = 'Begin_Date';
	const ATTR_END_DATE = 'End_Date';
	const ATTR_CONTRACT_LABEL = 'Contract_Label';
	const ATTR_STATUS = 'Status';
	const ATTR_COMPANY_GUID = 'Firm_Num';
	const ATTR_AGENT_GUID = 'Agent_Num';
	const ATTR_GUID = 'rowguid';

	public static function tableName() {
		return 'Contracts_Log';
	}
}