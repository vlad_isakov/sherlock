<?php
namespace common\models;

use yii\db\ActiveRecord;

/**
 * Модель Улиц. Таблица Streets
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string $name
 * @property string $rowguid
 *
 */
class Street extends ActiveRecord {

	const ATTR_NAME = 'name';
	const ATTR_GUID = 'rowguid';

	public static function tableName() {
		return 'Streets';
	}
}