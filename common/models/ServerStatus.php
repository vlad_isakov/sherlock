<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Модель лога, записанного в базу.
 *
 * Столбцы в таблице:
 * @property int    $id             Уникальный идентификатор
 * @property string $name
 * @property string $server_ip
 * @property string $status
 * @property string $log_time
 *
 */
class ServerStatus extends ActiveRecord {
	const ATTR_ID = 'id';
	const ATTR_LOG_TIME = 'log_time';
	const ATTR_SERVER_IP = 'server_ip';
	const ATTR_STATUS = 'status';

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => 'log_time',
				'updatedAtAttribute' => 'log_time',
				'value'              => new Expression('getdate()'),
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public static function tableName() {
		return '{{%server_status}}';
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID        => 'Идентификатор',
			static::ATTR_SERVER_IP => 'IP',
			static::ATTR_LOG_TIME  => 'Дата и время',
			static::ATTR_STATUS    => 'Статус',
		];
	}
}