<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Модель режима работы компании. Таблица FirmsWorksHours
 *
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @property string $rowguid
 * @property string $firm_rowguid
 * @property string $timestring      Часы работы по дням
 * @property string $breaktimestring Обеденный перерыв
 *
 */
class CompanyWorkHours extends ActiveRecord {

	const ATTR_GUID = 'rowguid';
	const ATTR_FIRM_GUID = 'firm_rowguid';
	const ATTR_TIMESTRING = 'timestring';
	const ATTR_BREAK_TIMESTRING = 'breaktimestring';

	public static function tableName() {
		return 'FirmsWorksHours';
	}
}