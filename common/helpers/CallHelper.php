<?php

namespace common\helpers;

use common\components\Call;
use common\models\Calls;
use common\models\Town;
use common\models\User;
use Yii;
use yii\caching\TagDependency;

/**
 *
 * Хелпер для работы во звонками
 *
 * @author  Исаков Владислав <isakov.vi@dns-shop.ru>
 */
class CallHelper {

	/**
	 * Получение логического номера пользователя
	 *
	 * @param int $userId
	 *
	 * @return \common\models\User|mixed|null
	 *
	 * @author Исаков Владислав
	 */
	public static function _getLogicalNumber($userId) {
		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, $userId, 1]);
		$logicalNumber = Yii::$app->cache->get($cacheKey);
		if (false === $logicalNumber) {
			$logicalNumber = 999;
			$user = User::findOne([User::ATTR_GUID => $userId]);
			if (null !== $logicalNumber) {
				$logicalNumber = $user->logical_number;
			}

			Yii::$app->cache->set($cacheKey, $logicalNumber, null, new TagDependency(['tags' => User::class]));
		}

		return $logicalNumber;
	}

	/**
	 * Ключь кэша активного звонка для оператора
	 *
	 * @param int $logicalNumber
	 *
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public static function getCallCacheKey($logicalNumber) {
		return Yii::$app->cache->buildKey([__METHOD__, $logicalNumber]);
	}

	/**
	 * @param \common\components\Call $call
	 *
	 * @author Исаков Владислав
	 */
	public static function setNewCall($call) {
		$cacheKey = static::getCallCacheKey($call->logicalNumber);

		Yii::$app->cache->set($cacheKey, $call, null);

		$cacheKey = Yii::$app->cache->buildKey(['lastPhoneNumber', $call->logicalNumber]);

		Yii::$app->cache->set($cacheKey, 8 . $call->aNumber, 3600 * 10);
	}

	/**
	 *
	 * @return mixed
	 *
	 * @author Исаков Владислав
	 */
	public static function getLastPhoneNumber() {
		$cacheKey = Yii::$app->cache->buildKey(['lastPhoneNumber', static::_getLogicalNumber(Yii::$app->user->id)]);

		return Yii::$app->cache->get($cacheKey);
	}

	/**
	 * @param bool $connected
	 *
	 * @return \common\components\Call | false
	 *
	 * @author Исаков Владислав
	 */
	public static function getNewCall($connected = true) {

		if ($connected) {
			$call = static::_getConnectedCall();
		}
		else {
			$call = static::_getRinging();
		}

		if (null === $call) {
			return null;
		}

		if (array_key_exists($call->BNUMBER, Town::PHONE_GUID)) {
			$cityGuid = Town::PHONE_GUID[$call->BNUMBER];
		}
		else {
			$cityGuid = Town::DEFAULT_CITY_GUID;
		}

		$aNumber = $call->ANUMBER;
		$clientName = '';
		switch ($aNumber) {
			case '9146640629':
				$clientName = 'Исаков Владислав';
				break;
			case '9146968676':
			case '9151715000':
			case '9913943314':
				$clientName = 'Мокрушин Александр Иванович';
				break;
			case '9242548537':
				$clientName = 'Оржанцев Андрей Романович';
				break;
			default:
				break;
		}

		$callObj = new Call([
				Call::ATTR_ID          => $call->ID,
				Call::ATTR_A_NUMBER    => $aNumber,
				Call::ATTR_B_NUMBER    => $call->BNUMBER,
				Call::ATTR_CITY_GUID   => $cityGuid,
				Call::ATTR_CITY_NAME   => Town::GUID_RUS_NAME[$cityGuid],
				Call::ATTR_CLIENT_NAME => $clientName
			]
		);

		return $callObj;
	}

	/**
	 * @return \common\models\Calls
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	private static function _getRinging() {
		/** @var \common\models\Calls $call */
		$call = Calls::find()
			->andWhere([Calls::ATTR_OP_ID => static::_getLogicalNumber(Yii::$app->user->id)])
			->andWhere([Calls::ATTR_TYPE => Calls::TYPE_OP])
			->andWhere([Calls::ATTR_BEGIN_TIME => null])
			->one();

		return $call;
	}

	/**
	 * @return \common\models\Calls
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	private static function _getConnectedCall() {
		/** @var \common\models\Calls $call */
		$call = Calls::find()
			->andWhere([Calls::ATTR_OP_ID => static::_getLogicalNumber(Yii::$app->user->id)])
			->andWhere([Calls::ATTR_TYPE => Calls::TYPE_OP])
			->andWhere(['NOT', [Calls::ATTR_BEGIN_TIME => null]])
			->one();

		return $call;
	}
}