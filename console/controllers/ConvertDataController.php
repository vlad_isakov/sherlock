<?php

namespace console\controllers;

use common\models\Company;
use common\models\ObjectFile;
use common\models\site\SiteCompany;
use common\modules\news\models\SiteNews;
use Yii;
use yii\console\Controller;

/**
 * Загрузка и конвертирование данных со старого сайта
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class ConvertDataController extends Controller {

	const USER_AGENTS = [
		'Mozilla/5.0 (Linux; Android 7.0; BLL-L22 Build/HUAWEIBLL-L22) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36',
		'Mozilla/5.0 (Linux; Android 5.1.1; A37fw Build/LMY47V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.84 Mobile Safari/537.36',
		'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0',
		'Mozilla/5.0 (Linux; Android 4.4.2; de-de; SAMSUNG GT-I9195 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/1.5 Chrome/28.0.1500.94 Mobile Safari/537.36',
		'Mozilla/5.0 (Linux; U; Android 2.2.1; en-us; Nexus One Build/FRG83) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
		'Mozilla/5.0 (Linux; Android 6.0.1; SM-G532M Build/MMB29T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36',
		'Mozilla/5.0 (Linux; Android 5.1; A37f Build/LMY47V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.93 Mobile Safari/537.36',
		'Mozilla/5.0 (Linux; Android 6.0; vivo 1713 Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.124 Mobile Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 YaBrowser/17.3.1.840 Yowser/2.5 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 YaBrowser/17.3.1.840 Yowser/2.5 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 YaBrowser/17.1.0.2034 Yowser/2.5 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 YaBrowser/17.9.1.768 Yowser/2.5 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 YaBrowser/18.3.1.1232 Yowser/2.5 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
	];

	/**
	 * Загрузка новостей
	 *
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\Exception
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionNews() {
		/** @var \yii\db\Connection $dbOldSite */
		$dbOldSite = Yii::$app->get('dbSite');
		$news = $dbOldSite->createCommand("select * from b_iblock_element where IBLOCK_ID = 8 and DETAIL_TEXT_TYPE = 'html'")->queryAll();

		$i = 1;
		$newsCount = count($news);
		foreach ($news as $newsItem) {
			CronController::showStatus($i, $newsCount);
			$i++;
			$siteNews = SiteNews::find()
				->andWhere([SiteNews::ATTR_OLD_ID => $newsItem['ID']])
				->one();

			if (null !== $siteNews) {
				continue;
			}

			$company = $dbOldSite
				->createCommand('select PROPERTY_31 from b_iblock_element_prop_s8 where IBLOCK_ELEMENT_ID = ' . $newsItem['ID'])
				->queryOne();

			if (null === $company) {
				continue;
			}

			/** @var SiteCompany $company */
			$company = SiteCompany::find()->andWhere([SiteCompany::ATTR_OLD_ID => $company['PROPERTY_31']])->one();

			if (null === $company) {
				continue;
			}

			$siteNews = new SiteNews();
			$siteNews->company_guid = $company->guid;
			$siteNews->old_id = $newsItem['ID'];
			$siteNews->date = $newsItem['DATE_CREATE'];
			$siteNews->category_id = (null === $newsItem['DETAIL_PICTURE'] ? SiteNews::CATEGORY_ACTION : SiteNews::CATEGORY_NEWS);
			$siteNews->title = $newsItem['NAME'];
			if (null === $newsItem['DETAIL_TEXT']) {
				$newsItem['DETAIL_TEXT'] = '';
			}
			$siteNews->text = $newsItem['DETAIL_TEXT'];
			$siteNews->active_from = $newsItem['ACTIVE_FROM'];
			$siteNews->active_to = $newsItem['ACTIVE_TO'];
			$siteNews->is_published = ($newsItem['ACTIVE'] == 'Y' ? 1 : 0);

			if ($siteNews->save()) {
				$this->_downloadNewsImage($siteNews->id, $newsItem['DETAIL_PICTURE']);
			}
		}
	}

	/**
	 * Загрузка компаний
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionCompany() {
		/** @var \yii\db\Connection $dbOldSite */
		$dbOldSite = Yii::$app->get('dbSite');
		$companies = $dbOldSite->createCommand('select * from b_iblock_element where IBLOCK_ID = 6')->queryAll();
		$i = 1;
		$companyCount = count($companies);
		foreach ($companies as $company) {
			CronController::showStatus($i, $companyCount);
			$i++;
			/** @var SiteCompany $siteCompany */
			$siteCompany = SiteCompany::find()->andWhere([SiteCompany::ATTR_OLD_ID => $company['ID']])->one();

			if (null !== $siteCompany) {
				$this->_downloadLogoImage($siteCompany->guid, $company['PREVIEW_PICTURE']);
				$this->_downloadCompanyImages($siteCompany->old_id, $siteCompany->guid);
				continue;
			}

			$siteCompany = new SiteCompany();
			$siteCompany->old_id = $company['ID'];
			$siteCompany->guid = $company['XML_ID'];
			if ($siteCompany->save()) {
				$this->_downloadLogoImage($siteCompany->guid, $company['PREVIEW_PICTURE']);
				$this->_downloadCompanyImages($siteCompany->old_id, $siteCompany->guid);
			}
		}
	}

	private function _downloadLogoImage($companyGuid, $imageId) {
		if (empty($imageId)) {
			return;
		}

		$toPath = Company::getLogoPath();
		$fileName = $this->_downloadImage($imageId, $toPath);
		if (false !== $fileName) {
			$objectFile = new ObjectFile();
			$objectFile->object_guid = $companyGuid;
			$objectFile->object = Company::class;
			$objectFile->filename = $fileName;
			$objectFile->save();
		}
	}

	private function _downloadNewsImage($newsId, $imageId) {
		if (empty($imageId)) {
			return;
		}

		$toPath = SiteNews::getLogoPath();
		$fileName = $this->_downloadImage($imageId, $toPath);
		if (false !== $fileName) {
			$objectFile = new ObjectFile();
			$objectFile->object_id = $newsId;
			$objectFile->object = SiteNews::class;
			$objectFile->filename = $fileName;
			$objectFile->save();
		}

		$siteNews = SiteNews::findOne($newsId);
		$siteNews->image = $fileName;
		$siteNews->save();
	}

	private function _downloadCompanyImages($companyId, $companyGuid) {
		/** @var \yii\db\Connection $dbOldSite */
		$dbOldSite = Yii::$app->get('dbSite');
		$images = $dbOldSite
			->createCommand('select PROPERTY_32 from b_iblock_element_prop_s6 where IBLOCK_ELEMENT_ID =' . $companyId)
			->queryOne();

		$images = unserialize($images['PROPERTY_32']);
		$toPath = Company::getImagePath();

		if (empty($images['VALUE'])) {
			return;
		}

		foreach ($images['VALUE'] as $imageId) {
			$fileName = $this->_downloadImage($imageId, $toPath);
			if (false !== $fileName) {
				$objectFile = new ObjectFile();
				$objectFile->object_guid = $companyGuid;
				$objectFile->object = Company::OBJECT_COMPANY_IMAGES;
				$objectFile->filename = $fileName;
				$objectFile->save();
			}
		}
	}

	private function _downloadImage($fileId, $pathTo) {
		/** @var \yii\db\Connection $dbOldSite */
		$dbOldSite = Yii::$app->get('dbSite');

		$fileInfo = $dbOldSite->createCommand('select * from b_file where id = ' . $fileId)->queryOne();

		$fileName = $fileInfo['FILE_NAME'];

		if (empty($fileName)) {
			return false;
		}

		$filePathName = $fileInfo['SUBDIR'] . '/' . $fileName;

		$pathTo = $pathTo . DIRECTORY_SEPARATOR . substr($fileName, 0, 2);

		if (!is_dir($pathTo)) {
			mkdir($pathTo);
		}

		$pathTo = $pathTo . DIRECTORY_SEPARATOR . $fileName;

		$index = rand(0, count(static::USER_AGENTS) - 1);

		$command = 'wget -U "' . static::USER_AGENTS[$index] . '" -c -T 60 -O ' . $pathTo . ' ' . 'http://www.516.ru/upload/' . $filePathName;
		exec($command, $output, $status);

		return $fileName;
	}
}