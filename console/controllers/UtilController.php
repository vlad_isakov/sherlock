<?php
/**
 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
 */

namespace console\controllers;


use common\components\SmsProc;
use common\models\GeobaseCity;
use common\models\Town;
use Yii;
use yii\console\Controller;
use yii\db\Connection;
use yii\di\Instance;

class UtilController extends Controller {
	public $db = 'db';
	public $createTb;
	public $fieldUpper = true;

	public function options($actionID) {
		return ['createTb', 'db', 'scheme', 'fieldUpper'];
	}

	public function optionAliases() {
		return ['tb' => 'createTb', 'db' => 'db', 'fieldUpper' => 'fieldUpper'];
	}

	/**
	 * Создние модели на основе БД
	 *
	 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
	 */
	public function actionCreateModel() {
		/** @var \yii\db\Connection $db */
		$db = Yii::$app->get($this->db);

		$db = Instance::ensure($db, Connection::class);
		$tableSchema = $db->schema->getTableSchema($this->createTb);
		//print_r($tableSchema);
		//die;
		$classFile = '<?php ' . PHP_EOL . PHP_EOL;

		$classFile .= 'namespace common\models;' . PHP_EOL . PHP_EOL;


		$classFile .= 'use yii\db\ActiveRecord;' . PHP_EOL . PHP_EOL;

		$classFile .= '/**' . PHP_EOL . PHP_EOL;
		$classFile .= '* Поля таблицы:' . PHP_EOL;

		foreach ($tableSchema->columns as $columnName => $columnParam) {
			$fieldName = strtolower($columnName);
			if ($this->fieldUpper) {
				$fieldName = strtoupper($columnName);
			}
			$classFile .= '* @property ' . $columnParam->phpType . ' $' . $fieldName . PHP_EOL;
		}

		$classFile .= '*/' . PHP_EOL . PHP_EOL;

		$classFile .= 'class ' . ucfirst(strtolower($this->createTb)) . ' extends ActiveRecord {' . PHP_EOL . PHP_EOL;

		foreach ($tableSchema->columns as $columnName => $columnParam) {
			$fieldName = $columnName;
			if ($this->fieldUpper) {
				$fieldName = strtoupper($columnName);
			}

			$classFile .= "	const ATTR_" . strtoupper($columnName) . " = '" . $fieldName . "';" . PHP_EOL;
		}

		$classFile .= PHP_EOL . PHP_EOL;

		$classFile .= '	public static function tableName() {' . PHP_EOL;

		$classFile .= "		return ' . $this->createTb . ';" . PHP_EOL;

		$classFile .= '	}' . PHP_EOL . PHP_EOL;

		$classFile .= '	public function attributeLabels() {' . PHP_EOL;
		$classFile .= '		return [' . PHP_EOL;
		foreach ($tableSchema->columns as $columnName => $columnParam) {
			$classFile .= "			static::ATTR_" . strtoupper($columnName) . " => '" . $columnName . "'," . PHP_EOL;
		}

		$classFile .= '		];' . PHP_EOL;
		$classFile .= '	}' . PHP_EOL;

		$classFile .= '}' . PHP_EOL;

		$path = 'common' . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR;

		file_put_contents($path . '_' . ucfirst(strtolower($this->createTb)) . '.php', $classFile);
	}

	public function actionTestSms() {

		$sms = new SmsProc([
			SmsProc::ATTR_ACTION => SmsProc::ACTION_SEND,
			SmsProc::ATTR_PHONE  => '+79146640629',
			SmsProc::ATTR_TEXT   => 'test test 4',
		]);

		$result = simplexml_load_string($sms->send());

		echo $result->result->sms['id'];
	}

	public function actionSearchInBase() {
		$sql = 'SELECT CONCAT(
           \'SELECT "\', c.table_name, \'" `$table$`, \', GROUP_CONCAT(
        CONCAT(\'SUM(IF(`\', c.column_name, \'` LIKE "%search_text%", 1, 0)) `\', c.column_name, \'`\')
        SEPARATOR \', \'
      ),
           \' FROM `\', c.table_schema, \'`.`\', c.table_name, \'`\'
             \' WHERE \', GROUP_CONCAT(
               CONCAT(\'`\', c.column_name, \'` LIKE "%search_text%"\')
               SEPARATOR \' OR \'
             )
         ) query
FROM information_schema.columns c
WHERE c.table_schema = \'h_www\'
  AND c.data_type IN (
                      \'char\', \'varchar\', \'binary\', \'varbinary\',
                      \'tinytext\', \'text\', \'mediumtext\', \'longtext\',
                      \'tinyblob\', \'blob\', \'mediumblob\', \'longblob\'
  )
GROUP BY c.table_name;';
		/** @var \yii\db\Connection $siteDb */
		$siteDb = Yii::$app->get('dbSite');
		$sqlRows = $siteDb->createCommand($sql)->queryAll();
		//print_r($sqlRows);
		//die;
		$result = [];
		$text = '1278712';
		foreach ($sqlRows as $sql) {

			$sqlQuery = str_replace('search_text', $text, $sql['query']);
			if (false !== strpos($sqlQuery, 'banner')) {
				continue;
			}
			try {
				$sqlResult = $siteDb->createCommand($sqlQuery)->queryOne();
			}
			catch (\Exception $e) {
				continue;
			}
			$result[] = $sqlResult;

			//print_r($sqlQuery);
		}
		file_put_contents('log_query.txt', json_encode($result));
		print_r($result);
	}

	/**
	 * Привязка справочника городов к базе геолокации
	 *
	 * @throws \yii\db\Exception
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionLinkGeoTown() {
		/** @var GeobaseCity[] $geoCities */
		$geoCities = GeobaseCity::find()->all();
		foreach ($geoCities as $geoCity) {
			/** @var Town $town */
			$town = Town::findOne([Town::ATTR_NAME => $geoCity->name]);
			if (null === $town) {
				continue;
			}
			$town->id_geobase = $geoCity->id;
			$town->save();
		}
	}
}