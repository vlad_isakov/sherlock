<?php

namespace console\controllers;

use common\base\helpers\DateHelper;
use common\base\helpers\StringHelper;
use common\components\LocalServers;
use common\components\SmsProc;
use common\models\AgentsWork;
use common\models\Company;
use common\models\ContractsLog;
use common\models\CronSync;
use common\models\ServerStatus;
use common\models\site\BlockElement;
use common\models\site\BlockElementProp;
use common\models\Sms;
use Yii;
use yii\console\Controller;
use yii\db\Expression;

/**
 * Контроллер заданий крона
 *
 */
class CronController extends Controller {

	/**
	 * Обновление базы данных ip адресов //раз в неделю
	 *
	 * @throws \yii\base\Exception
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionUpdateIpGeoBase() {
		Yii::$app->ipgeobase->updateDB();
	}

	/**
	 * Отправка SMS
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionSendSms() {
		CronSync::startTask(CronSync::TASK_SMS);
		/** @var SMS[] $smsModels [] */
		$smsModels = Sms::find()
			->where([Sms::ATTR_STATUS => Sms::STATUS_WAIT_FOR_SEND])
			->all();

		foreach ($smsModels as $smsModel) {
			if (empty($smsModel->phone)) {
				continue;
			}

			//Добавляем +7 к номеру телефона
			if (substr($smsModel->phone, 0, 1) != '8') {
				$smsModel->phone = '+7' . $smsModel->phone;
			}
			else {
				$smsModel->phone = '+7' . substr($smsModel->phone, 1, 12);
			}

			$sms = new SmsProc([
				SmsProc::ATTR_ACTION => SmsProc::ACTION_SEND,
				SmsProc::ATTR_PHONE  => $smsModel->phone,
				SmsProc::ATTR_TEXT   => $smsModel->text,
			]);

			$response = simplexml_load_string($sms->send());

			$smsModel->operator_id = $response->result->sms['id'];
			$smsModel->status = Sms::STATUS_SENT_TO_OPERATOR;
			$smsModel->save();
		}

		CronSync::finishTask(CronSync::TASK_SMS, 'Обработано: ' . count($smsModels) . ' SMS');
	}

	/**
	 * Проверка статусов отправленных СМС
	 *
	 * @author Исаков Владислав
	 */
	public function actionCheckSmsStatus() {
		CronSync::startTask(CronSync::TASK_CHECK_SMS);
		/** @var SMS[] $smsModels [] */
		$smsModels = Sms::find()
			->where([Sms::ATTR_STATUS => Sms::STATUS_SENT_TO_OPERATOR])
			->all();

		foreach ($smsModels as $smsModel) {
			$sms = new SmsProc([
				SmsProc::ATTR_ID => $smsModel->operator_id
			]);

			$response = simplexml_load_string($sms->checkStatus());

			/** @var string $beelineStatus */
			$beelineStatus = (string)$response->MESSAGES->MESSAGE->SMSSTC_CODE;

			if (array_key_exists($beelineStatus, Sms::BEELINE_STATUSES)) {
				$smsModel->status = Sms::BEELINE_STATUSES[$beelineStatus];
				$smsModel->save();
			}
		}

		CronSync::finishTask(CronSync::TASK_CHECK_SMS, 'Обработано: ' . count($smsModels) . ' SMS');
	}

	/**
	 * Обновление фирм по действующим контрактам
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 *
	 */
	public function actionActualizeContracts() {
		CronSync::startTask(CronSync::TASK_ACTUALIZE_CONTRACTS);
		/** @var Company[] $companies */
		$companies = Company::find()
			->joinWith(Company::REL_CONTRACTS)
			->andWhere(['<=', new Expression(ContractsLog::tableName() . '.' . ContractsLog::ATTR_BEG_DATE), new Expression('convert(varchar(10), GETDATE(), 111)')])
			->andWhere(['>=', new Expression(ContractsLog::tableName() . '.' . ContractsLog::ATTR_END_DATE), new Expression('convert(varchar(10), GETDATE(), 111)')])
			->all();

		$index = 0;

		foreach ($companies as $company) {
			$company->Color_Index = 1;
			$company->End_Date = $company->contracts[0]->End_Date;
			if ($company->save()) {
				$index++;
			}

			echo $company->contracts[0]->Begin_Date . ' - ' . $company->contracts[0]->End_Date . PHP_EOL;
		}

		$message = 'Зеленых: ' . $index . ' компаний' . PHP_EOL;

		$index = 0;

		$companies = Company::find()
			->joinWith(Company::REL_CONTRACTS)
			->andWhere([Company::ATTR_COLOR_INDEX => 1])
			->andWhere(['<', new Expression(ContractsLog::tableName() . '.' . ContractsLog::ATTR_END_DATE), new Expression('convert(varchar(10), GETDATE(), 111)')])
			->all();

		foreach ($companies as $company) {
			if (null === $company->contracts) {
				continue;
			}

			if (strtotime($company->contracts[0]->End_Date) > time()) {
				continue;
			}

			$company->Color_Index = 3;
			$company->End_Date = $company->contracts[0]->End_Date;
			if ($company->save()) {
				$index++;
			}

			echo $company->Official_Name . ': ' . $company->contracts[0]->Begin_Date . ' - ' . $company->contracts[0]->End_Date . PHP_EOL;
		}

		$message .= 'Красных: ' . $index . ' компаний';

		CronSync::finishTask(CronSync::TASK_ACTUALIZE_CONTRACTS, $message);
	}

	/**
	 * Обновление данных на сайте
	 *
	 * @author Исаков Владислав
	 */
	public function actionSyncSite() {
		/** @var CronSync $lastSync */
		$lastSync = CronSync::find()->where([CronSync::ATTR_TASK => CronSync::TASK_SYNC_COMPANY_WITH_SITE])->one();
		$lastSyncDateTime = new Expression('convert(varchar(10), GETDATE(), 111)');

		if (null !== $lastSync) {
			$lastSyncDateTime = $lastSync->last_date;
		}

		echo $lastSyncDateTime . PHP_EOL;

		CronSync::startTask(CronSync::TASK_SYNC_COMPANY_WITH_SITE);

		/** @var Company[] $companies */
		$companies = Company::find()
			->andWhere(['>', Company::ATTR_UPDATE_STAMP, $lastSyncDateTime])
			->all();

		$count = 0;

		echo 'Компаний для синхронизации: ' . count($companies) . PHP_EOL;

		/** @var Company $company */
		foreach ($companies as $company) {
			/** @var BlockElement $siteCompany */
			$siteCompany = BlockElement::find()
				->andWhere([BlockElement::ATTR_XML_ID => $company->rowguid])
				->one();

			if (null === $siteCompany) {
				echo 'Новая компания: ' . $company->Official_Name . PHP_EOL;
				continue; //Новые пока не заводим т.к. ничего не понятно
			}

			$siteCompany->NAME = $company->Official_Name;
			$siteCompany->save();

			/** @var BlockElementProp $siteCompanyProperty */
			$siteCompanyProperty = BlockElementProp::find()
				->andWhere([BlockElementProp::ATTR_BLOCK_ELEMENT_ID => $siteCompany->ID])
				->one();

			if (null === $siteCompany) {
				continue; //Новые пока не заводим т.к. ничего не понятно
			}
			//Адрес
			$siteCompanyProperty->PROPERTY_17 = (empty($company->town) ? '' : $company->town->Name) . ', ' . (empty($company->street) ? '' : $company->street->name) . ', ' . $company->Home . ', ' . $company->Office;

			//Email
			$siteCompanyProperty->PROPERTY_21 = $company->Email;

			//Сайт
			$siteCompanyProperty->PROPERTY_22 = $company->URL;

			//Распорядок работы
			$siteCompanyProperty->PROPERTY_40 = $company->WorkHours;

			/**
			 * PROPERTY_67 - Вкладка "О компании"
			 *
			 */

			if ($siteCompanyProperty->save()) {
				$count++;
				$company->update_stamp = new Expression('convert(varchar(10), GETDATE(), 111)');
				$company->save();
			}
		}

		CronSync::finishTask(CronSync::TASK_SYNC_COMPANY_WITH_SITE, 'Обновлено компаний: ' . $count);
	}

	/**
	 * конвертация данных в полях связки компаний с торговыми представителями
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 *
	 */
	public function actionConvertAgents() {
		/** @var CronSync $lastSync */
		$lastSync = CronSync::find()->where([CronSync::ATTR_TASK => CronSync::TASK_CONVERT_AGENT_NUM])->one();
		$lastSyncDateTime = new Expression('convert(varchar(10), GETDATE(), 111)');

		if (null !== $lastSync) {
			$lastSyncDateTime = $lastSync->last_date;
		}

		echo $lastSyncDateTime . PHP_EOL;

		CronSync::startTask(CronSync::TASK_CONVERT_AGENT_NUM);

		/** @var Company[] $companies */
		$companies = Company::find()
			->andWhere(new Expression("ISNULL(" . Company::ATTR_UPDATE_STAMP . ", GETDATE()) > '" . $lastSyncDateTime . "'"))
			//->andWhere(['>', Company::ATTR_UPDATE_STAMP, $lastSyncDateTime])
			->all();

		$i = 1;
		$count = count($companies);
		foreach ($companies as $company) {
			CronController::showStatus($i, $count);

			$i++;

			$needSave = false;
			if ($company->CAgent_Num !== StringHelper::GUID_NULL) {
				/** @var AgentsWork $agentsWork */
				$agentsWork = AgentsWork::find()
					->andWhere([AgentsWork::ATTR_COMPANY_GUID => $company->rowguid])
					->orderBy([AgentsWork::ATTR_DATE_FINISH => SORT_DESC])
					->one();

				if (null !== $agentsWork) {
					$needSave = true;
					$company->CAgent_Num = $agentsWork->rowguid;
				}
			}

			if ($company->CrAgent_Num !== StringHelper::GUID_NULL) {
				/** @var ContractsLog $contractsLog */
				$contractsLog = ContractsLog::find()
					->andWhere([ContractsLog::ATTR_COMPANY_GUID => $company->rowguid])
					->orderBy([ContractsLog::ATTR_END_DATE => SORT_DESC])
					->one();

				$needSave = true;

				if (null !== $contractsLog) {
					$company->CrAgent_Num = $contractsLog->rowguid;
				}
				else {
					$company->CrAgent_Num = StringHelper::GUID_NULL;
				}
			}

			if ($needSave) {
				$company->save();
			}
		}

		CronSync::finishTask(CronSync::TASK_CONVERT_AGENT_NUM, 'Обновлено компаний: ' . $count);
	}


	/**
	 * show a status bar in the console
	 *
	 * <code>
	 * for($x=1;$x<=100;$x++){
	 *
	 *     show_status($x, 100);
	 *
	 *     usleep(100000);
	 *
	 * }
	 * </code>
	 *
	 * @param int $done  how many items are completed
	 * @param int $total how many items are to be done total
	 * @param int $size  optional size of the status bar
	 *
	 * @return  void
	 *
	 */
	public static function showStatus($done, $total, $size = 30) {

		static $start_time;

		// if we go over our bound, just ignore it
		if ($done > $total) {
			return;
		}

		if (empty($start_time)) {
			$start_time = time();
		}

		$now = time();

		$perc = (double)($done / $total);

		$bar = floor($perc * $size);

		$status_bar = "\r[";
		$status_bar .= str_repeat("=", $bar);
		if ($bar < $size) {
			$status_bar .= ">";
			$status_bar .= str_repeat(" ", $size - $bar);
		}
		else {
			$status_bar .= "=";
		}

		$disp = number_format($perc * 100, 0);

		$status_bar .= "] $disp%  $done/$total";

		$rate = ($now - $start_time) / $done;
		$left = $total - $done;
		$eta = round($rate * $left, 2);

		$elapsed = $now - $start_time;

		$status_bar .= " remaining: " . number_format($eta) . " sec.  elapsed: " . number_format($elapsed) . " sec.";

		echo "$status_bar  ";

		flush();

		// when done, send a newline
		if ($done == $total) {
			echo "\n";
		}
	}

	/**
	 * Проверка доступности серверов
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionCheckServers() {
		$serverStatus = [];
		foreach (LocalServers::SERVERS as $ip => $name) {
			$command = 'ping -c 5 ' . $ip;
			exec($command, $output, $status);
			$serverStatus[$ip] = $output[1];
			$output = null;
		}

		$message = [];
		foreach ($serverStatus as $ip => $status) {
			$server = ServerStatus::findOne([ServerStatus::ATTR_SERVER_IP => $ip]);

			if (null === $server) {
				$server = new ServerStatus();
				$server->server_ip = $ip;
			}

			if (empty($status) || false !== strpos($status, 'Destination Host Unreachable')) {
				$message[] = date(DateHelper::DATE_TIME_FORMAT_RU, time() + 36000) . ': Недоступен ' . LocalServers::SERVERS[$ip] . ' | ' . $status;
				$server->status = LocalServers::STATUS_OFFLINE;
			}
			else {
				$server->status = LocalServers::STATUS_ONLINE;
			}

			$server->log_time = new Expression('getdate()');
			$server->save();
		}

		if (count($message) > 0) {
			$message = implode('<br>', $message);
			Yii::$app->mailer->compose()
				->setFrom('robot@mail.516.ru')
				->setTo(['visakov@mail.516.ru', 'mokalex@mail.516.ru', 'mokalex57@mail.ru'])
				->setSubject('Отсутствует ответ сервера')
				->setHtmlBody($message)
				->send();
		}
	}
}