<?php
use \yii\db\Migration;

class m190406_110200_add_column_to_user_table extends Migration {

	public function up() {
		$this->addColumn('logins', 'logical_num', $this->integer()->defaultValue(null));
	}

	public function down() {
		$this->dropColumn('logins', 'logical_num');
	}
}
