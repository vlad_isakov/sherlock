<?php

namespace app\migrations;

use common\components\Migration;
use yii\db\Schema;

class m190821_054541_add_column_to_banner extends Migration {
	private $_tableName = 'banner';

	public function safeUp() {
		$this->addColumn($this->_tableName, 'priority', Schema::TYPE_INTEGER . ' NOT NULL');
		$this->createIndex(null, $this->_tableName, 'priority');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropColumn($this->_tableName, 'priority');
	}
}
