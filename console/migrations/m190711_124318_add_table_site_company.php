<?php

namespace app\migrations;

use common\components\Migration;
use yii\db\mysql\Schema;

class m190711_124318_add_table_site_company extends Migration {
	private $_tableName = 'site_company';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'     => $this->primaryKey(),
				'old_id' => Schema::TYPE_INTEGER . ' NOT NULL',
				'guid'   => Schema::TYPE_STRING . ' NOT NULL',
			]
		);

		$this->createIndex(null, $this->_tableName, ['old_id']);
		$this->createIndex(null, $this->_tableName, ['guid']);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
