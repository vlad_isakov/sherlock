<?php

namespace app\migrations;

use common\components\Migration;
use yii\db\Schema;

class m190821_054540_add_column_to_logins extends Migration {
	private $_tableName = 'Logins';

	public function safeUp() {
		$this->addColumn($this->_tableName, 'company_guid', Schema::TYPE_STRING . ' DEFAULT NULL');
		$this->createIndex(null, $this->_tableName, 'company_guid');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropColumn($this->_tableName, 'company_guid');
	}
}
