<?php

namespace app\migrations;

use common\components\Migration;
use yii\db\Schema;

/**
 * Class m190402_054540_add_table_towns
 */
class m190712_054540_add_column_to_towns extends Migration {
	private $_tableName = 'Towns';

	public function safeUp() {
		$this->addColumn($this->_tableName, 'id_geobase', Schema::TYPE_INTEGER . ' DEFAULT NULL');
		$this->createIndex(null, $this->_tableName, 'id_geobase');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropColumn($this->_tableName . 'id_geobase');
	}
}
