const mix = require('laravel-mix');



mix.js('backend/assets/src/js/calls.js', 'backend/web/js/516-backend.js');
mix.js('backend/assets/src/js/cronTaskPlugin.js', 'backend/web/js/516-backend.js');
mix.js('backend/assets/src/js/search.js', 'backend/web/js/516-backend.js');
mix.js('backend/assets/src/js/widgetPlugin.js', 'backend/web/js/516-backend.js');
mix.js('backend/assets/src/js/colResizable-1.6.min.js', 'backend/web/js/516-backend.js')
	.version();

mix.sass('backend/assets/src/sass/common.scss', 'backend/web/css')
	.options({
		imgLoaderOptions: {enabled: false},
		postCss: [
			require('postcss-css-variables')()
		]
	});

mix.styles([
	'backend/assets/src/css/site.css',
], 'backend/web/css/site-backend.css')
	.version();

mix.setPublicPath('backend/web/');