Команды

Модель по таблице ДСП:
php yii util/create-model -tb=COUNTRIES

Модель по таблице сайта:
php yii util/create-model -db=db tb=TOWN -fieldUpper=0
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ruleName'], 'in',
                'range' => array_keys(Yii::$app->authManager->getRules()),
                'message' => Yii::t('rbac', 'Rule not exists')],
            [['name'], 'required'],
            [['name'], 'uniqueValidator', 'when' => function() {
            return $this->isNewRecord || ($this->item->name != $this->name);
        }],
            [['description', 'data', 'ruleName'], 'default'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    Saira, sans-serif
    
    копировать в буфер:
    <a onclick="window.clipboardData.setData('text', document.getElementById('Test').innerText);">Copy</a>
    https://github.com/ryanpcmcquen/cheval
    
    Минификация
    "rmrevin/yii2-minify-view": "^1.15",