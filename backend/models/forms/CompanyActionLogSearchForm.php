<?php

namespace backend\models\forms;

use common\models\CompanyActionLog;
use yii\base\Model;
use yii\validators\SafeValidator;

class CompanyActionLogSearchForm extends Model {

	/** @var string */
	public $companyGuid;
	const ATTR_COMPANY_GUID = 'companyGuid';

	/** @var int */
	public $actionType;
	const ATTR_ACTION_TYPE = 'actionType';

	/** @var int */
	public $userId;
	const ATTR_USER_ID = 'userId';

	/** @var CompanyActionLog[] */
	public $result = [];

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function attributeLabels() {
		return [
			static::ATTR_COMPANY_GUID => 'Компания',
			static::ATTR_ACTION_TYPE  => 'Действие',
			static::ATTR_USER_ID      => 'Пользователь',
		];
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function rules() {
		return [
			[static::ATTR_COMPANY_GUID, SafeValidator::class],
			[static::ATTR_ACTION_TYPE, SafeValidator::class],
			[static::ATTR_USER_ID, SafeValidator::class],
		];
	}

	/**
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function search() {
		$log = CompanyActionLog::find();

		if (!empty($this->companyGuid)) {
			$log->andWhere([CompanyActionLog::ATTR_COMPANY_GUID => $this->companyGuid]);
		}

		if (!empty($this->actionType)) {
			$log->andWhere([CompanyActionLog::ATTR_ACTION_TYPE => $this->actionType]);
		}

		$this->result = $log->orderBy([CompanyActionLog::ATTR_INSERT_STAMP => SORT_DESC])
			->limit(200)
			->all();
	}
}