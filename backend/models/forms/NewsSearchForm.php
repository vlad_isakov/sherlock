<?php

namespace backend\models\forms;


use common\modules\news\models\SiteNews;
use yii\base\Model;
use yii\validators\SafeValidator;

class NewsSearchForm extends Model {

	public $title;
	const ATTR_TITLE = 'title';

	public $isPublished;
	const ATTR_IS_PUBLISHED = 'isPublished';

	public $companyGuid;
	const ATTR_COMPANY_GUID = 'companyGuid';

	/** @var SiteNews[] */
	public $result;

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function attributeLabels() {
		return [
			static::ATTR_TITLE        => 'Название',
			static::ATTR_IS_PUBLISHED => 'Опубликована',
			static::ATTR_COMPANY_GUID => 'Компания',
		];
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function rules() {
		return [
			[static::ATTR_TITLE, SafeValidator::class],
			[static::ATTR_IS_PUBLISHED, SafeValidator::class],
			[static::ATTR_COMPANY_GUID, SafeValidator::class],
		];
	}

	/**
	 * @return array|\yii\db\ActiveRecord[]
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function search() {
		$query = SiteNews::find();

		if (!empty($this->title)) {
			$query->andWhere(['LIKE', SiteNews::ATTR_TITLE, $this->title]);
		}

		if (!empty($this->isPublished)) {
			$query->andWhere([SiteNews::ATTR_IS_PUBLISHED => $this->isPublished]);
		}

		if (!empty($this->companyGuid)) {
			$query->andWhere([SiteNews::ATTR_COMPANY_GUID => $this->companyGuid]);
		}

		$query->orderBy([SiteNews::ATTR_DATE => SORT_DESC]);

		$this->result = $query->limit(30)->all();

	}
}