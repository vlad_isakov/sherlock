<?php

namespace backend\models\forms;

use common\base\helpers\StringHelper;
use common\base\validators\IntegerValidator;
use common\models\Addresses;
use common\models\Company;
use common\models\CompanyPhone;
use common\models\ObjectFile;
use common\models\ServiceName;
use common\models\Street;
use yii\base\Model;
use yii\db\Expression;
use yii\validators\BooleanValidator;
use yii\validators\StringValidator;

/**
 * Форма поиска организаций
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class CompanySearchForm extends Model {

	/** @var string */
	public $companyName;
	const ATTR_COMPANY_NAME = 'companyName';

	/** @var string */
	public $phone;
	const ATTR_PHONE = 'phone';

	/** @var string */
	public $productService;
	const ATTR_PRODUCT_SERVICE = 'productService';

	/** @var string */
	public $street;
	const ATTR_STREET = 'street';

	/** @var string */
	public $houseNum;
	const ATTR_HOUSE_NUM = 'houseNum';

	/** @var string */
	public $officeNum;
	const ATTR_OFFICE_NUM = "officeNum";

	/** @var string */
	public $cityDistrict;
	const ATTR_CITY_DISTRICT = 'cityDistrict';

	/** @var string */
	public $cityGuid;
	const ATTR_CITY_GUID = 'cityGuid';

	/** @var string */
	public $regionName;
	const ATTR_REGION_NAME = 'regionName';

	/** @var string */
	public $regionDistrictName;
	const ATTR_REGION_DISTRICT_NAME = 'regionDistrictName';

	/** @var string */
	public $districtAlias;
	const ATTR_DISTRICT_ALIAS = 'districtAlias';

	/** @var string */
	public $additionalInfo;
	const ATTR_ADDITIONAL_INFO = 'additionalInfo';

	/** @var string */
	public $saleType;
	const ATTR_SALE_TYPE = 'saleType';

	/** @var int */
	public $serviceNamePosition = self::POSITION_ALL;
	const ATTR_SERVICE_NAME_POSITION = 'serviceNamePosition';

	/** @var int */
	public $companyNamePosition = self::POSITION_ALL;
	const ATTR_COMPANY_NAME_POSITION = 'companyNamePosition';

	/** @var null|bool */
	public $isNotHaveLogo;
	const ATTR_IS_NOT_HAVE_LOGO = 'isNotHaveLogo';

	/** @var null|bool */
	public $isNotHaveImages;
	const ATTR_IS_NOT_HAVE_IMAGES = 'isNotHaveImages';

	/** @var null|bool */
	public $isNotHaveAddress;
	const ATTR_IS_NOT_HAVE_ADDRESS = 'isNotHaveAddress';

	/** @var null|bool */
	public $isNotHavePhone;
	const ATTR_IS_NOT_HAVE_PHONE = 'isNotHavePhone';

	/** @var null|bool */
	public $isNotHaveEmail;
	const ATTR_IS_NOT_HAVE_EMAIL = 'isNotHaveEmail';

	const POSITION_START = 0;
	const POSITION_END = 1;
	const POSITION_ALL = 2;

	const POSITION_NAMES = [
		self::POSITION_START => 'В начале',
		self::POSITION_END   => 'В конце',
		self::POSITION_ALL   => 'Везде',
	];

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
	 */
	public function rules() {
		return [
			[static::ATTR_COMPANY_NAME, StringValidator::class],
			[static::ATTR_CITY_GUID, StringValidator::class],
			[static::ATTR_PHONE, StringValidator::class],
			[static::ATTR_CITY_DISTRICT, StringValidator::class],
			[static::ATTR_PRODUCT_SERVICE, StringValidator::class],
			[static::ATTR_DISTRICT_ALIAS, StringValidator::class],
			[static::ATTR_ADDITIONAL_INFO, StringValidator::class],
			[static::ATTR_STREET, StringValidator::class],
			[static::ATTR_SALE_TYPE, StringValidator::class],
			[static::ATTR_HOUSE_NUM, StringValidator::class],
			[static::ATTR_SERVICE_NAME_POSITION, IntegerValidator::class],
			[static::ATTR_COMPANY_NAME_POSITION, IntegerValidator::class],

			[static::ATTR_IS_NOT_HAVE_ADDRESS, BooleanValidator::class],
			[static::ATTR_IS_NOT_HAVE_PHONE, BooleanValidator::class],
			[static::ATTR_IS_NOT_HAVE_LOGO, BooleanValidator::class],
			[static::ATTR_IS_NOT_HAVE_IMAGES, BooleanValidator::class],
			[static::ATTR_IS_NOT_HAVE_EMAIL, BooleanValidator::class],
		];
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function attributeLabels() {
		return [
			static::ATTR_COMPANY_NAME    => 'Компания',
			static::ATTR_CITY_GUID       => 'Город',
			static::ATTR_PHONE           => 'Телефон',
			static::ATTR_PRODUCT_SERVICE => 'Товар/Услуга',
			static::ATTR_CITY_DISTRICT   => 'Район города',
			static::ATTR_DISTRICT_ALIAS  => 'Местоположение',
			static::ATTR_ADDITIONAL_INFO => 'Доп.инфо',
			static::ATTR_STREET          => 'Улица',
			static::ATTR_SALE_TYPE       => 'Вид продаж',
			static::ATTR_HOUSE_NUM       => 'Дом',

			static::ATTR_IS_NOT_HAVE_ADDRESS => 'Без адреса',
			static::ATTR_IS_NOT_HAVE_PHONE   => 'Без телефона',
			static::ATTR_IS_NOT_HAVE_LOGO    => 'Без логотипа',
			static::ATTR_IS_NOT_HAVE_IMAGES  => 'Без фотографий',
			static::ATTR_IS_NOT_HAVE_EMAIL   => 'Без email',
		];
	}

	/**
	 * Поиск компаний
	 *
	 * @param int  $limit
	 * @param bool $isNeedBlack
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 *
	 * @author Исаков Владислав
	 */
	public function search($limit = 200, $isNeedBlack = true) {
		$query = Company::find();
		$query->joinWith(Company::REL_ADDRESSES);
		$query->joinWith(Company::REL_PHONES);


		if (null !== $this->companyName) {
			$arrayNames = explode(' ', $this->companyName);

			$orWhereCompany = ['AND'];
			foreach ($arrayNames as $companyName) {
				switch ($this->companyNamePosition) {
					case static::POSITION_END:
						$orWhereCompany[] = ['LIKE', Company::ATTR_OFFICIAL_NAME, '%' . $companyName, false];
						break;
					case static::POSITION_START:
						$orWhereCompany[] = ['LIKE', Company::ATTR_OFFICIAL_NAME, $companyName . '%', false];
						break;
					default:
						$orWhereCompany[] = ['LIKE', Company::ATTR_OFFICIAL_NAME, $companyName];
						break;
				}
			}

			$orWhereAddress = ['AND'];
			foreach ($arrayNames as $companyName) {
				switch ($this->companyNamePosition) {
					case static::POSITION_END:
						$orWhereAddress[] = ['LIKE', Addresses::tableName() . '.' . Addresses::ATTR_NAME, '%' . $companyName, false];
						break;
					case static::POSITION_START:
						$orWhereAddress[] = ['LIKE', Addresses::tableName() . '.' . Addresses::ATTR_NAME, $companyName . '%', false];
						break;
					default:
						$orWhereAddress[] = ['LIKE', Addresses::tableName() . '.' . Addresses::ATTR_NAME, $companyName];
						break;
				}
			}

			$query->where(['OR', $orWhereCompany, $orWhereAddress]);

			/** @todo Тут делать запрос в SMSList */
		}

		if (false === $isNeedBlack) {
			$query->andWhere(['!=', Company::ATTR_COLOR_INDEX, 6]);
		}


		if (null !== $this->cityGuid && !empty($this->cityGuid)) {
			$query->andWhere([
					'OR',
					[Company::tableName() . '.' . Company::ATTR_TOWN_NUM => $this->cityGuid],
					[Addresses::tableName() . '.' . Addresses::ATTR_CITY_GUID => $this->cityGuid]
				]
			);
		}

		if (!empty($this->phone)) {
			$this->phone = str_replace('-', '', $this->phone);
			$query->andWhere(['LIKE', new Expression("REPLACE ( " . CompanyPhone::tableName() . "." . CompanyPhone::ATTR_PHONE . " ,'-', '')  "), $this->phone]);
		}

		if (!empty($this->cityDistrict)) {
			$query->joinWith(Company::REL_DISTRICT);
			$query->andWhere(['LIKE', Company::tableName() . '.' . Company::ATTR_DISTRICT_NUM, $this->cityDistrict]);
		}

		if (!empty($this->productService)) {
			$query->joinWith(Company::REL_SERVICE_NAME_ONE);
			switch ($this->serviceNamePosition) {
				case static::POSITION_END:
					$query->andWhere(['LIKE', ServiceName::tableName() . '.' . ServiceName::ATTR_NAME, '%' . $this->productService, false]);
					break;
				case static::POSITION_START:
					$query->andWhere(['LIKE', ServiceName::tableName() . '.' . ServiceName::ATTR_NAME, $this->productService . '%', false]);
					break;
				default:
					$query->andWhere(['LIKE', ServiceName::tableName() . '.' . ServiceName::ATTR_NAME, $this->productService]);
					break;
			}
		}

		if (!empty($this->street)) {
			$query->joinWith(Company::REL_STREET);
			$query->joinWith([
				Company::REL_ADDRESSES . '.' . Addresses::REL_ADDR_STREET => function ($q) {
					$q->from('streets addrStreet');
				},
			]);

			$query->andWhere([
					'OR',
					['LIKE', Street::tableName() . '.' . Street::ATTR_NAME, $this->street],
					['LIKE', 'addrStreet.' . Street::ATTR_NAME, $this->street],
				]
			);
		}

		if (!empty($this->houseNum)) {
			$query->andWhere([
					'OR',
					['LIKE', new Expression("REPLACE(" . Company::tableName() . "." . Company::ATTR_HOME . ",' ', '')"), str_replace(' ', '', $this->houseNum)],
					['LIKE', new Expression("REPLACE(" . Addresses::tableName() . "." . Addresses::ATTR_HOME . ",' ', '')"), str_replace(' ', '', $this->houseNum)]
				]
			);
		}

		//Без лого
		if (true == $this->isNotHaveLogo) {
			$subQuery = ObjectFile::find()->select(ObjectFile::ATTR_OBJECT_GUID)
				->andWhere([ObjectFile::ATTR_OBJECT => Company::class]);

			$query->andWhere(['NOT IN', Company::ATTR_GUID, $subQuery]);
		}

		//Без доп изображений
		if (true == $this->isNotHaveImages) {
			$subQuery = ObjectFile::find()->select(ObjectFile::ATTR_OBJECT_GUID)
				->andWhere([ObjectFile::ATTR_OBJECT => Company::OBJECT_COMPANY_IMAGES]);

			$query->andWhere(['NOT IN', Company::ATTR_GUID, $subQuery]);
		}

		//Без телефонов
		if (true == $this->isNotHavePhone) {
			$subQuery = CompanyPhone::find()->select(CompanyPhone::ATTR_FIRM_GUID);

			$query->andWhere(['NOT IN', Company::ATTR_GUID, $subQuery]);
		}

		//Без адресов
		if (true == $this->isNotHaveAddress) {
			$query->andWhere([Company::ATTR_STREET_NUM => StringHelper::GUID_NULL]);
		}

		//Без email
		if (true == $this->isNotHaveEmail) {
			$query->andWhere([Company::ATTR_EMAIL => '']);
		}

		$query->orderBy([Company::tableName() . '.' . Company::ATTR_COLOR_INDEX => SORT_ASC]);
		$query->limit($limit);

		$result = $query->all();

		return $result;
	}
}