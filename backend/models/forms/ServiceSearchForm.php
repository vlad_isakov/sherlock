<?php

namespace backend\models\forms;

use common\models\Addresses;
use common\models\Company;
use common\models\PriceDistrictAlias;
use common\models\PriorityPrice;
use common\models\ServiceName;
use common\models\Street;

/**
 * Форма поиска услуги
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class ServiceSearchForm extends CompanySearchForm {

	/**
	 * Поиск компаний
	 *
	 * @param int  $limit
	 * @param bool $groupByService
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 *
	 * @author Исаков Владислав
	 */
	public function search($limit = 500, $groupByService = true) {
		$query = PriorityPrice::find();
		$query->joinWith(PriorityPrice::REL_COMPANY);
		$query->joinWith(PriorityPrice::REL_COMPANY . '.' . Company::REL_ADDRESSES);
		$query->joinWith(PriorityPrice::REL_REFERENCE_ONE);

		if (!empty($this->productService)) {
			$arrayNames = explode(' ', $this->productService);
			foreach ($arrayNames as $str) {
				switch ($this->serviceNamePosition) {
					case static::POSITION_END:
						$query->andWhere(['like', ServiceName::tableName() . '.' . ServiceName::ATTR_NAME, '%' . $str, false]);
						break;
					case static::POSITION_START:
						$query->andWhere(['like', ServiceName::tableName() . '.' . ServiceName::ATTR_NAME, $str . '%', false]);
						break;
					default:
						$query->andWhere(['like', ServiceName::tableName() . '.' . ServiceName::ATTR_NAME, $str]);
						break;
				}
			}
		}

		if (!empty($this->additionalInfo)) {
			$arrayNames = explode(' ', $this->additionalInfo);
			foreach ($arrayNames as $str) {
				$query->andWhere(['like', PriorityPrice::ATTR_TEXT, $str]);
			}
		}

		if (null !== $this->companyName) {
			$arrayNames = explode(' ', $this->companyName);
			$orWhereCompany = ['AND'];
			foreach ($arrayNames as $companyName) {
				$orWhereCompany[] = ['LIKE', Company::ATTR_OFFICIAL_NAME, $companyName];
			}

			$orWhereAddress = ['AND'];
			foreach ($arrayNames as $companyName) {
				$orWhereAddress[] = ['LIKE', Addresses::tableName() . '.' . Addresses::ATTR_NAME, $companyName];
			}

			$query->andWhere(['OR', $orWhereCompany, $orWhereAddress]);
		}

		if (null !== $this->cityGuid && !empty($this->cityGuid)) {
			$query->andWhere([
					'OR',
					[Company::tableName() . '.' . Company::ATTR_TOWN_NUM => $this->cityGuid],
					[Addresses::tableName() . '.' . Addresses::ATTR_CITY_GUID => $this->cityGuid]
				]
			);
		}

		if (!empty($this->cityDistrict)) {
			$query->joinWith(PriorityPrice::REL_COMPANY . '.' . Company::REL_DISTRICT);
			$query->andWhere(['LIKE', Company::tableName() . '.' . Company::ATTR_DISTRICT_NUM, $this->cityDistrict]);
		}

		if (!empty($this->districtAlias)) {
			$query->joinWith(PriorityPrice::REL_DISTRICT_ALIAS);
			$query->andWhere(['LIKE', PriceDistrictAlias::tableName() . '.' . PriceDistrictAlias::ATTR_NAME_LIST, $this->districtAlias]);
		}

		if (!empty($this->street)) {
			$query->joinWith(PriorityPrice::REL_COMPANY . '.' . Company::REL_STREET);
			$query->andWhere(['LIKE', Street::tableName() . '.' . Street::ATTR_NAME, $this->street]);
		}

		if (!empty($this->saleType)) {
			$arrayNames = explode(' ', $this->saleType);
			foreach ($arrayNames as $str) {
				$query->andWhere(['like', PriorityPrice::ATTR_STRING_ONE, $str]);
			}
		}

		if (!empty($this->houseNum)) {
			$query->andWhere(['LIKE', Company::tableName() . '.' . Company::ATTR_HOME, $this->houseNum]);
		}

		$query->orderBy([
			PriorityPrice::ATTR_TOWN_PRIORITY => SORT_ASC,
			PriorityPrice::ATTR_PRIORITY      => SORT_ASC,
			Company::ATTR_COLOR_INDEX         => SORT_ASC,
		]);

		$query->limit($limit);
		/** @var PriorityPrice[] $queryResult */
		$queryResult = $query->all();
		$result = [];
		if ($groupByService) {
			foreach ($queryResult as $row) {
				$result[$row->referenceOne->Name][] = $row;
			}
		}
		else {
			foreach ($queryResult as $row) {
				$result[$row->referenceOne->rowguid] = $row;
			}
		}

		return $result;
	}
}