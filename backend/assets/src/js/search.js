(function (b) {
	b.fn.searchPlugin = function (l) {
		if (k[l]) return k[l].apply(this, Array.prototype.slice.call(arguments, 1));
		if ("object" !== typeof l && l) b.error('\u041c\u0435\u0442\u043e\u0434 "' + l + '" \u043d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d.'); else return k.init.apply(this, arguments)
	};
	var k = {
		init: function (k) {

		},
		openCompanyInfo: function (guid, index, serviceGuid) {
			var info = $('#' + guid + '-' + index);
			var sendSmsButton = $('.send-sms-btn');
			var townGuid = $('#city-guid').data('value');

			var phone = $('#phone-to-sms').data('value');

			if (phone.length == 0) {
				sendSmsButton.hide();
			}

			$('.company-list-name').removeClass('selected');
			if (info.css('display') !== 'none') {
				$(this).removeClass('selected');
				info.hide();
				return;
			}

			$('.company-info').hide();
			info.fadeIn();
			$(this).addClass('selected');

			b.ajax({
				url: $("#open-company-url").data("value"),
				method: "GET",
				async: true,
				data: {
					'phone': phone,
					'companyGuid': guid,
					'serviceGuid': serviceGuid,
					'townGuid': townGuid
				},
				beforeSend: function () {
					$('.open-news-link').hide();
				}
			}).done(function (data) {
				$('.news-text').html(data.newsText);
				if (data.newsText != false) {
					if (data.ownerType == 4) {
						$('.open-news-link').addClass('orange-btn');
					}
					else {
						$('.open-news-link').addClass('green-btn');
					}

					$('.open-news-link').show();
				}
			});

			b.ajax({
				url: $("#get-company-info").data("value"),
				method: "GET",
				async: false,
				data: {
					'guid': guid,
				},
				beforeSend: function () {
					$('.loading-widget').show();
				}
			}).done(function (data) {
				$('.addresses-' + guid).html('');
				$('.addresses-' + guid).html(data.addresses);

				$('.contracts-' + guid).html('');
				$('.contracts-' + guid).html(data.contracts);
				if (phone.length == 0) {
					var sendFilialSmsButton = $('.filial-sms');
					sendFilialSmsButton.hide();
				}

				$(".loading-widget").hide();
			});
		}
	}
})(jQuery);