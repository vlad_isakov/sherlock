(function ($) {
	$.fn.cronTaskPlugin = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Метод с именем ' + method + ' не существует для jQuery.tooltip');
		}
	};

	var methods = {
		init: function (options) {
			setTimeout(function run() {
				$(this).cronTaskPlugin('refreshTask');
				setTimeout(run, 1000);
			}, 1000);
		},
		refreshTask: function () {
			var url = $('.cron-status-container').data('url');
			$.ajax({
				url: url,
				method: "GET",
				async: true,
				data: {},
				beforeSend: function () {

				}
			}).done(function (data) {
				$('.cron-status-container').html('');
				$('.cron-status-container').html(data);
			});
		}
	};

})(jQuery);

//https://habr.com/ru/post/158235/