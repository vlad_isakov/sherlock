(function (b) {
	b.fn.callsPlugin = function (l) {
		if (k[l]) return k[l].apply(this, Array.prototype.slice.call(arguments, 1));
		if ("object" !== typeof l && l) b.error('\u041c\u0435\u0442\u043e\u0434 "' + l + '" \u043d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d.'); else return k.init.apply(this, arguments)
	};
	var k = {
		init: function (k) { //инициализация плагина
			setTimeout(function run() {
				b(this).callsPlugin('checkRingingCall');
				setTimeout(run, 1000);
			}, 1000);

			setTimeout(function run() {
				b(this).callsPlugin('getSmsCount');
				setTimeout(run, 5000);
			}, 5000);

			var cityGuid = $('#city-guid').data('value');
			var phoneNumber = $('#phone-to-sms').data('value');

			$("#servicesearchform-cityguid").val(cityGuid).trigger('change');
			$("#companysearchform-cityguid").val(cityGuid).trigger('change');

			var sendSmsButton = $('.send-sms-btn');

			sendSmsButton.show();
		},
		checkRingingCall: function () { //проверка звонящего звонка
			b.ajax({
				url: $("#ringing-call-url").data("value"),
				method: "GET",
				async: true,
				data: {},
				beforeSend: function () {
				}
			}).done(function (data) {
				if (data.hasRinging) {

					$('.ringing-call-widget').show();
					$('.ringing-call-widget .city').html(data.call.cityName);
					$('.ringing-call-widget .phone-num').html(data.call.aNumber);
					$('.ringing-call-widget .client-name').html(data.call.clientName);

					//Устанавливаем номер телефона абонента для отправки смс
					$('#phone-to-sms').data('value', data.call.aNumber);
					//Чистим все поля формы
					$("input[type=text]").val("");
					//Устанавливаем город из звонка абонента
					$("#servicesearchform-cityguid").val(data.call.cityGuid).trigger('change');
					$("#companysearchform-cityguid").val(data.call.cityGuid).trigger('change');
					var sendSmsButton = $('.send-sms-btn');
					sendSmsButton.show();

				} else {
					$('.ringing-call-widget').hide();
				}
			});
		},
		sendSms: function (y) { //отправка смс по компании
			var companyGuid = $(this).data('company-guid');
			var phone = $('#phone-to-sms').data('value');
			b.ajax({
				url: $("#send-sms-url").data("value"),
				method: "GET",
				async: false,
				data: {
					'phone': phone,
					'companyGuid': companyGuid
				},
				beforeSend: function () {
					$('.loading-widget').show();
				}
			}).done(function () {
				$(y).removeClass('btn-primary');
				$(y).addClass('btn-success');
				$(y).addClass('disabled');
				$(y).html('SMS Отправлено');

				$(".loading-widget").hide();
			});
		},
		sendSmsFilial: function (text) { //отправка смс по филиалу компании
			var companyGuid = $(this).data('company-guid');
			var phone = $('#phone-to-sms').data('value');
			$(this).hide();
			b.ajax({
				url: $("#send-sms-filial-url").data("value"),
				async: false,
				data: {
					'phone': phone,
					'text': text,
					'companyGuid': companyGuid
				},
				beforeSend: function () {
					$('.loading-widget').show();
				}
			}).done(function () {
				$(".loading-widget").hide();
			});
		},
		getSmsCount: function () { //проверка кол-ва отправленных смс
			var SMS_DELIVERED_CONTAINER = $('.sms-delivered');
			var SMS_NOT_DELIVERED_CONTAINER = $('.sms-not-delivered');
			b.ajax({
				url: $("#get-sms-count-url").data("value"),
				async: true,
				beforeSend: function () {
				}
			}).done(function (data) {
				SMS_DELIVERED_CONTAINER.html(data.delivered);
				SMS_NOT_DELIVERED_CONTAINER.html(data.not_delivered);
			});
		}
	}
})(jQuery);