<?php

use backend\controllers\AdminNewsController;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use backend\controllers\SiteController;
use common\modules\rbac\rules\Permissions;
use common\models\Sms;
Use common\modules\pages\controllers\PageController;
use common\modules\seo\controllers\SeoController;
use common\modules\banner\controllers\BannerController;

NavBar::begin([
	'brandLabel' => '<img src="/images/logo.png" class="img-responsive" width="75px" alt="516"/>',
	'brandUrl'   => Yii::$app->homeUrl,
	'options'    => [
		'class' => 'navbar-default navbar-fixed-top',
	],
]);
$menuItems = [
	[
		'label'   => 'Пользователи',
		'visible' => Yii::$app->user->can(Permissions::P_ADMIN, [], false),
		'items'   => [
			[
				'label' => 'Роли',
				'url'   => ['/rbac/role'],
			],
			[
				'label' => 'Правила',
				'url'   => ['/rbac/rule'],
			],
			[
				'label' => 'Разрешения',
				'url'   => ['/rbac/permission'],
			],
			[
				'label' => 'Назначение ролей',
				'url'   => ['/rbac/assignment'],
			],
		]
	],
	[
		'label'   => 'Сайт',
		'visible' => Yii::$app->user->can(Permissions::ROLE_ADMIN, [], false),
		'items'   => [
			['label' => 'Страницы', 'url' => PageController::getActionUrl(PageController::ACTION_INDEX)],
			['label' => 'Новости', 'url' => AdminNewsController::getActionUrl(AdminNewsController::ACTION_INDEX)],
			['label' => 'Настройки SEO', 'url' => SeoController::getActionUrl(SeoController::ACTION_INDEX)],
			['label' => 'Баннеры', 'url' => BannerController::getActionUrl(BannerController::ACTION_INDEX)],
			[
				'label' => 'Ошибки',
				'url'   => ['/log/index'],
			],
			[
				'label' => 'События по компаниям',
				'url'   => ['/company-action-log/index'],
			],
		]
	],
	[
		'label'   => 'Статусы системы',
		'url'     => ['/cron-task/index'],
		'visible' => Yii::$app->user->can(Permissions::P_ADMIN)
	],
	[
		'label'   => 'Информация',
		'url'     => ['/site/index'],
		'visible' => Yii::$app->user->can(Permissions::P_ADMIN)
	],
	[
		'label'   => 'Смс ' . Html::tag('label', Sms::getCountBuStatus(Yii::$app->user->id, Sms::STATUS_DELIVERED), ['class' => 'label label-success sms-delivered']) . ' ' . Html::tag('label', Sms::getCountBuStatus(Yii::$app->user->id, Sms::STATUS_NOT_DELIVERED), ['class' => 'label label-danger sms-not-delivered']),
		'url'     => SiteController::getActionUrl(SiteController::ACTION_SMS_LOG),
		'visible' => !Yii::$app->user->isGuest//can(Permissions::P_ADMIN) || Yii::$app->user->can(Permissions::P_SEND_SMS)
	],
	[
		'label'   => 'Компании',
		'url'     => SiteController::getActionUrl(SiteController::ACTION_SEARCH),
		'visible' => !Yii::$app->user->isGuest//Yii::$app->user->can(Permissions::P_ADMIN) || Yii::$app->user->can(Permissions::P_SEARCH_COMPANY)
	],
	[
		'label'   => 'Услуги',
		'url'     => SiteController::getActionUrl(SiteController::ACTION_SEARCH_SERVICE),
		'visible' => !Yii::$app->user->isGuest//Yii::$app->user->can(Permissions::P_ADMIN) || Yii::$app->user->can(Permissions::P_SEARCH_COMPANY)
	],
	[
		'label'   => 'Настройки',
		'url'     => SiteController::getActionUrl(SiteController::ACTION_SETTINGS),
		'visible' => !Yii::$app->user->isGuest
	],
];
if (Yii::$app->user->isGuest) {
	$menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
}
else {
	$menuItems[] = '<li>'
		. Html::beginForm(['/site/logout'], 'post')
		. Html::submitButton(
			'Выход (' . Yii::$app->user->identity->username . ')',
			['class' => 'btn btn-link logout']
		)
		. Html::endForm()
		. '</li>';
}
echo Nav::widget([
	'options'      => ['class' => 'navbar-nav navbar-right'],
	'encodeLabels' => false,
	'items'        => $menuItems,
]);
NavBar::end();
?>