<?php

use backend\controllers\SiteController;

/**
 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
 */
?>
<div class="search-menu">
  <a class="<?= (Yii::$app->request->url == SiteController::getActionUrl(SiteController::ACTION_SEARCH) ? 'active' : '')?>" href="<?= SiteController::getActionUrl(SiteController::ACTION_SEARCH) ?>">Компании</a>
   <a class="<?= (Yii::$app->request->url == SiteController::getActionUrl(SiteController::ACTION_SEARCH_SERVICE) ? 'active' : '')?>" href="<?= SiteController::getActionUrl(SiteController::ACTION_SEARCH_SERVICE) ?>">Товары/Услуги</a>
</div>