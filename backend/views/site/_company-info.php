<?php

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 * @var \common\models\Company $company
 * @var int                    $index
 * @var int                    $colspan
 */

?>
<tr class="company-info" style="display: none;" id="<?= $company->rowguid ?>-<?= $index ?>">
    <td colspan="<?= $colspan ?>">
        <table class="table table-hover table-bordered table-condensed">
            <tr>
                <td width="200px" bgcolor="#faebd7"><b class="pull-right">Режим работы:</td>
                <td><?= $company->WorkHours ?></td>
            </tr>
            <tr>
                <td bgcolor="#faebd7"><b class="pull-right">Адрес:</td>
                <td><?= $company->town->Name ?>, <?= $company->street->name ?>, <?= $company->Home ?>, <?= $company->Office ?> </td>
            </tr>
            <tr>
                <td bgcolor="#faebd7"><b class="pull-right">Как добраться:</td>
                <td><?= $company->HowToGetScheme ?></td>
            </tr>
            <tr>
                <td bgcolor="#faebd7"><b class="pull-right">Телефоны:</td>
                <td>
					<?php if (null !== $company->phones): ?>
						<?php foreach ($company->phones as $phone): ?>
                            <a href="tel:<?= $phone->company->town->Phone_Code ?><?= $phone->Phone ?> ">(<?= $phone->company->town->Phone_Code ?>) <?= $phone->Phone ?> </a> <?= $phone->Description ?> <br>
						<?php endforeach ?>
					<?php endif ?>
                </td>
            </tr>
            <tr>
                <td bgcolor="#faebd7"><b class="pull-right">Доп. инфо:</td>
                <td><?= str_replace(PHP_EOL, '<br>', $company->Description) ?></td>
            </tr>
            <tr>
                <td bgcolor="#faebd7"><b class="pull-right">Электронная почта:</td>
                <td><a href="mailto:<?= $company->Email ?>"><?= $company->Email ?></a></td>
            </tr>
            <tr>
                <td bgcolor="#faebd7"><b class="pull-right">Сайт:</td>
                <td><a href="http://<?= $company->URL ?>"><?= $company->URL ?></a></td>
            </tr>
        </table>
        <div class="addresses-<?= $company->rowguid ?>"></div>

        <div class="contracts-<?= $company->rowguid ?>"></div>
        <p>
        <button class="btn btn-primary btn-sm send-sms-btn" data-company-guid="<?= $company->rowguid ?>" onclick="$(this).callsPlugin('sendSms', $(this))">Отправить SMS</button>
        <div class="alert alert-success sms-result" style="display: none;">SMS
            отправлена
        </div>
    </td>
</tr>
