<?php

use common\base\bootstrap\ActiveForm;
use common\models\User;
use yii\helpers\Html;

/**
 * @author Исаков Владислав
 *
 * @var User $login
 */
$this->params['breadcrumbs'][] = ['label' => 'Настройки'];
?>
<?php
$form = ActiveForm::begin([
	'id'      => 'login-form',
	'options' => ['class' => 'form-horizontal'],
]) ?>
<div class="container">
    <div class="form-group">
        <div class="row">
            <div class="col-xs-3">
				<?= $form->field($login, $login::ATTR_LOGICAL_NUMBER) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end() ?>
