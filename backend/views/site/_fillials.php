<?php

use backend\controllers\SiteController;

/**
 * @author Исаков Владислав
 *
 * @var \common\models\Addresses[] $addresses
 * @var \yii\web\View              $this
 */
?>

<?php if (count($addresses) > 0): ?>
	<div id="send-sms-filial-url" data-value="<?= SiteController::getActionUrl(SiteController::ACTION_SEND_SMS_FILIAL) ?>"></div>
	<table class="table table-hover table-bordered table-condensed filials">
		<thead>
		<tr>
			<th colspan="5" style="cursor:pointer;" onclick="console.log($(this).parent().parent().parent().find('tbody').toggle());">Филиалы <i class="pull-right glyphicon glyphicon-chevron-down"></i></th>
		</tr>

		</thead>
		<tbody style="display: none;">
		<tr>
			<th>Название</th>
			<th>Адрес</th>
			<th>Режим работы</th>
			<th>Телефон</th>
			<th></th>
		</tr>
		<?php foreach ($addresses as $address): ?>
			<tr>
				<td><?= $address->Name ?></td>
				<td><?= $address->city->Name ?>, <?= (null !== $address->addrStreet ? $address->addrStreet->name : '') ?> <?= $address->Home ?> <?= $address->Office ?> </td>
				<td><?= $address->WorkHours ?></td>
				<td><?= $address->Phone_NUm ?></td>
				<td class="text-center">
					<button data-company-guid="<?= $address->Firm_Num ?>" data-text="<?= $address->Name ?>: <?= $address->Phone_NUm ?>" class="btn btn-xs btn-primary filial-sms"
					        onclick="$('.filials').callsPlugin('sendSmsFilial', '<?= $address->Name ?>: <?=
					        $address->Phone_NUm
					        ?>');">SMS
					</button>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>

<?php endif ?>