<?php

use common\base\helpers\StringHelper;
use common\models\Company;
use common\models\District;
use common\models\Town;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var                                         $this \yii\web\View
 * @var \backend\models\forms\CompanySearchForm $form
 * @var \common\models\Company[]                $companies
 *
 */

$this->title .= ' - Поиск компаний';
echo $this->render('_params');
?>

<?= $this->render('form-menu') ?>
<div class="site-search">
	<?php Pjax::begin(); ?>
	<?php $htmlForm = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
    <div class="search-form">
        <div class="row">
            <div class="col-xs-2">
				<?= $htmlForm->field($form, $form::ATTR_COMPANY_NAME) ?>
            </div>
            <div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_PHONE) ?>
            </div>
            <div class="col-xs-2">
				<?= $htmlForm->field($form, $form::ATTR_PRODUCT_SERVICE) ?>
            </div>
            <div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_STREET) ?>
            </div>
            <div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_HOUSE_NUM) ?>
            </div>
            <div class="col-xs-2">
				<?= $htmlForm->field($form, $form::ATTR_CITY_GUID)->widget(Select2::class, [
					'data'          => Town::getAll(),
					'language'      => 'ru',
					'options'       => [
						'placeholder' => 'Выбрать город',
					],
					'pluginOptions' => [
						'allowClear'         => true,
						'minimumInputLength' => 2
					],
					'pluginEvents'  => [
						"select2:select" => "function() { $('#w0').submit(); }",
					]
				]); ?>
            </div>
            <div class="col-xs-2">
				<?= $htmlForm->field($form, $form::ATTR_CITY_DISTRICT)->widget(Select2::class, [
					'data'          => District::getAll()
					,
					'language'      => 'ru',
					'options'       => [
						'placeholder' => 'Выбрать',
					],
					'pluginOptions' => [
						'allowClear'         => true,
						'minimumInputLength' => 2
					],
					'pluginEvents'  => [
						"select2:select" => "function() { $('#w0').submit(); }",
					]
				]); ?>
            </div>
            <div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_DISTRICT_ALIAS) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2 text-center">
				<?= $htmlForm->field($form, $form::ATTR_COMPANY_NAME_POSITION)
					->radioButtonGroup(
						[$form::POSITION_ALL => 'везде', $form::POSITION_START => 'в начале', $form::POSITION_END => 'в конце'],
						[
							'class' => 'btn-group-xs',
						]
					)
					->label(false)
				?>
            </div>
            <div class="col-xs-1">

            </div>
            <div class="col-xs-2 text-center">
				<?= $htmlForm->field($form, $form::ATTR_SERVICE_NAME_POSITION)
					->radioButtonGroup(
						[$form::POSITION_ALL => 'везде', $form::POSITION_START => 'в начале', $form::POSITION_END => 'в конце'],
						[
							'class' => 'btn-group-xs',
						]
					)
					->label(false)
				?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="form-group">
					<?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'style' => 'display: none;']) ?>
                </div>
            </div>
        </div>
    </div>
	<?php ActiveForm::end(); ?>

    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered table-hover table-responsive result-table colored-tbl table-resizable">
                <tbody>
                <tr>
                    <th>Название</th>
                    <th width="100px">Город</th>
                    <th width="100px">Улица</th>
                    <th width="50px">Дом</th>
                    <th width="50px">Офис</th>
                </tr>
				<?php $i = 1 ?>
				<?php foreach ($companies as $company): ?>
                    <tr style="cursor:pointer;" onclick="$(this).searchPlugin('openCompanyInfo', '<?= $company->rowguid ?>', <?= $i ?>, '<?= StringHelper::GUID_NULL ?>')"
                        class="company-list-name <?= isset(Company::COLORS[$company->Color_Index]) ?
							Company::COLORS[$company->Color_Index] : '' ?>">
                        <td class="left-<?= Company::COLUMN_COLORS[$company->Color_Index] ?>">
							<?= $company->Official_Name ?>
							<?php
							$addressNames = [];
							foreach ($company->addresses as $address) {
								if (empty($address->Name)) {
									continue;
								}
								$addressNames[] = $address->Name;
							}
							?>
							<?= implode('/ ', $addressNames) ?>
                        </td>
                        <td>
							<?= $company->town->Name ?>
                        </td>
                        <td>
							<?= $company->street->name ?>
                        </td>
                        <td>
							<?= $company->Home ?>
                        </td>
                        <td>
							<?= $company->Office ?>
                        </td>

                    </tr>
					<?= $this->render('_company-info', ['company' => $company, 'index' => $i, 'colspan' => 6]) ?>
					<?php $i++ ?>
				<?php endforeach ?>
            </table>
        </div>
    </div>
	<?php
	$this->registerJs('
$(function() {
	var startX,
		 startWidth,
		 $handle,
		 $table,
		 pressed = false;
	
	$(document).on({
		mousemove: function(event) {
			if (pressed) {
				$handle.width(startWidth + (event.pageX - startX));
			}
		},
		mouseup: function() {
			if (pressed) {
				$table.removeClass(\'resizing\');
				pressed = false;
			}
		}
	}).on(\'mousedown\', \'.table-resizable th\', function(event) {
		$handle = $(this);
		pressed = true;
		startX = event.pageX;
		startWidth = $handle.width();
		
		$table = $handle.closest(\'.table-resizable\').addClass(\'resizing\');
	}).on(\'dblclick\', \'.table-resizable thead\', function() {
		// Reset column sizes on double click
		$(this).find(\'th[style]\').css(\'width\', \'\');
	});
});
');

	?>
	<?php Pjax::end(); ?>
</div>
<?= $this->registerJs('$("#check-call-url").callsPlugin();') ?>


