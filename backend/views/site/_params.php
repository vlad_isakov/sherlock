<?php
/**
 * @author Исаков Владислав <visakov@biletur.ru>
 */

use backend\controllers\SiteController;
use common\helpers\CallHelper;
use backend\controllers\AjaxController;

?>
<?php
$call = CallHelper::getNewCall();
?>
<div id="check-call-url" data-value="<?= SiteController::getActionUrl(SiteController::ACTION_CHECK_CALL) ?>"></div>
<div id="ringing-call-url" data-value="<?= SiteController::getActionUrl(SiteController::ACTION_CHECK_ONLINE_CALL) ?>"></div>
<div id="send-sms-url" data-value="<?= SiteController::getActionUrl(SiteController::ACTION_SEND_SMS) ?>"></div>
<div id="get-company-info" data-value="<?= SiteController::getActionUrl(SiteController::ACTION_GET_COMPANY_INFO) ?>"></div>
<div id="open-company-url" data-value="<?= SiteController::getActionUrl(SiteController::ACTION_OPEN_COMPANY) ?>"></div>
<div id="phone-to-sms" data-value="<?= (null === $call ? '' : $call->aNumber) ?>"></div>
<div id="city-guid" data-value="<?= (null === $call ? '' : $call->cityGuid) ?>"></div>
<div id="get-sms-count-url" data-value="<?= AjaxController::getActionUrl(AjaxController::ACTION_SMS_COUNT) ?>"></div>
