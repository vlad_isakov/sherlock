<?php
/**
 * @author Исаков Владислав
 *
 * @var \common\models\PriorityPrice[] $prices
 */

?>
<?php if (count($prices) > 0): ?>
	<table class="table table-hover table-bordered table-condensed">
		<thead>
		<tr>
            <th colspan="5" style="cursor:pointer;" onclick="console.log($(this).parent().parent().parent().find('tbody').toggle());">Прайс <i class="pull-right glyphicon glyphicon-chevron-down"></i></th>
		</tr>
		</thead>
		<tbody style="display: none;">
		<?php foreach ($prices as $price): ?>
			<tr>
				<td><?= $price->prioritet ?></td>
				<td><?= (null === $price->districtAlias ? '' : $price->districtAlias->NamesList) ?></td>
				<td>
					<?= (null !== $price->referenceOne ? $price->referenceOne->Name : '') ?>
					<?= (null !== $price->referenceTwo ? $price->referenceTwo->Name : '') ?>
				</td>
				<td><?= $price->Text1 ?></td>
				<td><?= $price->String1 ?></td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
<?php endif ?>
