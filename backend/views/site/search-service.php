<?php

use common\models\Company;
use common\models\District;
use common\models\Town;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var                                         $this yii\web\View
 * @var \backend\models\forms\ServiceSearchForm $form
 * @var array                                   $serviceCompanies
 *
 */

$this->title .= ' - Товары и услуги';
echo $this->render('_params');
?>
<?= $this->render('form-menu') ?>
<div class="site-search">
	<?php Pjax::begin(); ?>
	<?php $htmlForm = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
	<div class="search-form">
		<div class="row">
			<div class="col-xs-2">
				<?= $htmlForm->field($form, $form::ATTR_PRODUCT_SERVICE) ?>
			</div>
			<div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_COMPANY_NAME) ?>
			</div>
			<div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_ADDITIONAL_INFO) ?>
			</div>
			<div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_STREET) ?>
			</div>
			<div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_HOUSE_NUM) ?>
			</div>
			<div class="col-xs-2">
				<?= $htmlForm->field($form, $form::ATTR_CITY_GUID)->widget(Select2::class, [
					'data'          => Town::getAll(),
					'language'      => 'ru',
					'options'       => [
						'placeholder' => 'Выбрать город',
					],
					'pluginOptions' => [
						'allowClear'         => true,
						'minimumInputLength' => 2
					],
					'pluginEvents'  => [
						"select2:select" => "function() { $('#w0').submit(); }",
					]
				]); ?>
			</div>
			<div class="col-xs-2">
				<?= $htmlForm->field($form, $form::ATTR_CITY_DISTRICT)->widget(Select2::class, [
					'data'          => District::getAll()
					,
					'language'      => 'ru',
					'options'       => [
						'placeholder' => 'Выбрать',
					],
					'pluginOptions' => [
						'allowClear'         => true,
						'minimumInputLength' => 2
					],
					'pluginEvents'  => [
						"select2:select" => "function() { $('#w0').submit(); }",
					]
				]); ?>
			</div>
			<div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_DISTRICT_ALIAS) ?>
			</div>
			<div class="col-xs-1">
				<?= $htmlForm->field($form, $form::ATTR_SALE_TYPE) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2 text-center">
				<?= $htmlForm->field($form, $form::ATTR_SERVICE_NAME_POSITION)
					->radioButtonGroup(
						[$form::POSITION_ALL => 'везде', $form::POSITION_START => 'в начале', $form::POSITION_END => 'в конце'],
						[
							'class' => 'btn-group-xs',
						]
					)
					->label(false)
				?>
			</div>

		</div>
		<div class="row">
			<div class="col-xs-4">
				<div class="form-group">
					<?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'style' => 'display: none;']) ?>
				</div>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<a href="#newsModal" class="open-news-link btn btn-warning" data-toggle="modal" style="display: none;">Открыть новость</a>
			<div id="newsModal" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title">Новость</h4>
						</div>
						<div class="modal-body">
							<span class="news-text"></span>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<table class="table table-bordered table-hover table-responsive result-table colored-tbl table-resizable">
				<tr>
					<th width="15px">ПГ</th>
					<th width="15px">П</th>
					<th>Компания</th>
					<th>Доп.инфо</th>
					<th width="130px">Вид продаж</th>
					<th width="130px">Телефоны</th>
					<th width="130px">Район</th>
				</tr>
				<?php foreach ($serviceCompanies as $serviceName => $services): ?>
					<tr>
						<td colspan="7" style="background-color: #d4c9ff; font-weight: bolder; font-size: 14px;">
							Услуга: <?= $serviceName ?>
						</td>
					</tr>
					<?php $i = 1 ?>
					<?php /** @var \common\models\PriorityPrice[] $services */ ?>
					<?php foreach ($services as $service): ?>
						<?php
						if (null === $service->company) {
							continue;
						}
						?>
						<tr style="cursor:pointer;" onclick="$(this).searchPlugin('openCompanyInfo', '<?= $service->company->rowguid ?>', <?= $i ?>, '<?= $service->referenceOne->rowguid ?>')" class="company-list-name <?= isset
						(Company::COLORS[$service->company->Color_Index]) ?
							Company::COLORS[$service->company->Color_Index] : '' ?>">
							<td class="left-<?= Company::COLUMN_COLORS[$service->company->Color_Index] ?>">
								<b><?= $service->townprioritet ?></b>
							</td>
							<td class="left-<?= Company::COLUMN_COLORS[$service->company->Color_Index] ?>">
								<b><?= $service->prioritet ?></b>
							</td>
							<td>
								<?= $service->company->Official_Name ?>
							</td>
							<td>
								<?= $service->Text1 ?>
							</td>
							<td>
								<?= $service->String1 ?>
							</td>
							<td>
								<?php $phones = [] ?>
								<?php if (null !== $service->company->phones): ?>
									<?php foreach ($service->company->phones as $phone): ?>
										<?php $phones[] = $phone->Phone ?>
									<?php endforeach ?>
								<?php endif ?>
								<?= implode(', ', $phones) ?>
							</td>
							<td class="right-<?= Company::COLUMN_COLORS[$service->company->Color_Index] ?>">
								<?= $service->company->district->Name ?> <?= (null === $service->company->districtAlias ? '' : $service->company->districtAlias->NamesList) ?>
							</td>
						</tr>
						<?= $this->render('_company-info', ['company' => $service->company, 'index' => $i, 'colspan' => 8]) ?>
						<?php $i++ ?>
					<?php endforeach ?>
				<?php endforeach ?>
			</table>
		</div>
	</div>
	<?php
	$this->registerJs('
$(function() {
	var startX,
		 startWidth,
		 $handle,
		 $table,
		 pressed = false;
	
	$(document).on({
		mousemove: function(event) {
			if (pressed) {
				$handle.width(startWidth + (event.pageX - startX));
			}
		},
		mouseup: function() {
			if (pressed) {
				$table.removeClass(\'resizing\');
				pressed = false;
			}
		}
	}).on(\'mousedown\', \'.table-resizable th\', function(event) {
		$handle = $(this);
		pressed = true;
		startX = event.pageX;
		startWidth = $handle.width();
		
		$table = $handle.closest(\'.table-resizable\').addClass(\'resizing\');
	}).on(\'dblclick\', \'.table-resizable thead\', function() {
		// Reset column sizes on double click
		$(this).find(\'th[style]\').css(\'width\', \'\');
	});
});
');

	?>
	<?php Pjax::end(); ?>
</div>
<?= $this->registerJs('$("#check-call-url").callsPlugin();') ?>

