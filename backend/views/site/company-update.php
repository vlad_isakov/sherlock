<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\models\Company;

/**
 * @var                                         $this yii\web\View
 * @var \common\models\Company                  $company
 *
 */

$this->title .= ' - Поиск компаний';
?>
<div class="site-search">
	<?php $htmlForm = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-4">
			<?= $htmlForm->field($company, $company::ATTR_OFFICIAL_NAME) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="form-group">
				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
		<?php ActiveForm::end(); ?>
    </div>
    <div class="row">

    </div>
</div>
</div>