<?php
use common\models\Sms;
/**
 * @author Исаков Владислав
 *
 * @var \common\models\Sms[] $smsLog
 */
$this->params['breadcrumbs'][] = ['label' => 'Отправленные SMS'];
?>
<table class="table table-hover table-bordered table-condensed">
	<thead>
		<tr>
			<th>Дата</th>
			<th>Телефон</th>
			<th>Текст</th>
			<th>Статус</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($smsLog as $sms): ?>
			<tr class="<?= Sms::STATUS_COLOR[$sms->status] ?>">
				<td><?= $sms->date ?></td>
				<td><?= $sms->phone ?></td>
				<td><?= $sms->text ?></td>
				<td><?= Sms::STATUS_NAMES[$sms->status] ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
