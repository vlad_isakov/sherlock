<?php

use common\base\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;
use backend\controllers\AjaxController;
use yii\web\JsExpression;
use common\models\CompanyActionLog;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 * @var \backend\models\forms\CompanyActionLogSearchForm $form
 */
$this->params['breadcrumbs'][] = ['label' => 'Действия по компаниям'];
?>
<?php Pjax::begin(); ?>
<?php $htmlForm = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
	<div class="row">
		<div class="col-xs-3">
			<?=
			$htmlForm->field($form, $form::ATTR_COMPANY_GUID)->widget(Select2::class, [
				'model'         => $form,
				'attribute'     => $form::ATTR_COMPANY_GUID,
				'options'       => [
					'multiple'    => false,
					'placeholder' => 'Компания...',
					'prompt'      => 'Нет',
				],
				'pluginOptions' => [
					'ajax'               => [
						'url'      => AjaxController::getActionUrl(AjaxController::ACTION_AUTOCOMPLETE_COMPANY),
						'dataType' => 'json',
						'data'     => new JsExpression('function(params) { return {q:params.term}; }')
					],
					'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
					'templateResult'     => new JsExpression('function(data) { return data.text; }'),
					'templateSelection'  => new JsExpression('function (data) { return data.text; }'),
					'allowClear'         => true,
					'minimumInputLength' => 3,
				],
			])->label(false);
			?>
		</div>
		<div class="col-xs-3">
			<?= $htmlForm->field($form, $form::ATTR_ACTION_TYPE)->widget(Select2::class, [
				'data'          => CompanyActionLog::ACTION_NAMES,
				'language'      => 'ru',
				'options'       => [
					'placeholder' => 'Действие...',
				],
				'pluginOptions' => [
					'allowClear'         => true,
					'minimumInputLength' => 2,
				],
				'pluginEvents'  => [
					"select2:select" => "function() { $('#w0').submit(); }",
				]
			])->label(false); ?>
		</div>
		<div class="col-xs-3">
			<div class="form-group">
				<?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
			</div>
		</div>
	</div>

<?php ActiveForm::end(); ?>
	<div class="row">
		<div class="col-xs-12">
			<table class="table table-bordered table-striped log">
				<tr>
					<th>Дата</th>
					<th>Компания</th>
					<th>Событие</th>
					<th>Пользователь</th>
					<th>Сообщение</th>
					<th>Связанная сущность</th>
				</tr>
				<?php foreach ($form->result as $logRecord): ?>

					<tr>
						<td><?= $logRecord->insert_stamp ?></td>
						<td><?= (null !== $logRecord->company ? $logRecord->company->Official_Name : '') ?></td>
						<td><?= $logRecord::ACTION_NAMES[$logRecord->action_type] ?></td>
						<td><?= $logRecord->user->User_Name ?></td>
						<td class="log-message"><?= $logRecord->message ?></td>
						<td class="log-message"><?= $logRecord->reference_guid ?></td>
					</tr>
				<?php endforeach ?>
			</table>
		</div>
	</div>
<?php Pjax::end(); ?>