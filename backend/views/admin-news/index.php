<?php

use backend\controllers\AdminNewsController;
use backend\controllers\AjaxController;
use common\base\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/**
 * @var \backend\models\forms\NewsSearchForm $form
 *
 */

$this->params['breadcrumbs'][] = ['label' => 'Новости компаний'];
?>
<?php Pjax::begin(); ?>
<?php $htmlForm = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
<div class="row">
	<div class="col-xs-2">
		<a class="btn btn-md btn-primary" href="<?= AdminNewsController::getActionUrl(AdminNewsController::ACTION_CREATE)?>">Добавить</a>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-4">
		<?=
		$htmlForm->field($form, $form::ATTR_COMPANY_GUID)->widget(Select2::class, [
			'model'         => $form,
			'attribute'     => $form::ATTR_COMPANY_GUID,
			'options'       => [
				'multiple'    => false,
				'placeholder' => 'Компания...',
				'prompt'      => 'Нет',
			],
			'pluginOptions' => [
				'ajax'               => [
					'url'      => AjaxController::getActionUrl(AjaxController::ACTION_AUTOCOMPLETE_COMPANY),
					'dataType' => 'json',
					'data'     => new JsExpression('function(params) { return {q:params.term}; }')
				],
				'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
				'templateResult'     => new JsExpression('function(data) { return data.text; }'),
				'templateSelection'  => new JsExpression('function (data) { return data.text; }'),
				'allowClear'         => true,
				'minimumInputLength' => 3,
			],
		])->label(false);
		?>
	</div>
	<div class="col-xs-4">
		<?= $htmlForm->field($form, $form::ATTR_TITLE)->textInput(['placeholder' => 'Название...'])->label(false) ?>
	</div>
	<div class="col-xs-4">
		<button class="btn btn-primary">Найти </button>
	</div>
</div>
<?php ActiveForm::end(); ?>
<div class="row">
	<div class="col-xs-12">
		<table class="table table-bordered table-striped log">
			<tr>
				<th>Дата</th>
				<th>Компания</th>
				<th>Название</th>
				<th>Опубликована</th>
				<th></th>
			</tr>
			<?php foreach ($form->result as $news): ?>
				<tr bgcolor="<?= ($news->is_published == 0 ? '#c0c0c0' : '')?>">
					<td><?= $news->date ?></td>
					<td><?= $news->company->Official_Name ?></td>
					<td><?= $news->title ?></td>
					<td><?= ($news->is_published == 0 ? 'нет' : 'да')?></td>
					<td>
						<a href="<?= AdminNewsController::getActionUrl(AdminNewsController::ACTION_UPDATE, ['id' => $news->id])?>"><i class="glyphicon glyphicon-edit"> </i></a>
					</td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
</div>
<?php Pjax::end(); ?>
