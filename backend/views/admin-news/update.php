<?php

use backend\controllers\AjaxController;
use dosamigos\tinymce\TinyMce;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @var \common\modules\news\models\SiteNews $news
 * @var string                               $createdImage
 */

$this->title = 'Изменение страницы: ' . $news->title;
$this->params['breadcrumbs'][] = ['label' => 'Новости компаний', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $news->title, 'url' => ['view', 'id' => $news->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($news, $news::ATTR_TITLE)->textInput(['maxlength' => true]) ?>

<?= $form->field($news, $news::ATTR_DATE)->widget(DatePicker::class, [
	'options'       => ['placeholder' => 'Дата'],
	'pluginOptions' => [
		'autoclose' => true,
		'format'    => 'yyyy-mm-dd'
	]
]); ?>

<?=
$form->field($news, $news::ATTR_COMPANY_GUID)->widget(Select2::class, [
	'model'         => $news,
	'attribute'     => $news::ATTR_COMPANY_GUID,
	'options'       => [
		'multiple'    => false,
		'placeholder' => 'Компания...',
		'prompt'      => 'Нет',
	],
	'pluginOptions' => [
		'ajax'               => [
			'url'      => AjaxController::getActionUrl(AjaxController::ACTION_AUTOCOMPLETE_COMPANY),
			'dataType' => 'json',
			'data'     => new JsExpression('function(params) { return {q:params.term}; }')
		],
		'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
		'templateResult'     => new JsExpression('function(data) { return data.text; }'),
		'templateSelection'  => new JsExpression('function (data) { return data.text; }'),
		'allowClear'         => true,
		'minimumInputLength' => 3,
	],
])->label(false);
?>
<?= $form->field($news, $news::ATTR_IMAGE_FILE)->widget(FileInput::class, [
	'options'       => [
		'accept'   => 'image/*',
		'multiple' => false,
	],
	'pluginOptions' => [
		'initialPreviewShowDelete' => false,
		'allowedFileExtensions'    => ['jpeg', 'jpg', 'png'],
		'showPreview'              => true,
		'showCaption'              => true,
		'showRemove'               => false,
		'showUpload'               => false,
		'overwriteInitial'         => true,
		'maxFileSize'              => 28000,
		'initialPreview'           => [
			$createdImage,
		],
		'initialPreviewAsData'     => true,
		'initialPreviewConfig'     => [
			[
				'caption' => $createdImage,
			],
		],
	]
]); ?>

<?= $form->field($news, $news::ATTR_TEXT)->widget(TinyMce::class, [
	'options'       => ['rows' => 30],
	'language'      => 'ru',
	'clientOptions' => [
		'plugins' => [
			"advlist autolink lists link charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	]
]); ?>
<?= $form->field($news, $news::ATTR_IS_PUBLISHED)->checkbox() ?>

<div class="form-group">
	<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

