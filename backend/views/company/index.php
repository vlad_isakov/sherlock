<?php

use common\models\Town;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Pjax;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @var \backend\models\forms\CompanySearchForm $form
 * @var \common\models\Company[]                $companies
 */
?>
<?php Pjax::begin(); ?>
<?php $htmlForm = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
<div class="search-form">
	<div class="row">
		<div class="col-xs-5">
			<?= $htmlForm->field($form, $form::ATTR_CITY_GUID)->widget(Select2::class, [
				'data'          => Town::getAll(),
				'language'      => 'ru',
				'options'       => [
					'placeholder' => 'Выбрать город',
				],
				'pluginOptions' => [
					'allowClear'         => true,
					'minimumInputLength' => 2
				],
				'pluginEvents'  => [
					"select2:select" => "function() { $('#w0').submit(); }",
				]
			]); ?>
		</div>
		<div class="col-xs-6">
			<?= $htmlForm->field($form, $form::ATTR_COMPANY_NAME) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-2">
			<?= $htmlForm->field($form, $form::ATTR_COMPANY_NAME) ?>
		</div>
		<div class="col-xs-2">
			<?= $htmlForm->field($form, $form::ATTR_IS_NOT_HAVE_ADDRESS)->checkbox() ?>
		</div>
		<div class="col-xs-2">
			<?= $htmlForm->field($form, $form::ATTR_IS_NOT_HAVE_PHONE)->checkbox() ?>
		</div>
		<div class="col-xs-2">
			<?= $htmlForm->field($form, $form::ATTR_IS_NOT_HAVE_EMAIL)->checkbox() ?>
		</div>
		<div class="col-xs-2">
			<?= $htmlForm->field($form, $form::ATTR_IS_NOT_HAVE_LOGO)->checkbox() ?>
		</div>
		<div class="col-xs-2">
			<?= $htmlForm->field($form, $form::ATTR_IS_NOT_HAVE_IMAGES)->checkbox() ?>
		</div>
	</div>
</div>
<?php ActiveForm::end(); ?>
<div class="row">
	<div class="col-xs-12">
		<table class="table table-bordered table-hover table-responsive result-table colored-tbl table-resizable">
			<tr>

			</tr>
			<?php foreach ($companies as $company): ?>
				<tr>

				</tr>
			<?php endforeach ?>
		</table>
	</div>
</div>
<?php Pjax::end(); ?>
