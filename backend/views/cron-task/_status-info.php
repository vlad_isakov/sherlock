<?php

use common\models\CronSync;
use common\components\LocalServers;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 * @var \common\models\CronSync[]     $tasks
 * @var \common\models\ServerStatus[] $serverStatuses
 */
?>
<table class="table table-bordered table-hover table-striped">
    <tr>
        <th>Задача</th>
        <th>Статус</th>
        <th>Последнее сообщение</th>
        <th>Последнее выполнение</th>
    </tr>
	<?php foreach ($tasks as $task): ?>
        <tr>
            <td><?= CronSync::TASK_NAMES[$task->task] ?></td>
            <td class="<?= CronSync::CLASS_STATUS[$task->status] ?>"><?= CronSync::STATUS_NAMES[$task->status] ?></td>
            <td><?= $task->last_info ?></td>
            <td><?= $task->last_date ?></td>
        </tr>
	<?php endforeach ?>
</table>

<table class="table table-bordered table-hover table-striped">
    <tr>
        <th>Сервер</th>
        <th>Ip</th>
        <th>Статус</th>
        <th>Последняя проверка</th>
    </tr>
	<?php foreach ($serverStatuses as $serverStatus): ?>
        <tr>
            <td><?= LocalServers::SERVERS[$serverStatus->server_ip] ?></td>
            <td><?= $serverStatus->server_ip ?></td>
            <td class="<?= LocalServers::CLASS_STATUS[$serverStatus->status] ?>"><?= LocalServers::STATUS_NAMES[$serverStatus->status] ?></td>
            <td><?= $serverStatus->log_time ?></td>
        </tr>
	<?php endforeach ?>
</table>