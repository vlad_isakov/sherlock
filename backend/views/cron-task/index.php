<?php

use backend\controllers\CronTaskController;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 * @var \yii\web\View $this
 */
$this->params['breadcrumbs'][] = ['label' => 'Статусы системы'];
?>
<div class="cron-status-container" data-url="<?= CronTaskController::getActionUrl(CronTaskController::ACTION_GET_TASKS) ?>"></div>

<?php
$this->registerJs('$(".cron-status-container").cronTaskPlugin()');
?>