<?php
use kartik\form\ActiveForm;
use yii\helpers\Html;
/**
 * @author Исаков Владислав
 *
 * @var \common\models\CompanyPhone[] $phones
 */

$form = ActiveForm::begin([
	'id' => 'login-form',
	'options' => ['class' => 'form-horizontal'],
]) ?>
	<div class="form-group">


		<div class="col-lg-offset-1 col-lg-11">
			<?= Html::submitButton('Вход', ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
<?php ActiveForm::end() ?>