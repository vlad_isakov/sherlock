<?php
namespace backend\widgets;

use common\models\CompanyPhone;
/**
 * @author Исаков Владислав <isakov.vi@dns-shop.ru>
 */

class SendSmsWidget extends \yii\base\Widget {

	/** @var string Guid Компании */
	public $companyGuid;
	const ATTR_COMPANY_GUID = 'companyGuid';

	public function run() {
		$phones = CompanyPhone::find()
			->andWhere([CompanyPhone::ATTR_FIRM_GUID => $this->companyGuid])
			->all();

		return $this->render('send-sms', ['phones' => $phones]);
	}
}