<?php

use common\models\User;

$params = array_merge(
	require __DIR__ . '/../../common/config/params.php',
	require __DIR__ . '/../../common/config/params-local.php',
	require __DIR__ . '/params.php',
	require __DIR__ . '/params-local.php'
);

return [
	'id'                  => 'app-backend',
	'name'                => 'Sherlock WEB',
	'language'            => 'ru',
	'basePath'            => dirname(__DIR__),
	'controllerNamespace' => 'backend\controllers',
	'bootstrap'           => ['log'],
	'components'          => [
		'authManager'  => [
			'class' => 'yii\rbac\DbManager',
		],
		'request'      => [
			'csrfParam' => '_csrf-backend-2',
			'baseUrl'   => '/internal',
		],
		'user'         => [
			'identityCookie' => [
				'name'     => '_identity-backend-2',
				'httpOnly' => true
			],
		],
		'session'      => [
			// this is the name of the session cookie used for login on the backend
			'name' => 'advanced-backend',
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'urlManager'   => [
			'enablePrettyUrl' => true,
			'showScriptName'  => false,
			'suffix'          => '/',
			'rules'           => [
				''                                                             => 'site/index',
				'company-search'                                               => 'site/search',
				'service-search'                                               => 'site/search-service',
				'company-view'                                                 => 'site/view-company',
				'log'                                                          => 'log/index',
				'<controller:\w+>/<action:\w+>/'                               => '<controller>/<action>',
				'<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
			],
		],
	],
	'modules'             => [
		'gridview' => ['class' => 'kartik\grid\Module'],
		'rbac'     => [
			/**
			 * /rbac/rule
			 * /rbac/permission
			 * /rbac/role
			 * /rbac/assignment
			 */

			'class'                    => \common\modules\rbac\Module::class,
			'userModelClassName'       => User::class,
			'userModelIdField'         => User::ATTR_GUID,
			'userModelLoginField'      => User::ATTR_USER_NAME,
			'userModelLoginFieldLabel' => null,
			'beforeCreateController'   => function ($route) {
				/**  @var string $route The route consisting of module, controller and action IDs. */
				return true;
			},
			'beforeAction'             => function ($action) {
				/** @var yii\base\Action $action the action to be executed. */
				return true;
			}
		]
	],
	'params'              => $params,
];
