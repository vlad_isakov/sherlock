<?php

namespace backend\controllers;

use common\models\CronSync;
use common\models\ServerStatus;
use yii\filters\AccessControl;
use common\modules\rbac\rules\Permissions;

class CronTaskController extends BackendController {

	const ACTION_INDEX = 'index';
	const ACTION_GET_TASKS = 'get-tasks';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'actions' => [
							static::ACTION_INDEX,
							static::ACTION_GET_TASKS,
						],
						'roles' => [Permissions::P_ADMIN],
					],
				],
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex() {
		return $this->render('index');
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionGetTasks() {
		$tasks = CronSync::find()->all();
		$serverStatuses = ServerStatus::find()->all();

		$this->layout = false;

		return $this->renderPartial('_status-info', ['tasks' => $tasks, 'serverStatuses' => $serverStatuses]);
	}
}
