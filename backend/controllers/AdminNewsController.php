<?php

namespace backend\controllers;

use backend\models\forms\NewsSearchForm;
use common\models\ObjectFile;
use common\modules\news\models\SiteNews;
use common\modules\rbac\rules\Permissions;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Контроллер управления новостями сайта
 *
 * @package backend\controllers
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class AdminNewsController extends BackendController {
	const ACTION_INDEX = 'index';
	const ACTION_UPDATE = 'update';
	const ACTION_CREATE = 'create';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow'   => true,
						'actions' => [
							static::ACTION_INDEX,
							static::ACTION_UPDATE,
							static::ACTION_CREATE,
						],
						'roles'   => [Permissions::ROLE_ADMIN],
					],
				],
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionIndex() {
		$form = new NewsSearchForm();

		if (Yii::$app->request->isPost) {
			$form->load(Yii::$app->request->post());
		}

		$form->search();

		return $this->render('index', ['form' => $form]);
	}

	/**
	 * @param $id
	 *
	 * @return string
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionUpdate($id) {
		/** @var SiteNews $news */
		$news = SiteNews::find()
			->andWhere([SiteNews::ATTR_ID => $id])
			->one();

		if (null === $news) {
			throw new NotFoundHttpException('Новость не найдена');
		}


		if (Yii::$app->request->isPost) {
			$news->load(Yii::$app->request->post());
			if ($news->save()) {

				$photo = UploadedFile::getInstance($news, $news::ATTR_IMAGE_FILE);

				if($photo === null){
					return '';
				}


				$fileName = $photo->name;
				$filePath = SiteNews::getLogoPath() . DIRECTORY_SEPARATOR . substr($fileName, 0, 2);

				if (!is_dir($filePath)) {
					mkdir($filePath);
				}
				if ($photo->saveAs($filePath . DIRECTORY_SEPARATOR . $fileName)) {
					$objectFile = new ObjectFile();
					$objectFile->object_id = $news->id;
					$objectFile->object = SiteNews::class;
					$objectFile->filename = $fileName;
					$objectFile->save();
				}
			}

			return $this->redirect(AdminNewsController::getActionUrl(AdminNewsController::ACTION_INDEX));
		}

		$createdImage = null;

		return $this->render('update', ['news' => $news, 'createdImage' => $createdImage]);
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionCreate() {
		$news = new SiteNews();

		if (Yii::$app->request->isPost) {
			$news->load(Yii::$app->request->post());
			$news->save();

			return $this->redirect(AdminNewsController::getActionUrl(AdminNewsController::ACTION_INDEX));
		}

		return $this->render('update', ['news' => $news, 'createdImage' => null]);
	}
}
