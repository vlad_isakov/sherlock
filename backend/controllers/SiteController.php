<?php

namespace backend\controllers;

use backend\models\forms\CompanySearchForm;
use backend\models\forms\ServiceSearchForm;
use common\base\helpers\StringHelper;
use common\helpers\CallHelper;
use common\models\Company;
use common\models\CompanyActionLog;
use common\models\CompanyNews;
use common\models\LoginForm;
use common\models\PriorityPrice;
use common\models\Sms;
use common\models\SMSAction;
use common\models\User;
use common\modules\rbac\rules\Permissions;
use Yii;
use yii\caching\TagDependency;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends BackendController {

	const ACTION_CHECK_CALL = 'check-call';
	const ACTION_CHECK_ONLINE_CALL = 'check-online-call';
	const ACTION_SEND_SMS = 'send-sms';
	const ACTION_SEND_SMS_FILIAL = 'send-sms-filial';
	const ACTION_OPEN_COMPANY = 'open-company';
	const ACTION_SEARCH = 'search';
	const ACTION_SEARCH_SERVICE = 'search-service';
	const ACTION_SETTINGS = 'settings';
	const ACTION_GET_COMPANY_INFO = 'get-company-info';
	const ACTION_SMS_LOG = 'sms-log';
	const ACTION_LOGIN = 'login';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'actions' => ['login', 'error'],
						'allow'   => true,
					],
					[
						'actions' => [
							'index',
							static::ACTION_SEARCH,
							static::ACTION_SEARCH_SERVICE,
							'view-company',
							static::ACTION_SEND_SMS,
							static::ACTION_SEND_SMS_FILIAL,
							static::ACTION_OPEN_COMPANY,
							static::ACTION_SETTINGS,
							static::ACTION_GET_COMPANY_INFO,
							static::ACTION_SMS_LOG,
						],
						'allow'   => true,
						'roles'   => ['@']//[Permissions::P_ADMIN, Permissions::P_SEARCH_COMPANY],
					],
					[
						'actions' => [
							static::ACTION_SEND_SMS,
							static::ACTION_SEND_SMS_FILIAL,
							static::ACTION_SMS_LOG,
						],
						'allow'   => true,
						'roles'   => ['@']//[Permissions::P_ADMIN, Permissions::P_SEND_SMS],
					],
					[
						'actions' => [
							'logout',
							static::ACTION_CHECK_CALL,
							static::ACTION_CHECK_ONLINE_CALL,
						],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::class,
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex() {
		return $this->render('index');
	}

	/**
	 * Запрос соидененного входящего звонка
	 *
	 * @return mixed
	 */
	public function actionCheckCall() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$call = CallHelper::getNewCall(true);
		$newCall = false;
		if (null !== $call) {
			$newCall = true;
		}

		return ['call' => $call, 'newCall' => $newCall];
	}

	/**
	 * Запрос звонящего входящего звонка
	 *
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionCheckOnlineCall() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$call = CallHelper::getNewCall(false);

		$hasRinging = false;

		if (null !== $call) {
			$hasRinging = true;
		}

		return ['call' => $call, 'hasRinging' => $hasRinging];
	}

	/**
	 * Открытие карточки компании
	 *
	 * @param string      $phone
	 * @param string      $companyGuid
	 * @param string|null $serviceGuid
	 * @param string|null $townGuid
	 *
	 * @return array
	 * @throws \yii\web\NotFoundHttpException
	 *
	 */
	public function actionOpenCompany($phone, $companyGuid, $serviceGuid = null, $townGuid = null) {
		CompanyActionLog::add($companyGuid, CompanyActionLog::ACTION_OPEN_COMPANY, '');

		Yii::$app->response->format = Response::FORMAT_JSON;

		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, 'getCompany', $companyGuid]);
		$company = Yii::$app->cache->get($cacheKey);
		/** @var Company $company */
		if (false === $company) {
			$company = Company::find()->where([Company::ATTR_GUID => $companyGuid])->one();
			Yii::$app->cache->set($cacheKey, $company, 3600 * 3);
		}

		if (null === $company) {
			throw new NotFoundHttpException('Компания не найдена');
		}

		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, 'getActionText', $serviceGuid, 4]);
		$smsActionText = Yii::$app->cache->get($cacheKey);

		if (false === $smsActionText) {
			$smsActionText = SMSAction::getAction($serviceGuid);

			Yii::$app->cache->set($cacheKey, $smsActionText, 3600 * 3);
		}

		if (false !== $smsActionText) {
			sleep(1);
			$cacheKey = Yii::$app->cache->buildKey([__METHOD__, 'action', $phone, $companyGuid, $serviceGuid, Yii::$app->user->id]);
			$isAlreadySentSms = Yii::$app->cache->get($cacheKey);
			if (false === $isAlreadySentSms) {
				$text =  substr($smsActionText, 0, 1000);
				$smsModel = Sms::find()
					->andWhere([Sms::ATTR_PHONE => $phone])
					->andWhere([Sms::ATTR_TEXT => $text])
					->one();

				if (null === $smsModel) {
					$smsModel = new Sms();
					$smsModel->text = $text;
					$smsModel->date = new Expression('getdate()');
					$smsModel->phone = $phone;
					$smsModel->user_guid = Yii::$app->user->id;
					$smsModel->user_id = 0;
					$smsModel->status = Sms::STATUS_WAIT_FOR_SEND;

					if ($smsModel->save()) {
						CompanyActionLog::add($companyGuid, CompanyActionLog::ACTION_SEND_SMS_ACTION, $smsModel->text, $smsModel->id);
						//Лочим отправку смс-акции для данной услуги на этот номер на сутки
						Yii::$app->cache->set($cacheKey, 'sent', 3600 * 3);
					}
				}
			}
		}

		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, 'getNewsText', $serviceGuid, $companyGuid, $townGuid, 7]);
		$newsArray = Yii::$app->cache->get($cacheKey);

		if (false === $newsArray) {
			if (null === $townGuid || empty($townGuid)) {
				$townGuid = 'E6D830DB-FA47-4D7C-8D6E-5BD09E037E98';
			}

			$prNumber = '-5';
			$priceNumber = '4A304E1F-52B4-41F4-9DAA-04017E04D69D';
			$nameNum = '%#' . $serviceGuid . '#%';

			$price = PriorityPrice::findOne([PriorityPrice::ATTR_FIRM_GUID => $companyGuid, PriorityPrice::ATTR_REFERENCE_ONE => $serviceGuid]);
			if (null !== $price) {
				$prNumber = $price->rowguid;
			}

			$newsGuid = Yii::$app->db->createCommand("exec SKS_Get_NewsE 
				@rowguid = :p_rowguid, 
				@pr_number = :p_pr_number,
				@price_number = :p_price_number,
				@firm_number = :p_firm_number,
				@operation_type = :p_operation_type,
				@Name_num = :p_name_num,
				@town_num = :p_town_num
			")
				->bindValue(':p_rowguid', $serviceGuid)
				->bindValue(':p_pr_number', $prNumber)
				->bindValue(':p_price_number', $priceNumber)
				->bindValue(':p_firm_number', $companyGuid)
				->bindValue(':p_operation_type', 0)
				->bindValue(':p_name_num', $nameNum)
				->bindValue(':p_town_num', $townGuid)
				->queryOne();

			$newsArray['newsText'] = CompanyNews::getText($newsGuid['rowguid']);
			$newsArray['ownerType'] = $newsGuid['owner_type'];

			Yii::$app->cache->set($cacheKey, $newsArray, 3600 * 3);
		}

		return [
			'status'    => 'success',
			'text'      => $smsActionText,
			'newsText'  => $newsArray['newsText'],
			'ownerType' => $newsArray['ownerType']
		];
	}

	/**
	 *
	 * Отправка SMS
	 *
	 * @param string $phone
	 * @param string $companyGuid
	 *
	 * @return array
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 */
	public function actionSendSms($phone, $companyGuid) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		/** @var Company $company */
		$company = Company::find()->where([Company::ATTR_GUID => $companyGuid])->one();

		if (null === $company) {
			throw new NotFoundHttpException('Компания не найдена');
		}

		$phones = [];
		if (count($company->phones) > 0) {
			$phones[] = $company->phones[0]->Phone;
		}

		$companyName = $company->Official_Name;

		if (false !== strpos($companyName, '/')) {
			$companyName = explode('/', $companyName);
			$companyName = $companyName[0];
		}

		$text = trim($companyName) . ': ' . implode(', ', $phones);

		$smsModel = new Sms();
		$smsModel->text = substr($text, 0, 255);
		$smsModel->date = new Expression('getdate()');
		$smsModel->phone = $phone;
		$smsModel->user_guid = Yii::$app->user->id;
		$smsModel->user_id = 0;
		$smsModel->operator_id = 0;
		$smsModel->status = Sms::STATUS_WAIT_FOR_SEND;
		$smsModel->save();

		CompanyActionLog::add($companyGuid, CompanyActionLog::ACTION_SEND_SMS_BY_DEMAND, $smsModel->text, $smsModel->id);

		sleep(1);

		return [$phone, $text];
	}

	/**
	 *
	 * Отправка SMS
	 *
	 * @param string $phone
	 * @param string $text
	 * @param string $companyGuid
	 *
	 * @return array
	 *
	 */
	public function actionSendSmsFilial($phone, $text, $companyGuid = StringHelper::GUID_NULL) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$smsModel = new Sms();
		$smsModel->text = substr($text, 0, 255);
		$smsModel->date = new Expression('getdate()');
		$smsModel->phone = $phone;
		$smsModel->user_guid = Yii::$app->user->id;
		$smsModel->operator_id = 0;
		$smsModel->user_id = 0;
		$smsModel->status = Sms::STATUS_WAIT_FOR_SEND;
		$smsModel->save();

		CompanyActionLog::add($companyGuid, CompanyActionLog::ACTION_SEND_SMS_BY_DEMAND, $smsModel->text, $smsModel->id);

		sleep(1);

		return [$phone, $text];
	}

	/**
	 * Поиск компаний
	 *
	 * @return string
	 */
	public function actionSearch() {
		$companies = [];
		$form = new CompanySearchForm();

		if (Yii::$app->request->isPost) {

			if ($form->load(Yii::$app->request->post())) {
				$companies = $form->search();
			}
		}

		return $this->render('search', ['form' => $form, 'companies' => $companies]);
	}

	/**
	 * Поиск компаний
	 *
	 * @return string
	 */
	public function actionSearchService() {
		$serviceCompanies = [];
		$form = new ServiceSearchForm();

		if (Yii::$app->request->isPost) {
			if ($form->load(Yii::$app->request->post())) {
				$serviceCompanies = $form->search();
			}
		}

		return $this->render('search-service', ['form' => $form, 'serviceCompanies' => $serviceCompanies]);
	}

	public function actionViewCompany($guid) {
		$company = Company::find()->andWhere([Company::tableName() . '.' . Company::ATTR_GUID => $guid])
			->joinWith(Company::REL_CONTRACTS)
			->one();

		if (null === $company) {
			throw  new NotFoundHttpException('Компания не найдена');
		}

		if (Yii::$app->request->isPost) {
			$company->load(Yii::$app->request->post());
			$company->save();
		}

		return $this->render('company-update', ['company' => $company]);
	}

	/**
	 * @param string $guid
	 *
	 * @return array
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionGetCompanyInfo($guid) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		/** @var Company $company */
		$company = Company::find()
			->andWhere([Company::tableName() . '.' . Company::ATTR_GUID => $guid])
			->joinWith(Company::REL_ADDRESSES)
			->one();

		if (null === $company) {
			return [
				'contracts' => '',
				'addresses' => ''
			];
		}

		return [
			'contracts' => $this->renderAjax('_price', ['prices' => $company->activePriorityPrices]),
			'addresses' => $this->renderAjax('_fillials', ['addresses' => $company->addresses])
		];
	}

	/**
	 * Login action.
	 *
	 * @return string
	 */
	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		}
		else {
			$model->password = '';

			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Logout action.
	 *
	 * @return string
	 */
	public function actionLogout() {
		Yii::$app->user->logout();

		return $this->goHome();
	}

	/**
	 * @return string
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionSettings() {

		/** @var User $login */
		$login = User::find()->where([User::ATTR_GUID => Yii::$app->user->id])->one();

		if (null === $login) {
			throw new NotFoundHttpException('Пользователь не найден');
		}

		if (Yii::$app->request->isPost) {
			$login->load(Yii::$app->request->post());

			if ($login->save()) {
				TagDependency::invalidate(Yii::$app->cache, [User::class]);
				Yii::$app->session->addFlash('success', 'Сохранено');
			}
		}

		return $this->render('settings', ['login' => $login]);
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function actionSmsLog() {
		$smsLog = Sms::find();

		if (!Yii::$app->user->can(Permissions::P_ADMIN)) {
			$smsLog->andWhere([Sms::ATTR_USER_GUID => Yii::$app->user->id]);
		}

		$smsLog->orderBy([Sms::ATTR_DATE => SORT_DESC]);
		$smsLog->limit(20);
		$smsLog = $smsLog->all();

		return $this->render('sms-log', ['smsLog' => $smsLog]);
	}
}
