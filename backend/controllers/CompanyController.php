<?php

namespace backend\controllers;

use backend\models\forms\CompanySearchForm;
use common\models\Company;
use common\modules\rbac\rules\Permissions;
use Yii;
use yii\filters\AccessControl;

/**
 * Class CompanyController
 *
 * @package backend\controllers
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class CompanyController extends BackendController {
	const ACTION_INDEX = 'index';
	const ACTION_UPDATE = 'update';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow'   => true,
						'actions' => [
							static::ACTION_INDEX,
							static::ACTION_UPDATE,
						],
						'roles'   => [Permissions::ROLE_ADMIN],
					],
				],
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionIndex() {
		$form = new CompanySearchForm();

		if (Yii::$app->request->isPost) {
			$form->load(Yii::$app->request->post());
			$form->search();
		}

		return $this->render('index', ['form' => $form]);
	}

	/**
	 * @param string $guid
	 *
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionUpdate($guid) {
		$company = Company::find()
			->andWhere([Company::ATTR_GUID => $guid])
			->one();

		return $this->render('update', ['company' => $company]);
	}
}
