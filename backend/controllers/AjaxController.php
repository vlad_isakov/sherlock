<?php
namespace backend\controllers;

use common\models\Sms;
use yii\filters\AccessControl;
use Yii;
use yii\web\Response;
use yii\web\MethodNotAllowedHttpException;
use common\models\Company;
use common\models\User;
use yii\db\Expression;
use common\base\helpers\StringHelper;

/**
 * Контроллер для AJAX-запросов справочников
 *
 * @package backend\controllers
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class AjaxController extends BackendController {

	const ACTION_AUTOCOMPLETE_COMPANY = 'autocomplete-company';
	const ACTION_AUTOCOMPLETE_USER = 'autocomplete-user';
	const ACTION_SMS_COUNT = 'sms-count';

	/**
	 * @param $action
	 *
	 * @return bool|\yii\web\Response
	 *
	 * @throws \yii\web\MethodNotAllowedHttpException
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function beforeAction($action) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		if (false === Yii::$app->request->isAjax) {
			throw new MethodNotAllowedHttpException();
		}

		return parent::beforeAction($action);
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'actions' => [
							static::ACTION_AUTOCOMPLETE_USER,
							static::ACTION_AUTOCOMPLETE_COMPANY,
							static::ACTION_SMS_COUNT,
						],
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @param string $q
	 *
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionAutocompleteCompany($q) {
		$result = [
			'results' => []
		];

		/** @var Company[] $companies */
		$companies = Company::find()
			->andWhere(['LIKE', new Expression('lower(' . Company::ATTR_OFFICIAL_NAME . ')'), mb_strtolower($q)])
			->limit(20)
			->all();

		if (count($companies) === 0) {
			$q = StringHelper::switcher($q, 3);
			/** @var Company[] $companies */
			$companies = Company::find()
				->andWhere(['LIKE', new Expression('lower(' . Company::ATTR_OFFICIAL_NAME . ')'), mb_strtolower($q)])
				->limit(20)
				->all();
		}

		foreach ($companies as $company) {
			$result['results'][] = ['id' => $company->rowguid, 'text' => $company->Official_Name];
		}

		return $result;
	}

	/**
	 * @param string $q
	 *
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionAutocompleteUser($q) {
		$result = [
			'results' => []
		];

		/** @var User[] $users */
		$users = User::find()
			->andWhere(['LIKE', new Expression('lower(' . User::ATTR_USER_NAME . ')'), mb_strtolower($q)])
			->limit(20)
			->all();

		if (count($users) === 0) {
			$q = StringHelper::switcher($q, 3);
			/** @var User[] $users */
			$users = Company::find()
				->andWhere(['LIKE', new Expression('lower(' . User::ATTR_USER_NAME . ')'), mb_strtolower($q)])
				->limit(20)
				->all();
		}

		foreach ($users as $user) {
			$result['results'][] = ['id' => $user->rowguid, 'text' => $user->User_Name];
		}

		return $result;
	}

	/**
	 * @return mixed
	 *
	 * @author Исаков Владислав
	 */
	public function actionSmsCount() {
		$result['delivered'] = Sms::getCountBuStatus(Yii::$app->user->id, Sms::STATUS_DELIVERED);
		$result['not_delivered'] = Sms::getCountBuStatus(Yii::$app->user->id,Sms::STATUS_NOT_DELIVERED);

		return $result;
	}
}
