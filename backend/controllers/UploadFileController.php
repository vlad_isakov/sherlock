<?php

namespace backend\controllers;

use backend\components\BackendController;
use common\models\CategoryCatalog;
use common\models\CustomService;
use common\models\ObjectFile;
use common\models\OrderProduct;
use common\models\ProductCatalog;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Контроллер загрузки файлов
 *
 * @author Исаков Владислав <newsperparser@gmail.com>
 */
class UploadFileController extends BackendController {
	const ACTION_INDEX = 'index';
	const ACTION_DELETE = 'delete';
	const ACTION_DELETE_FROM_CATALOG = 'delete-from-catalog';
	const ACTION_DELETE_FROM_CATEGORY = 'delete-from-category';
	const ACTION_GET_FILE = 'get-file';

	/**
	 * @inheritdoc
	 *
	 * @author Исаков Владислав <newsperparser@gmail.com>
	 */
	public function beforeAction($action) {

		if (!Yii::$app->user->can('admin') && !Yii::$app->user->can('manager')) {
			throw new ForbiddenHttpException('У вас нет прав доступа.');
		}

		return parent::beforeAction($action);
	}

	/**
	 * Загрузка
	 *
	 * @param string $objectName Модель к которой подгружаем файл
	 * @param int    $objectId   Идентификатор данных к которым привязываем файл
	 *
	 * @return array
	 *
	 * @throws \yii\web\MethodNotAllowedHttpException
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав <newsperparser@gmail.com>
	 */
	public function actionIndex($objectName, $objectId) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		if (false === Yii::$app->request->isAjax) {
			throw new MethodNotAllowedHttpException();
		}

		$response = [];

		switch ($objectName) {
			case OrderProduct::class:
				$object = OrderProduct::findOne($objectId);
				if (null === $object) {
					throw new NotFoundHttpException('Заказ не найден.');
				}
				break;
			case ProductCatalog::class:
				$object = ProductCatalog::find()
					->where([ProductCatalog::ATTR_ID => $objectId])
					->userFilter()
					->one();

				if (null === $object) {
					throw new NotFoundHttpException('Товар не найден в каталоге.');
				}
				break;
			case CategoryCatalog::class:
				$object = CategoryCatalog::find()
					->where([CategoryCatalog::ATTR_ID => $objectId])
					->userFilter()
					->one();
				if (null === $object) {
					throw new NotFoundHttpException('Товар не найден в каталоге.');
				}
				break;
			default:
				$response[] = ['error' => 'Объект не зарегистрирован для загрузки файлов.'];
				break;
		}

		if (count($response) > 0) {
			return $response;
		}

		$objectFile = new ObjectFile();

		$objectFile->object = $objectName;
		$objectFile->object_id = $objectId;
		$objectFile->file = UploadedFile::getInstance($object, $object::ATTR_FILE);

		if (null === $objectFile->file) {
			$response[] = ['error' => 'Произошла ошибка. Обратитись к системным администраторам.'];

			return $response;
		}
		$objectFile->filename = $objectFile->file->name;

		if (false === $objectFile->validate($object::ATTR_FILE)) {
			$response[] = ['error' => $objectFile->getFirstError()];

			return $response;
		}

		if (!$objectFile->save(false)) {
			$response[] = ['error' => 'Невозможно сохранить данные о файле. Обратитись к системным администраторам.'];

			return $response;
		}

		if (false === $objectFile->file->saveAs($objectFile->getDir() . '/' . $objectFile->filename, true)) {
			$response[] = ['error' => 'Невозможно сохранить файл. Обратитись к системным администраторам.'];
			$objectFile->delete();

			return $response;
		}

		$response['files'][] = [
			'name'       => $objectFile->file->name,
			'type'       => $objectFile->file->type,
			'size'       => $objectFile->file->size,
			'url'        => static::getActionUrl(static::ACTION_GET_FILE, ['id' => $objectFile->id]),
			'deleteUrl'  => static::getActionUrl(static::ACTION_DELETE, ['id' => $objectFile->id]),
			'deleteType' => 'POST'
		];

		return $response;
	}

	/**
	 * Удаление файла
	 *
	 * @param int $id
	 *
	 * @return array
	 * @throws \yii\web\NotFoundHttpException
	 *
	 */
	public function actionDelete($id) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$objectFile = ObjectFile::findOne($id);

		if (null === $objectFile) {
			throw new NotFoundHttpException();
		}
		$objectFile->delete();

		return [];
	}

	/**
	 * Удаление файла из каталога
	 *
	 * @param int $id
	 * @param int $productId
	 *
	 * @return \yii\web\Response
	 * @throws \yii\web\NotFoundHttpException
	 *
	 */
	public function actionDeleteFromCatalog($id, $productId) {

		$objectFile = ObjectFile::findOne($id);

		if (null === $objectFile) {
			throw new NotFoundHttpException();
		}
		$objectFile->delete();

		return $this->redirect(CatalogController::getActionUrl(CatalogController::ACTION_UPDATE_PRODUCT, ['id' => $productId]));
	}

	/**
	 * Удаление файла из категории каталога
	 *
	 * @param int $id
	 * @param int $categoryId
	 *
	 * @return \yii\web\Response
	 * @throws \yii\web\NotFoundHttpException
	 *
	 */
	public function actionDeleteFromCategory($id, $categoryId) {

		$objectFile = ObjectFile::findOne($id);

		if (null === $objectFile) {
			throw new NotFoundHttpException();
		}
		$objectFile->delete();

		return $this->redirect(CatalogController::getActionUrl(CatalogController::ACTION_UPDATE_CATEGORY, ['id' => $categoryId]));
	}

	/**
	 * Скачивание файла по идентификатору
	 *
	 * @param int $id
	 *
	 * @return yii\web\Response
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав <newsperparser@gmail.com>
	 */
	public function actionGetFile($id) {
		$objectFile = ObjectFile::findOne($id);

		if (null === $objectFile) {
			throw new NotFoundHttpException();
		}
		$path = $objectFile->getFullPath();

		return Yii::$app->response->sendFile($path, $objectFile->filename);
	}
}