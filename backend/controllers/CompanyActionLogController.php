<?php

namespace backend\controllers;

use backend\models\forms\CompanyActionLogSearchForm;
use common\models\CompanyActionLog;
use common\models\LoginForm;
use common\models\LogYii;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\modules\rbac\rules\Permissions;

/**
 * Class CompanyActionLogController
 *
 * @package backend\controllers
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class CompanyActionLogController extends BackendController {
	const ACTION_INDEX = 'index';


	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'actions' => [
							static::ACTION_INDEX,
						],
						'roles' => [Permissions::ROLE_ADMIN],
					],
				],
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionIndex() {
		$form = new CompanyActionLogSearchForm();

		if (Yii::$app->request->isPost) {
			$form->load(Yii::$app->request->post());
			$form->search();
		}

		return $this->render('index', ['form' => $form]);
	}
}
