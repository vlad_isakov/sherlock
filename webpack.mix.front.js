const mix = require('laravel-mix');

mix.js('frontend/assets/src/js/commonPlugin.js', 'frontend/web/js/516.js')
	.js('frontend/assets/src/js/script.js', 'frontend/web/js/516.js')
	.version();

mix.sass('frontend/assets/src/sass/common-516.scss', 'frontend/web/css')
	.sass('frontend/assets/src/sass/516.scss', 'frontend/web/css')
	.options({
		imgLoaderOptions: {enabled: false},
		postCss: [
			require('postcss-css-variables')()
		]
	});



mix.setPublicPath('frontend/web/');
