<?php

namespace frontend\models\forms;

use common\base\validators\StringValidator;
use common\models\Addresses;
use common\models\Company;
use common\models\PriorityPrice;
use common\models\ServiceName;
use Yii;
use yii\base\Model;
use yii\validators\SafeValidator;

/**
 * Class CompanySearchForm
 *
 * @package frontend\models\forms
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class CompanySearchForm extends Model {
	/** @var string */
	public $name;
	const ATTR_NAME = 'name';

	/** @var string */
	public $serviceName;
	const ATTR_SERVICE_NAME = 'serviceName';

	/** @var string[] */
	public $presetFilter;
	const ATTR_PRESET_FILTER = 'presetFilter';

	/** @var int */
	public $count = 0;
	const ATTR_COUNT = 'count';

	/** @var bool Подгрузка ли это при скролле */
	public $isLoad = false;
	const ATTR_IS_LOAD = 'isLoad';

	//Параметр пагинации
	const ITEMS_PER_PAGE = 15;

	/** @var Company[] */
	public $result = [];

	const CONTAINER_COMPANY = 'container_company';
	const CONTAINER_SERVICE = 'container_service';

	const PRESET_PUBLIC_SERVICE = 'public-services';
	const PRESET_TECH_SERVICE = 'tech-services';
	const PRESET_REST_SERVICE = 'rest-services';

	/** @var array Гуиды услуг для пресетов */
	const FILTERS = [
		self::PRESET_PUBLIC_SERVICE => [

		],
		self::PRESET_TECH_SERVICE   => [

		],
		self::PRESET_REST_SERVICE   => [

		],
	];

	/**
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function rules() {
		return [
			[static::ATTR_NAME, StringValidator::class],
			[static::ATTR_SERVICE_NAME, StringValidator::class],
			[static::ATTR_PRESET_FILTER, SafeValidator::class],
		];
	}

	/**
	 *
	 * Поиск компаний и услуг для основной формы
	 *
	 * @throws \yii\base\Exception
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function search() {
		if (null !== $this->presetFilter) {
			$this->result[static::CONTAINER_SERVICE] = $this->_searchService();
		}
		else {
			$this->result[static::CONTAINER_COMPANY] = $this->_searchCompany();
			$this->result[static::CONTAINER_SERVICE] = $this->_searchService();
		}

		$this->_slice();
	}

	/**
	 * Поиск по названию компании
	 *
	 * @return Company[]
	 *
	 * @throws \yii\base\Exception
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	private function _searchCompany() {
		$query = Company::find();
		$query->joinWith(Company::REL_ADDRESSES);

		//Фильтруем по выбранному городу
		$query->andWhere([
				'OR',
				[Company::tableName() . '.' . Company::ATTR_TOWN_NUM => Yii::$app->env->getCity()->rowguid],
				[Addresses::tableName() . '.' . Addresses::ATTR_CITY_GUID => Yii::$app->env->getCity()->rowguid]
			]
		);

		//Фильтруем по имени
		if (null !== $this->name) {
			$arrayNames = explode(' ', $this->name);
			$orWhereCompany = ['AND'];
			foreach ($arrayNames as $companyName) {
				$orWhereCompany[] = ['LIKE', Company::ATTR_OFFICIAL_NAME, $companyName];
			}
			$orWhereAddress = ['AND'];
			foreach ($arrayNames as $companyName) {
				$orWhereAddress[] = ['LIKE', Addresses::tableName() . '.' . Addresses::ATTR_NAME, $companyName];
				break;
			}
			$query->andWhere(['OR', $orWhereCompany, $orWhereAddress]);
		}
		$query->andWhere(['<>', Company::tableName() . '.' . Company::ATTR_COLOR_INDEX, 6]);

		$query->joinWith(Company::REL_ACTIVE_PRIORITY_PRICES);

		$query->orderBy([
			PriorityPrice::tableName() . '.' . PriorityPrice::ATTR_TOWN_PRIORITY => SORT_ASC,
			PriorityPrice::tableName() . '.' . PriorityPrice::ATTR_PRIORITY      => SORT_ASC,
			Company::tableName() . '.' . Company::ATTR_COLOR_INDEX               => SORT_ASC
		]);

		$query->limit(300);

		$result = $query->all();

		return $result;
	}

	/**
	 * Поиск по услуге
	 *
	 * @return array
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	private function _searchService() {
		$query = PriorityPrice::find();
		$query->joinWith(PriorityPrice::REL_COMPANY);
		$query->joinWith(PriorityPrice::REL_COMPANY . '.' . Company::REL_ADDRESSES);
		$query->joinWith(PriorityPrice::REL_REFERENCE_ONE);

		if (!empty($this->name)) {
			$arrayNames = explode(' ', $this->name);
			foreach ($arrayNames as $str) {
				$query->andWhere(['like', ServiceName::tableName() . '.' . ServiceName::ATTR_NAME, $str]);
			}
		}

		//Поиск по фильтрам пресета
		if (null !== $this->presetFilter) {
			$query->andWhere(['IN', ServiceName::tableName() . '.' . ServiceName::ATTR_GROUP_NUM, $this->presetFilter]);
		}

		$query->orderBy([
			PriorityPrice::ATTR_TOWN_PRIORITY => SORT_ASC,
			PriorityPrice::ATTR_PRIORITY      => SORT_ASC,
			Company::ATTR_COLOR_INDEX         => SORT_ASC,
		]);

		$query->limit(300);
		/** @var PriorityPrice[] $queryResult */
		$queryResult = $query->all();

		//Группируем компании по услуге
		$result = [];
		foreach ($queryResult as $row) {
			if (false !== strpos(mb_strtolower($row->referenceOne->Name), 'досуг')) {
				continue;
			}

			$result[$row->referenceOne->Name][] = $row;
		}

		return $result;
	}

	/**
	 * Пагинация результата
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	private function _slice() {
		//Если не подгрузка то отдаём с первого элемента, иначе берем номер из формы
		if (false === $this->isLoad) {
			$this->count = 0;
		}

		$this->result = array_slice($this->result, $this->count, static::ITEMS_PER_PAGE);
	}
}