<?php

namespace frontend\widgets;

use common\models\Town;
use Yii;
use yii\base\Widget;
use yii\caching\TagDependency;

/**
 * Виджет выбора городов
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class CityWidget extends Widget {

	const ACTIVE_TOWNS = [
		'E6D830DB-FA47-4D7C-8D6E-5BD09E037E98', //Владивосток
		'BB2287A8-EB92-4CEE-A606-D09353452996', //Находка
		'0A674AF6-378E-4A52-932E-A27ACC16581A', //Уссурийск
		'FEB01130-4C36-4006-81F6-52F5CD64DD2C', //Артем
		'4E69AEB1-FC35-42A4-A313-F2A8B2158CCF', //п.Угловое
	];

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function run() {
		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, 4]);
		$towns = Yii::$app->cache->get($cacheKey);
		if (false === $towns) {
			/** @var Town[] $towns */
			$towns = Town::find()->andWhere([
				'IN', Town::ATTR_GUID, static::ACTIVE_TOWNS
			])->all();


			Yii::$app->cache->set($cacheKey, $towns, null, new TagDependency(['tags' => Town::class]));
		}

		return $this->render('city-widget', ['towns' => $towns]);
	}
}