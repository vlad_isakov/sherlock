<?php

use frontend\controllers\CatalogController;
use kartik\form\ActiveForm;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 *
 * @var \frontend\models\forms\CompanySearchForm $form
 */
?>
<?php $htmlForm = ActiveForm::begin(
	[
		'method'  => 'POST',
		'action'  => CatalogController::getActionUrl(CatalogController::ACTION_SEARCH),
		'options' => []
	]
); ?>
	<input name="CompanySearchForm[name]" tabindex="1" type="text" class="search-input" placeholder="Название услуги или компании...">
<?= \yii\helpers\Html::submitButton('найти', ['style' => 'display: none;']) ?>
<?php ActiveForm::end(); ?>