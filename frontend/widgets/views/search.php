<?php

use kartik\typeahead\Typeahead;
use yii\web\JsExpression;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 */
?>

<div class="v2 solr-search-b">
	<?php
	$template = $this->render('search-result');
	?>
	<?= Typeahead::widget([
		'name'          => 'country',
		'options'       => ['placeholder' => 'Название услуги или компании...', 'class' => 'search-input'],
		'pluginOptions' => ['highlight' => true],
		'dataset'       => [
			[
				'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
				'display'        => 'value',
				'templates'      => [
					'notFound'   => '<div class="text-danger" style="padding:0 8px">Ничего не найдено.</div>',
					'suggestion' => new JsExpression("Handlebars.compile('{$template}', {'noEscape' : true})")
				],
				'remote'         => [
					'url'      => '/search/?query=%QUERY',
					'wildcard' => '%QUERY'
				]
			]
		]
	]); ?>
</div>