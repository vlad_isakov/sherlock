<?php

use yii\bootstrap\Html;
use common\base\helpers\StringHelper;
use frontend\controllers\CompanyController;
use frontend\controllers\NewsController;
use frontend\controllers\CatalogController;
/**
 * @author Исаков Владислав <visakov@biletur.ru>
 * @var \yii\web\View                          $this
 * @var \common\modules\news\models\SiteNews[] $news
 */

?>

<div class="row row-30">
	<div class="col-md-12 col-xs-12 col-sm-12 wow fadeInUp news"><h3>Новости</h3></div>
	<?php foreach ($news as $newsItem): ?>
		<div class="col-md-4 col-xs-12 col-sm-6 wow fadeInUp news">
			<b><?= $newsItem->date ?></b><br>
			<h5><?= StringHelper::trimSentence($newsItem->title, 50) ?></h5>
			<p class="q"><a style="color: #95999c;" href="<?= CatalogController::getActionUrl(CatalogController::ACTION_NEWS, ['id' => $newsItem->id]) ?>"><?= StringHelper::trimSentence(strip_tags($newsItem->text), 200) ?></a></p>
			<?php
			$companyName = explode('/', $newsItem->company->Official_Name);
			$companyName = $companyName[0];
			?>
			<b><a href="<?= CompanyController::getActionUrl(CompanyController::ACTION_VIEW, ['id' => $newsItem->company->siteCompany->old_id]) ?>"><?= $companyName ?></a></b>
		</div>
	<?php endforeach ?>
</div>
