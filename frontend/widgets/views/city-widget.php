<?php

use frontend\controllers\SiteController;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 * @var \common\models\Town[] $towns
 */
?>

<ul class="rd-menu rd-navbar-dropdown">
	<?php foreach ($towns as $town): ?>
	<?php
		if($town->Name == Yii::$app->env->getCity()->Name) {
			continue;
		}
	?>
		<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="<?= SiteController::getActionUrl(SiteController::ACTION_SET_CITY, ['id' => $town->rowguid]) ?>"><?= $town->Name ?></a>
		</li>
	<?php endforeach ?>
</ul>
