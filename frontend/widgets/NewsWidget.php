<?php
/**
 * @author Исаков Владислав <visakov@biletur.ru>
 */

namespace frontend\widgets;


use common\models\Company;
use common\modules\news\models\SiteNews;
use Yii;
use yii\base\Widget;

/**
 * Class NewsWidget
 *
 * @package frontend\widgets
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class NewsWidget extends Widget {
	/** @var int Лимит */
	public $limit = 9;
	const ATTR_LIMIT = 'limit';

	/** @var array|null Гуиды компаний */
	public $companyGuid;
	const ATTR_COMPANY_GUID = 'companyGuid';

	/** @var string Вьюшка */
	public $view = 'main-news';
	const ATTR_VIEW = 'view';

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function run() {
		$query = SiteNews::find();
		$query->andWhere([SiteNews::ATTR_IS_PUBLISHED => 1]);

		if (null !== $this->companyGuid) {
			$query->andWhere(['IN', SiteNews::ATTR_IS_PUBLISHED, $this->companyGuid]);
		}
		$query->joinWith(SiteNews::REL_COMPANY);
		//$query->joinWith(SiteNews::REL_IMAGE);
		$query->andWhere([Company::tableName() . '.' . Company::ATTR_TOWN_NUM => Yii::$app->env->getCity()->rowguid]);

		$query->orderBy([SiteNews::ATTR_DATE => SORT_DESC])
			->limit($this->limit);

		$news = $query->all();

		return $this->render($this->view, ['news' => $news]);
	}
}