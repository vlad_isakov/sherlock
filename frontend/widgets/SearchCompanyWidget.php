<?php

namespace frontend\widgets;

use frontend\models\forms\CompanySearchForm;
use yii\base\Widget;

/**
 * Class SearchCompanyWidget
 *
 * @package frontend\widgets
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class SearchCompanyWidget extends Widget {


	public function run() {
		$form = new CompanySearchForm();

		return $this->render('search', [
			'form' => $form
		]);
	}
}