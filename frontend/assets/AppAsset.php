<?php

namespace frontend\assets;

use common\helpers\Dump;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\helpers\ArrayHelper;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle {
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/common-516.css',
		'css/516.css',
	];
	public $js = [
		'/js/core.min.js',
		'/js/516.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
		BootstrapPluginAsset::class
	];

	/**
	 * Переопределение метода для публикации ресурсов с хэшем версии
	 *
	 * @param \yii\web\View $view
	 */
	public function registerAssetFiles($view) {
		$manifest = self::getManifest();

		$manager = $view->getAssetManager();
		foreach ($this->js as $js) {
			if (is_array($js)) {
				$file = array_shift($js);
				$options = ArrayHelper::merge($this->jsOptions, $js);
				$view->registerJsFile($manager->getAssetUrl($this, $manifest[$file]), $options);
			}
			else {
				if ($js !== null) {
					if (array_key_exists($js, $manifest)) {
						$view->registerJsFile($manager->getAssetUrl($this, $manifest[$js]), $this->jsOptions);
					}
					else {
						$view->registerJsFile($manager->getAssetUrl($this, $js), $this->jsOptions);
					}
				}

			}
		}
		foreach ($this->css as $css) {
			if (is_array($css)) {
				$file = array_shift($css);
				$options = ArrayHelper::merge($this->cssOptions, $css);
				$view->registerCssFile($manager->getAssetUrl($this, $manifest['/' . $file]), $options);
			}
			else {
				if ($css !== null) {
					$view->registerCssFile($manager->getAssetUrl($this, $manifest['/' . $css]), $this->cssOptions);
				}
			}
		}
	}

	/**
	 * Получение хэшированного ключа для публикации ресурса
	 *
	 * @return false|mixed|string
	 */
	public static function getManifest() {
		$manifest = file_get_contents(\Yii::getAlias("@webroot") . "/mix-manifest.json");
		$manifest = json_decode($manifest, true);

		return $manifest;
	}
}
