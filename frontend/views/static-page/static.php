<?php
/**
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @var string $title
 * @var string $html
 */
?>
<h2><?= $title ?></h2>

<div class="row">
	<div class="col-xs-12">
		<?= $html ?>
	</div>
</div>