<?php

use frontend\controllers\CompanyController;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @var \frontend\models\forms\CompanySearchForm $form
 * @var \yii\web\View                            $this
 */

/** @var \common\models\Company[] $companies */
$companies = $form->result[$form::CONTAINER_COMPANY];

/** @var \common\models\PriorityPrice[] $services */
$services = $form->result[$form::CONTAINER_SERVICE];

echo $this->render('catalog', ['companies' => $companies, 'services' => $services, 'name' => $form->name]);
?>

