<?php

use common\base\helpers\StringHelper;
use frontend\controllers\CatalogController;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 * @var \common\models\Classifier[] $map
 */

?>
<div class="row row-30 ">
	<div class="col-md-12 col-xs-12 col-sm-12 wow fadeInUp news"><h3>Каталог</h3></div>
	<?php foreach ($map as $mainFolder): ?>
		<div class="col-md-4 col-xs-12 col-sm-6 wow fadeInUp catalog category-block">
			<h5><a href="<?= CatalogController::getActionUrl(CatalogController::ACTION_CATEGORY, ['guid' => strtolower($mainFolder->rowguid), 'slug' => StringHelper::urlAlias($mainFolder->Folder_Name)]) ?>"><?= $mainFolder->Folder_Name
					?></a></h5>
			<?php foreach ($mainFolder->childs as $levelOneFolder): ?>
				| <a href="<?= CatalogController::getActionUrl(CatalogController::ACTION_CATEGORY, ['guid' => strtolower($levelOneFolder->rowguid), 'slug' => StringHelper::urlAlias($levelOneFolder->Folder_Name)]) ?>"><?= $levelOneFolder->Folder_Name
					?></a>
			<?php endforeach ?>
		</div>
	<?php endforeach ?>
</div>