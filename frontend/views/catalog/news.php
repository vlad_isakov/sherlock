<?php

use yii\helpers\Html;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @var \yii\web\View                        $this
 * @var \common\modules\news\models\SiteNews $news
 */

?>

<div class="col-xs-12">
	<b><?= $news->date ?></b><br>
	<h4><?= $news->title ?></h4>
	<?php if (null !== $news->objectImage): ?>
		<?= Html::img($news->getWebImagePath(), ['width' => 300, 'alt' => $news->company->Official_Name]) ?>
	<?php endif ?>
	<?= $news->text ?>
</div>
