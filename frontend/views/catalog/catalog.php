<?php

use common\base\helpers\StringHelper;
use frontend\controllers\CatalogController;
use frontend\controllers\CompanyController;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @var \common\models\Company[]       $companies
 * @var \common\models\PriorityPrice[] $services
 * @var                                $name
 */

?>
<div class="row row-30">
	<?php if (count($companies) > 0): ?>
		<div class="col-md-12 col-xs-12 col-sm-12 wow fadeInUp news"><h3>Компании по запросу "<?= $name ?>":</h3></div>
		<?php foreach ($companies as $company): ?>
			<?php
			$companyName = explode('/', $company->Official_Name);
			$companyName = $companyName[0];
			/*if (null === $company->siteCompany) {
				continue;
			}*/
			?>
			<div class="col-md-12 col-xs-12 col-sm-12 wow fadeInUp catalog company-item">
				<?php if (null !== $company->siteCompany):?>
					<h5><a href="<?= CompanyController::getActionUrl(CompanyController::ACTION_VIEW, ['id' => $company->siteCompany->old_id]) ?>"><?= $companyName ?></a></h5>
				<?php else: ?>
					<h5><a href="<?= CompanyController::getActionUrl(CompanyController::ACTION_VIEW, ['id' => $company->rowguid]) ?>"><?= $companyName ?></a></h5>
				<?php endif ?>
				<?php if (null !== $company->logo): ?>
					<img src="<?= $company->logo ?>" alt="<?= $companyName ?>" width="100">
				<?php endif ?>
				<?php if (null !== $company->street): ?>
					<p>
						<i class="glyphicon glyphicon-map-marker"></i> <?= $company->street->name ?>, <?= $company->Home ?>
					</p>
				<?php elseif (isset($company->addresses[0])): ?>
					<p>
						<i class="glyphicon glyphicon-map-marker"></i> <?= $company->addresses[0]->street->name ?>, <?= $company->addresses[0]->Home ?>
						<br>
						<a href="tel:<?= $company->addresses[0]->Phone_Digits ?>"> <?= $company->addresses[0]->Phone_NUm ?></a>
					</p>
				<?php endif ?>
				<?php if (!empty($company->WorkHours)): ?>
					<p>
						<i class="glyphicon glyphicon-time"></i> <?= $company->WorkHours ?>
					</p>
				<?php elseif (isset($company->addresses[0]) && null !== $company->addresses[0]->WorkHours): ?>
					<p>
						<i class="glyphicon glyphicon-time"></i> <?= $company->addresses[0]->WorkHours ?>
					</p>
				<?php endif ?>
			</div>
		<?php endforeach ?>
	<?php endif ?>
	<?php if (count($services) > 0): ?>
		<div class="col-md-12 col-xs-12 col-sm-12 wow fadeInUp news"><h3>Услуги по запросу "<?= $name ?>":</h3></div>
		<?php foreach ($services as $name => $service): ?>
			<div class="col-md-12 col-xs-12 col-sm-12 wow fadeInUp catalog category">
				<a style="font-size: 1.7rem" href="<?= CatalogController::getActionUrl(CatalogController::ACTION_CATEGORY, ['guid' => $service[0]->referenceOne->Class_Num, 'slug' => StringHelper::urlAlias($name)]) ?>"><?= $name ?></a>
			</div>
		<?php endforeach ?>
	<?php endif ?>
</div>