<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\AppAsset;
use frontend\controllers\CatalogController;
use frontend\widgets\CityWidget;
use frontend\widgets\SearchCompanyWidget;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="yandex-verification" content="e3ddc79f7d475d67"/>
	<link rel="icon" href="/images/favicon.png" type="image/x-icon">
	<style>.ie-panel {
			display: none;
			background: #212121;
			padding: 10px 0;
			box-shadow: 3px 3px 5px 0 rgba(0, 0, 0, .3);
			clear: both;
			text-align: center;
			position: relative;
			z-index: 1;
		}

		html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {
			display: block;
		}</style>
	<?php $this->registerCsrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (m, e, t, r, i, k, a) {
			m[i] = m[i] || function () {
				(m[i].a = m[i].a || []).push(arguments)
			};
			m[i].l = 1 * new Date();
			k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
		})
		(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		ym(53562316, "init", {
			clickmap: true,
			trackLinks: true,
			accurateTrackBounce: true,
			webvisor: true
		});
	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/53562316" style="position:absolute; left:-9999px;" alt=""/></div>
	</noscript>
	<!-- /Yandex.Metrika counter -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139683220-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}

		gtag('js', new Date());

		gtag('config', 'UA-139683220-1');
	</script>


	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820"
                                                                                           alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
<div class="preloader">
	<div class="preloader-body">
		<div class="cssload-container">
			<div class="cssload-speeding-wheel"></div>
		</div>
		<p>Загрузка...</p>
	</div>
</div>
<div class="page">
	<header class="section page-header">
		<!--RD Navbar-->
		<div class="rd-navbar-wrap">
			<nav class="rd-navbar rd-navbar-classic" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static"
			     data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="170px"
			     data-xl-stick-up-offset="170px" data-xxl-stick-up-offset="170px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
				<div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
				<div class="rd-navbar-aside-outer">
					<div class="rd-navbar-aside">
						<!--RD Navbar Panel-->
						<div class="rd-navbar-panel">
							<!--RD Navbar Toggle-->
							<button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
							<!--RD Navbar Brand-->
							<div class="rd-navbar-brand">
								<!--Brand--><a class="brand" href="/"><img class="brand-logo-dark" src="/images/logo.png" alt="Справочная служба 516.ru" height="51"/></a>
							</div>
						</div>
						<div class="rd-navbar-info rd-navbar-collapse">
							<article class="info-modern">
								<div class="info-modern-icon fa-phone"></div>
								<?php if('BB2287A8-EB92-4CEE-A606-D09353452996' == Yii::$app->env->getCity()->rowguid):?>
									<a class="info-modern-link" href="tel:4236692516">692-516</a>
								<?php else: ?>
									<a class="info-modern-link" href="tel:4232516516">2-516-516</a>
								<?php endif ?>
							</article>
							<p><h5>Позвоните нам сейчас, чтобы получить консультацию от наших специалистов!</h5></p>
						</div>
					</div>
				</div>
				<div class="rd-navbar-main-outer">
					<div class="rd-navbar-main">
						<div class="rd-navbar-nav-wrap">
							<ul class="rd-navbar-nav">
								<li class="rd-nav-item active"><a class="rd-nav-link" href="#"><b><i class="glyphicon glyphicon-map-marker"></i> <?= Yii::$app->env->getCity()->Name ?></b></a>
									<?= CityWidget::widget() ?>
								</li>
								<li class="rd-nav-item"><a class="rd-nav-link" href="<?= CatalogController::getActionUrl(CatalogController::ACTION_INDEX) ?>">Каталог</a>
								</li>
								<li class="rd-nav-item"><a class="rd-nav-link" href="#">Реклама</a>
								</li>
								<li class="rd-nav-item"><a class="rd-nav-link" href="/about/">О компании</a>
								</li>
								<li class="rd-nav-item col-md-5 col-xs-12">
									<?= SearchCompanyWidget::widget() ?>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!--Swiper-->
	<section class="section swiper-container swiper-slider swiper-slider-1" data-loop="true" data-autoplay="false" data-simulate-touch="false" data-slide-effect="fade">
		<div class="swiper-wrapper text-center text-lg-left">
			<div class="swiper-slide" data-slide-bg="" data-url="https://www.mechnikoff.ru/novosti/geneticheskie-testy/">
				<img src="/images/banner_06_11_19_web.jpg" class="img-responsive">
			</div>
			<div class="swiper-slide" data-slide-bg="" data-url="<?= CatalogController::getActionUrl(CatalogController::ACTION_INDEX) ?>">
				<img src="/images/banner1.jpg" class="img-responsive">
			</div>
			<div class="swiper-slide" data-slide-bg="" data-url="<?= CatalogController::getActionUrl(CatalogController::ACTION_INDEX) ?>">
				<img src="/images/banner2.jpg" class="img-responsive">
			</div>
		</div>
		<!--Swiper Pagination-->
		<div class="swiper-pagination"></div>
		<!--Swiper Navigation-->
		<div class="swiper-button-prev"></div>
		<div class="swiper-button-next"></div>
	</section>
	<!--About-->
	<section class="section section-lg section-inset-1 position-relative index-1">
		<div class="container" style="margin-top: 20px;">
			<?= $content ?>
		</div>
	</section>
	<!--Contacts-->
	<section class="section section-lg bg-gray-700">
		<div class="container">
			<div class="row row-30">
				<div class="col-lg-4">
					<div class="row row-30">
						<div class="col-sm-6 col-lg-12 wow fadeInRight">
							<article class="info-classic align-items-center">
								<div class="unit unit-spacing-md align-items-center flex-column flex-md-row text-center text-md-left">
									<div class="unit-left">
										<div class="info-classic-icon fa-map-marker"></div>
									</div>
									<div class="unit-body"><a class="info-classic-link" href="#">Россия, Приморский край, г. Владивосток, пр-т. Острякова, д. 5</a></div>
								</div>
							</article>
						</div>
						<div class="col-sm-6 col-lg-12 wow fadeInRight" data-wow-delay=".05s">
							<article class="info-classic align-items-center">
								<div class="unit unit-spacing-md align-items-center flex-column flex-md-row text-center text-md-left">
									<div class="unit-left">
										<div class="info-classic-icon fa-envelope"></div>
									</div>
									<div class="unit-body"><a class="info-classic-link" href="mailto:office@mail.516.ru">office@mail.516.ru</a></div>
								</div>
							</article>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="row row-30">
						<div class="col-sm-6 col-lg-12 wow fadeInRight" data-wow-delay=".1s">
							<article class="info-classic align-items-center info-classic-2">
								<div class="unit unit-spacing-md align-items-center flex-column flex-md-row text-center text-md-left">
									<div class="unit-left">
										<div class="info-classic-icon fa-phone"></div>
									</div>
									<div class="unit-body"><a class="info-classic-link" href="tel:74232516516">+7 (423) 2-516-516</a></div>
								</div>
							</article>
						</div>
						<div class="col-sm-6 col-lg-12 wow fadeInRight">
							<article class="info-classic align-items-center info-classic-2">
								<div class="unit unit-spacing-md align-items-center flex-column flex-md-row text-center text-md-left">
									<div class="unit-left">
										<div class="info-classic-icon fa-fax"></div>
									</div>
									<div class="unit-body"><a class="info-classic-link" href="tel:74232215516">+7 (423) 2-215-516</a></div>
								</div>
							</article>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="row row-30">
						<div class="col-sm-6 col-lg-12 wow fadeInRight" data-wow-delay=".05s">
							<article class="info-classic align-items-center">
								<div class="unit unit-spacing-md align-items-center flex-column flex-md-row text-center text-md-left">
									<div class="unit-left">
										<div class="info-classic-icon fa-instagram"></div>
									</div>
									<div class="unit-body"><a class="info-classic-link" rel="nofollow" href="https://instagram.com/Allo_516/">Присоединяйтесь в Instagram</a></div>
								</div>
							</article>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
	<footer class="section footer-classic">
		<div class="container">
			<p class="rights"><span>&copy;&nbsp; </span><span class="copyright-year"></span><span>&nbsp;</span><span>Компания 516</span></p>
		</div>
	</footer>
</div>
<?php $this->registerJs('$(this).commonPlugin();')?>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
