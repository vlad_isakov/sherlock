<?php
/**
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @var \frontend\models\forms\CompanySearchForm $form
 */

use yii\widgets\Pjax; ?>

<?php Pjax::begin(); ?>
<div class="row">
	<div class="col-xs-12">
		<div class="block-panel">
			<?= $this->render('_search-form', ['form' => $form]) ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="block-panel">
			<?= $this->render('_search-result', ['companies' => $form->result]) ?>
		</div>
	</div>
</div>
<?php Pjax::end(); ?>
