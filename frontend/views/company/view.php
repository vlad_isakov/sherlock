<?php

use common\base\helpers\StringHelper;
use frontend\controllers\CatalogController;
use phpnt\yandexMap\YandexMaps;

/**
 * @author Исаков Владислав <visakov@biletur.ru>
 *
 * @var \common\models\Company                 $company
 * @var \common\modules\news\models\SiteNews[] $news
 * @var  string                                $address
 * @var  string                                $location
 * @var array                                  $images
 */

if (null !== $location) {
	$items = [
		[
			'latitude'  => $location[1],
			'longitude' => $location[0],
			'options'   => [
				[
					'hintContent'          => $company->Official_Name,
					'balloonContentHeader' => $company->Official_Name,
					'balloonContentBody'   => $company->HowToGetScheme,
					'balloonContentFooter' => $company->Office,
				],
				[
					'preset'    => 'islands#icon',
					'iconColor' => '#19a111'
				]
			]
		],
	];
}
?>

<h3><?= $company->Official_Name ?></h3>

<div style="margin-top: 20px;"></div>

<ul class="nav nav-tabs">
	<li class="active"><a data-toggle="tab" href="#menu-contact">Контакты</a></li>
	<?php if ($company->Color_Index == 1): ?>
		<li><a data-toggle="tab" href="#menu-service">Товары и услуги</a></li>
	<?php endif ?>
	<?php if (count($news) > 0): ?>
		<li><a data-toggle="tab" href="#menu-news">Новости</a></li>
	<?php endif ?>
	<?php if (count($images) > 0): ?>
		<li><a data-toggle="tab" href="#menu-photo">Фото</a></li>
	<?php endif ?>
</ul>

<div class="tab-content" style="padding-top: 20px;">
	<div id="menu-contact" class="tab-pane active">
		<div class="col-md-8 col-xs-12">
			<p><h5><?= $address ?></h5></p>
			<?php if (null !== $location) : ?>
				<?= YandexMaps::widget([
					'myPlacemarks'  => $items,
					'mapOptions'    => [
						'center'   => [$location[1], $location[0]],                           // центр карты
						'zoom'     => 14,                                                     // показывать в масштабе
						'controls' => ['zoomControl', 'fullscreenControl', 'searchControl'],  // использовать эл. управления
						'control'  => [
							'zoomControl' => [                                                        // расположение кнопок управлением масштабом
							                                                                          'top'  => 75,
							                                                                          'left' => 5
							],
						],
					],
					'disableScroll' => true,                                                    // отключить скролл колесиком мыши (по умолчанию true)
					'windowWidth'   => '100%',                                                  // длинна карты (по умолчанию 100%)
					'windowHeight'  => '400px',                                                 // высота карты (по умолчанию 400px)
				]); ?>
			<?php endif ?>
		</div>
		<div class="col-md-4 col-xs-12">
			<?php if (null !== $company->phones): ?>
				<p>
				<h5>Телефоны:</h5>
				<?php foreach ($company->phones as $phone): ?>
					<?php if (strlen($phone->Phone) < 11): ?>
						<a href="tel:<?= $phone->company->town->Phone_Code ?><?= $phone->Phone ?> ">(<?= $phone->company->town->Phone_Code ?>) <?= $phone->Phone ?> </a> <?= $phone->Description ?> <br>
					<?php else: ?>
						<a href="tel:<?= $phone->Phone ?> "> <?= $phone->Phone ?> </a> <?= $phone->Description ?> <br>
					<?php endif ?>
				<?php endforeach ?>
				</p>
			<?php endif ?>
			<?php if (!empty($company->WorkHours)): ?>
				<p>
				<h5>Режим работы:</h5>
				<?= $company->WorkHours ?>
				</p>
			<?php endif ?>
			<?php if (!empty($company->URL)): ?>
				<p>
				<h5>Сайт:</h5>
				<a rel="nofollow" href="http://<?= $company->URL ?>"><?= $company->URL ?></a>
				</p>
			<?php endif ?>
			<?php if (!empty($company->Email)): ?>
				<h5>Email:</h5>
				<a href="mailto:<?= $company->Email ?>"><?= $company->Email ?></a>
			<?php endif ?>
		</div>
	</div>
	<div id="menu-service" class="tab-pane fade">
		<table class="table table-bordered table-condensed table-striped table-hover price">
			<tr>
				<th class="text-center">Товар/услуга</th>
				<th class="text-center">Доп.информация</th>
				<th class="text-center">Цена(мин)</th>
				<th class="text-center">Цена(макс)</th>
				<th class="text-center">Вид продаж</th>
				<th class="text-center">Страна производитель</th>
			</tr>
			<?php foreach ($company->activePriorityPrices as $price): ?>
				<?php
				if (null === $price->referenceOne) {
					continue;
				}
				?>
				<tr>
					<td><?= $price->referenceOne->Name ?></td>
					<td class="text-center"><?= $price->Text1 ?></td>
					<td class="text-center"><?= $price->Float1 ?></td>
					<td class="text-center"><?= $price->Float2 ?></td>
					<td class="text-center"><?= $price->String1 ?></td>
					<td class="text-center"><?= $price->String2 ?></td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
	<div id="menu-news" class="tab-pane fade">
		<?php foreach ($news as $newsItem): ?>
			<b><?= $newsItem->date ?></b><br>
			<h5><?= StringHelper::trimSentence($newsItem->title, 150) ?></h5>
			<p class="q"><a style="color: #95999c;" href="<?= CatalogController::getActionUrl(CatalogController::ACTION_NEWS, ['id' => $newsItem->id]) ?>"><?= StringHelper::trimSentence(strip_tags($newsItem->text), 300) ?></a></p>
			<?php
			$companyName = explode('/', $newsItem->company->Official_Name);
			$companyName = $companyName[0];
			?>
			<hr style="margin-top: 20px;">
		<?php endforeach ?>
	</div>
	<div id="menu-photo" class="tab-pane fade">
		<?php
		//echo dosamigos\gallery\Gallery::widget(['items' => $images]);
		?>
	</div>
</div>