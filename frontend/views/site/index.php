<?php

/* @var $this yii\web\View */

$this->title = '«Служба 516» | Создание и поддержание информационно-справочных баз данных о предприятиях, их товарах и услугах';

use frontend\controllers\CompanyController;
use frontend\models\forms\CompanySearchForm;
use frontend\controllers\CatalogController;

?>
<div class="container offset-negative-1">
	<div class="row no-gutters">
		<div class="col-sm-6 col-lg-3 wow fadeInUp">
			<article class="box-default box-default-3">
				<div class="box-default-icon fa-globe"></div>
				<h4 class="box-default-title">Поиск <br>компаний</h4>
				<div class="box-default-text">Поиск среди тысяч организаций Вашего города.</div>
				<a class="box-default-link fa-chevron-right" href="<?= CatalogController::getActionUrl(CatalogController::ACTION_INDEX) ?>"></a>
			</article>
		</div>
		<div class="col-sm-6 col-lg-3 wow fadeInUp" data-wow-delay=".05s">
			<article class="box-default box-default-2">
				<div class="box-default-icon fa-lightbulb-o"></div>
				<h4 class="box-default-title">Услуги <br>ЖКХ</h4>
				<div class="box-default-text">Организации ЖКХ....</div>
				<a class="box-default-link fa-chevron-right" href="<?= CatalogController::getActionUrl(CatalogController::ACTION_CATEGORY, ['guid' => '5946ed93-43f1-4274-aff9-bda782566d50', 'slug' => 'public-service']) ?>"></a>
			</article>
		</div>
		<div class="col-sm-6 col-lg-3 wow fadeInUp" data-wow-delay=".1s">
			<article class="box-default box-default-1">
				<div class="box-default-icon fa-cog"></div>
				<h4 class="box-default-title">Сервисные <br>центры</h4>
				<div class="box-default-text">Комании по ремонту бытовой техники, оборудования, экипировки</div>
				<a class="box-default-link fa-chevron-right" href="<?= CatalogController::getActionUrl(CatalogController::ACTION_CATEGORY, ['guid' => '28EAB870-66CD-4812-975B-5CD084E67F8B', 'slug' => 'service-center']) ?>"></a>
			</article>
		</div>
		<div class="col-sm-6 col-lg-3 wow fadeInUp" data-wow-delay=".15s">
			<article class="box-default">
				<div class="box-default-icon fa-briefcase"></div>
				<h4 class="box-default-title">Отдых и <br>досуг</h4>
				<div class="box-default-text">Турбазы, сауны, клубы и кинотеатры - всё что связано с отдыхом</div>
				<a class="box-default-link fa-chevron-right" href="<?= CatalogController::getActionUrl(CatalogController::ACTION_CATEGORY, ['guid' => 'b914855d-eae1-43bb-ab33-20ce9b2f401a', 'slug' => 'dosug-i-otdyx']) ?>"></a>
			</article>
		</div>
	</div>
</div>
<div class="container">
	<?= \frontend\widgets\NewsWidget::widget() ?>
</div>
<div class="container">
	<div class="row row-30">
		<div class="col-md-6 col-lg-4 wow fadeInUp">
			<h3>О компании</h3><img src="/images/logo.png" alt="" width="200"/>
			<p>Служба 516 является коммерческой компанией, которая создает и поддерживает информационно-справочные базы данных о предприятиях, их товарах и услугах и организовывает к ним доступ через сайт www.516.ru и Контакт-центр Службы 516. </p>
			<p><h4>Миссия нашей компании</h4>
			Устанавливать связи между потребителем и компанией-продавцом различных товаров и услуг и, тем самым, способствовать развитию экономики Дальнего Востока. </p>

		</div>
		<div class="col-md-6 col-lg-4 wow fadeInUp" data-wow-delay=".05s">
			<h3>Услуги</h3>
			<p>Приглашаем Вас присоединиться к числу наших партнеров.</p>
			<ul class="list-marked-1">
				<li><a href="#">Широкий охват аудитории</a></li>
				<li><a href="#">Многообразие доступов</a></li>
				<li><a href="#">Бесплатное SEO продвижение</a></li>
				<li><a href="#">SMS-маркетинг</a></li>
				<li><a href="#">Рекламное предложение</a></li>
				<li><a href="#">Абонент-клиент</a></li>
				<li><a href="#">Гибкие тарифы</a></li>
				<li><a href="#">Эксклюзивные предложения</a></li>
			</ul>

		</div>
		<div class="col-lg-4">
			<div class="row row-30">
				<div class="col-md-6 col-lg-12 wow fadeInUp" data-wow-delay=".1s">
					<div class="info">
						<div class="info-header">
							<div class="icon fa-comment"></div>
							<h4>24/7 Справочная</h4>
						</div>
						<div class="info-item" style="font-size: 1.7rem;">
							Владивосток: <a class="info-link" href="tel:#">(423) 2-516-516</a><br>
							Находка: <a class="info-link" href="tel:#">(4236) 692-516</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>