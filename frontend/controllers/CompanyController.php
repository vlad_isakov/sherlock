<?php

namespace frontend\controllers;

use common\components\FrontendController;
use common\forms\tour\SearchForm;
use common\helpers\Dump;
use common\models\Company;
use common\models\site\SiteCompany;
use common\models\User;
use common\modules\news\models\SiteNews;
use frontend\models\forms\CompanySearchForm;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Контроллер компаний
 *
 * @package frontend\controllers
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class CompanyController extends FrontendController {
	const ACTION_INDEX = '';
	const ACTION_VIEW = 'view';

	public function actionIndex($preset = null) {
		$form = new CompanySearchForm();

		if (null !== $preset) {
			$form->presetFilter = $preset;
		}

		if (Yii::$app->request->isPost) {
			$form->load(Yii::$app->request->post());
		}
		Dump::dDie($form);
		$form->search();

		return $this->render('index', ['form' => $form]);
	}

	/**
	 * @param null $id
	 *
	 * @return string
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionView($id = null) {

		$query = Company::find();
		$query->joinWith(Company::REL_ADDRESSES);
		$query->joinWith(Company::REL_PHONES);
		$query->joinWith(Company::REL_STREET);
		$query->joinWith(Company::REL_TOWN);
		$query->joinWith(Company::REL_WORK_HOURS);
		$query->joinWith(Company::REL_ACTIVE_PRIORITY_PRICES);

		if (strlen($id) < 10) {
			$query->joinWith(Company::REL_SITE_COMPANY);
			$query->andWhere([SiteCompany::tableName() . '.' . SiteCompany::ATTR_OLD_ID => $id]);
		}
		else {
			$query->andWhere([Company::tableName() . '.' . Company::ATTR_GUID => $id]);
		}

		/** @var Company $company */
		$company = $query->one();

		if (null === $company) {
			throw new NotFoundHttpException('Компания не найдена');
		}

		$address = $company->town->Name . ', ' . $company->street->name . ', ' . $company->Home . ', ' . $company->Office;
		$location = Yii::$app->yandexGeo->getLocation($address);
		$news = SiteNews::find()
			->andWhere([SiteNews::ATTR_COMPANY_GUID => $company->rowguid])
			->andWhere([SiteNews::ATTR_IS_PUBLISHED => 1])
			->orderBy([SiteNews::ATTR_DATE => SORT_DESC])
			->all();

		$title = $company->Official_Name . ': отзывы, адрес на карте, телефон, сайт | ' . 'Список предложений на сайте компании "Служба 516" | ' . $company->town->Name;

		$this->view->title = $title;

		$this->view->registerMetaTag([
			'name'    => 'title',
			'content' => $title
		]);

		$this->view->registerMetaTag([
			'name'    => 'description',
			'content' => 'Компания ' . $company->Official_Name
		]);

		$this->view->registerMetaTag([
			'name'    => 'keywords',
			'content' => $company->Official_Name
		]);

		$images = [];

		/*
		foreach ($company->objectFiles as $file) {
			$filePath = '/uploads/company/photo' . DIRECTORY_SEPARATOR . substr($file->filename, 0, 2) . DIRECTORY_SEPARATOR . $file->filename;
			$images[] = [
				'url'     => $filePath,
				'src'     => Yii::$app->imageresize->getUrl($filePath, 150, 150, 'outbound', 80),
				'options' => ['title' => $company->Official_Name]
			];
		}*/

		return $this->render('view', [
				'company'  => $company,
				'address'  => $address,
				'location' => $location,
				'news'     => $news,
				'images'   => $images
			]
		);
	}

	/**
	 * Подгрузка компаний ajax'ом
	 *
	 * @return string
	 *
	 * @throws \yii\base\Exception
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionSearch() {
		$this->layout = false;
		Yii::$app->response->format = Response::FORMAT_JSON;

		$form = new CompanySearchForm();

		if (Yii::$app->request->isAjax) {
			$form->load(Yii::$app->request->post());
		}

		$form->search();

		return $this->renderAjax('_companies', ['companies' => $form->result]);
	}

	/**
	 * Редактирование компании
	 *
	 * @return string|\yii\web\Response
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionEdit() {
		if (Yii::$app->user->isGuest) {
			return $this->redirect('/site/login');
		}
		/** @var User $user */
		$user = User::find()
			->joinWith(User::REL_COMPANY, true, 'INNER JOIN')
			->andWhere([User::ATTR_ID => Yii::$app->user->id])
			->one();

		if (null === $user) {
			throw  new NotFoundHttpException('Не найдена компания');
		}

		return $this->render('edit', ['company' => $user->company]);
	}
}