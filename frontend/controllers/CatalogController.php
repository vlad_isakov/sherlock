<?php

namespace frontend\controllers;

use common\components\FrontendController;
use common\models\Classifier;
use common\models\Company;
use common\models\PriorityPrice;
use common\models\ServiceName;
use common\modules\news\models\SiteNews;
use frontend\models\forms\CompanySearchForm;
use Yii;
use yii\caching\TagDependency;
use yii\web\NotFoundHttpException;

/**
 * Контроллер новостей
 *
 * @package frontend\controllers
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class CatalogController extends FrontendController {
	const ACTION_INDEX = '';
	const ACTION_CATEGORY = 'category';
	const ACTION_SEARCH = 'search';
	const ACTION_NEWS = 'news';

	public function actionIndex() {

		$map = Classifier::getTree();

		return $this->render('catalog-map', ['map' => $map]);
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionSearch() {
		$form = new CompanySearchForm();

		if (Yii::$app->request->isPost) {
			$form->load(Yii::$app->request->post());
			$form->search();
		}

		return $this->render('index', ['form' => $form]);
	}


	/**
	 * Отображение компаний по категории
	 *
	 * @param string $guid
	 * @param null   $slug
	 *
	 * @return string
	 * @throws \yii\base\Exception
	 * @throws \yii\web\NotFoundHttpException
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionCategory($guid, $slug = null) {

		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, $guid, Classifier::class]);
		/** @var Classifier $classifier */
		$classifier = Yii::$app->cache->get($cacheKey);
		if (false === $classifier) {
			$classifier = Classifier::find()->where([Classifier::ATTR_GUID => $guid])->one();

			Yii::$app->cache->set($cacheKey, $classifier, 3600 * 24 * 7, new TagDependency(['tags' => Classifier::class]));
		}

		if (null === $classifier) {
			throw new NotFoundHttpException('Категория не найдена');
		}

		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, $guid, ServiceName::class]);
		$serviceGuids = Yii::$app->cache->get($cacheKey);
		if (false === $serviceGuids) {
			$services = ServiceName::find()
				->where([ServiceName::ATTR_CLASS_NUM => $guid])
				->indexBy(ServiceName::ATTR_GUID)
				->all();

			$serviceGuids = [];

			if (count($services) > 0) {
				$serviceGuids = array_keys($services);
			}

			Yii::$app->cache->set($cacheKey, $serviceGuids, 3600 * 24 * 7, new TagDependency(['tags' => ServiceName::class]));
		}

		$cacheKey = Yii::$app->cache->buildKey([__METHOD__, $serviceGuids, Company::class, 5]);
		/** @var Company[] $companies */
		$companies = Yii::$app->cache->get($cacheKey);
		if (false === $companies) {
			$companies = Company::find()
				->joinWith(Company::REL_ACTIVE_PRIORITY_PRICES)
				->andWhere(['IN', PriorityPrice::ATTR_REFERENCE_ONE, $serviceGuids])
				->andWhere([Company::ATTR_COLOR_INDEX => 1])
				->andWhere([Company::ATTR_TOWN_NUM => Yii::$app->env->getCity()->rowguid])
				->andWhere([Company::ATTR_INTERNET_SHOW => true])
				->orderBy([
					PriorityPrice::tableName() . '.' . PriorityPrice::ATTR_TOWN_PRIORITY => SORT_ASC,
					PriorityPrice::tableName() . '.' . PriorityPrice::ATTR_PRIORITY      => SORT_ASC,
					Company::tableName() . '.' . Company::ATTR_COLOR_INDEX               => SORT_ASC,
				])
				->limit(100)
				->all();

			foreach ($companies as &$company) {
				$logo = $company->getLogo();
				if (null === $logo) {
					continue;
				}
				$company->logo = '/uploads/company/logo' . DIRECTORY_SEPARATOR . substr($logo->filename, 0, 2) . DIRECTORY_SEPARATOR . $logo->filename;
			}

			Yii::$app->cache->set($cacheKey, $companies, 3600 * 24, new TagDependency(['tags' => Company::class]));
		}

		$title = $classifier->Folder_Name . ' | Список предложений на сайте компании "Служба 516"';

		$this->view->title = $title;

		$this->view->registerMetaTag([
			'name'    => 'title',
			'content' => $title
		]);

		$this->view->registerMetaTag([
			'name'    => 'description',
			'content' => 'Поиск компаний по каталогу справочной службы 516: ' . $classifier->Folder_Name
		]);

		$this->view->registerMetaTag([
			'name'    => 'keywords',
			'content' => '516 поиск по каталогу'
		]);

		return $this->render('catalog', ['companies' => $companies, 'services' => [], 'name' => $classifier->Folder_Name]);
	}

	/**
	 * @param $id
	 *
	 * @return string
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionNews($id) {
		$news = SiteNews::findOne([SiteNews::ATTR_ID => $id]);

		if (null === $news) {
			throw new NotFoundHttpException('Новость не найдена');
		}

		$this->view->title = $news->title . ' | Список предложений на сайте компании "Служба 516"';

		return $this->render('news', ['news' => $news]);
	}
}