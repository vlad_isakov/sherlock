<?php

namespace frontend\controllers;

use common\components\FrontendController;
use common\modules\news\models\SiteNews;
use yii\web\NotFoundHttpException;

/**
 * Контроллер новостей
 *
 * @package frontend\controllers
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class NewsController extends FrontendController {
	const ACTION_INDEX = '';

	/**
	 * @param $id
	 *
	 * @return string
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionIndex($id) {
		$news = SiteNews::findOne([SiteNews::ATTR_ID => $id]);

		if (null === $news) {
			throw new NotFoundHttpException('Новость не найдена');
		}

		return $this->render('view', ['news' => $news]);
	}
}