<?php

namespace frontend\controllers;

use backend\models\forms\CompanySearchForm;
use backend\models\forms\ServiceSearchForm;
use common\base\helpers\StringHelper;
use common\components\FrontendController;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/**
 * Контроллер поиска
 *
 * @package frontend\controllers
 *
 * @author  Исаков Владислав <visakov@biletur.ru>
 */
class SearchController extends FrontendController {
	const ACTION_INDEX = '';
	const ACTION_SEARCH = 'search';

	/**
	 * Поиск по каталогу компаний и услуг
	 *
	 * @param string $query
	 *
	 * @return array
	 *
	 * @throws \yii\web\BadRequestHttpException
	 *
	 * @author Исаков Владислав <visakov@biletur.ru>
	 */
	public function actionIndex($query) {
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException('Запрос не поддерживается');
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		$resultSearch = [];
		$jsonResult = [];

		$result = [
			'companies' => null,
			'services'  => null,
		];

		$searchForm = new CompanySearchForm();
		$searchForm->companyName = $query;
		$searchForm->cityGuid = Yii::$app->env->getCity()->rowguid;
		$result['companies'] = $searchForm->search(10, false);

		$searchForm = new ServiceSearchForm();
		$searchForm->productService = $query;
		$searchForm->cityGuid = Yii::$app->env->getCity()->rowguid;
		$result['services'] = $searchForm->search(10, false);

		foreach ($result as $type => $items) {
			if (count($items) === 0) {
				continue;
			}

			foreach ($items as $item) {
				$url = '';
				$title = '';

				if ($type === 'companies') {
					/** @var \common\models\Company $item */
					$title = explode('/', $item->Official_Name);
					$title = $title[0];

					if (null !== $item->siteCompany) {
						$url = CompanyController::getActionUrl(CompanyController::ACTION_VIEW, ['id' => $item->siteCompany->old_id]);
					}
					else {
						$url = CompanyController::getActionUrl(CompanyController::ACTION_VIEW, ['id' => $item->rowguid]);
					}
				}

				if ($type === 'services') {
					/** @var \common\models\PriorityPrice $item */
					$title = $item->referenceOne->Name;
					$url = CatalogController::getActionUrl(CatalogController::ACTION_CATEGORY, ['guid' => $item->referenceOne->Class_Num, 'slug' => StringHelper::urlAlias($title)]);
				}

				switch ($type) {
					case 'companies':
						$rusType = 'Компании:';
						break;
					case 'services':
						$rusType = 'Услуги:';
						break;
					default:
						$rusType = 'Найдено';
						break;
				}

				$resultSearch[$rusType][] = [
					'title' => StringHelper::trimSentence($title, 60),
					'url'   => $url,
				];
			}
		}

		$text = 'Ничего не найдено';
		if (count($resultSearch) > 0) {
			$text = '';
		}

		foreach ($resultSearch as $type => $data) {
			$text .= '<p><strong>' . $type . '</strong></p>';
			$text .= '<ul class="search-result-list">';
			foreach ($data as $item) {
				$text .= '<li><a href="' . $item['url'] . '">' . $item['title'] . '</a></li>';
			}
			$text .= '</ul>';
		}

		$jsonResult[] = ['text' => $text];

		return $jsonResult;
	}
}