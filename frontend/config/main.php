<?php

use common\modules\pages\components\StaticPageUrlRule;

$params = array_merge(
	require __DIR__ . '/../../common/config/params.php',
	require __DIR__ . '/../../common/config/params-local.php',
	require __DIR__ . '/params.php',
	require __DIR__ . '/params-local.php'
);

return [
	'id'                  => 'app-frontend',
	'basePath'            => dirname(__DIR__),
	'bootstrap'           => ['log'],
	'controllerNamespace' => 'frontend\controllers',
	'name'                => 'Служба 516',
	'components'          => [
		'env'          => [
			'class'           => common\components\Environment::class,
			'defaultCityId'   => 'E6D830DB-FA47-4D7C-8D6E-5BD09E037E98', //Владивосток
			'defaultLanguage' => 'ru',
		],
		'request'      => [

			'csrfParam' => '_csrf-frontend',
			'baseUrl'   => '',
		],
		'user'         => [
			'identityClass'   => 'common\models\User',
			'enableAutoLogin' => true,
			'identityCookie'  => ['name' => '_identity-frontend', 'httpOnly' => true],
		],
		'session'      => [
			// this is the name of the session cookie used for login on the frontend
			'name' => 'advanced-frontend',
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'urlManager'   => [
			'enablePrettyUrl' => true,
			'showScriptName'  => false,
			'suffix'          => '/',
			'rules'           => [
				''                                         => 'site/index',
				[
					'pattern' => 'page',
					'route'   => 'page',
					'class'   => StaticPageUrlRule::class,
				],
				'company/<preset:[a-z_\-]+>/'              => 'company/index',
				'company/<id>'                             => 'company/view',
				'news/index/<id>'                          => 'news/index',
				'catalog/category/<guid>/<slug>'           => 'catalog/category',
				'search/<query>/'                          => 'search/index',
				'catalog/<id:[0-9_\-]+>/<subId:[0-9_\-]+>' => 'catalog/category',
				'catalog/news/<id>'                        => 'catalog/news',
				'<controller:\w+>/<action:\w+>/'           => '<controller>/<action>',
				'site/set-city/<id>'                       => 'site/set-city',

			],
		],
	],
	'params'              => $params,
];
